import { userConstants  } from '../_constants';
import { userService } from '../_services';
import { alertActions,notiActions } from './';
import { history,store } from '../_helpers';

export const userActions = {
     loading
    ,loading_success
    ,Login
    ,Logout
    ,Forgotpwd
    ,Registeroutsource
    ,dispAccount
    ,getAccount  
    ,getAccountBypass  
    ,updateAccount
    ,Tracking
    ,Bypass
    ,dispatchpRasValidation
};

function loading() {
    return dispatch => {
        dispatch(request());
    }
    function request() { return { type: userConstants.LOADING_REQUEST } }
}

function loading_success() {
    return dispatch => {
        dispatch(success());
    }
    function success() { return { type: userConstants.LOADING_SUCCESS } }
}

function Login(username, password) {
    return dispatch => {
        dispatch(notiActions.clear());
        dispatch(request({ username : username ,password :password}));
        userService.Login(username, password).then(
            user => {
                dispatch(userActions.loading_success());
                if(user.status === true){
                    if(user.data === 'Not Found'){
                        dispatch(notiActions.warn({severity: 'warn', summary: 'User Login', detail: 'กรุณา Logout แล้วทำการ Login ใหม่อีกครั้งค่ะ/ให้ Admin ตรวจสอบ Username' ,sticky: true}));
                    }else{
                     
                        const res  = user.data;
        
                        if(res.code === 200 || res.code === '200'){
                            dispatch(success(res));
                            history.push('/');
                        }else if(res.code === '52e') { 
                            //POST call error - code =  52e password incorrect
                            dispatch(failure());
                            dispatch(alertActions.error('The password you entered did not match our records. Please double-check and try again.')); 
                        }else if(res.code === '525') { 
                            //POST call error - code =  525 username && password incorrect
                            dispatch(failure());
                            dispatch(alertActions.error('The username and password you entered did not match our records.Please double-check and try again.')); 
                        
                        }else if(res.code === 401) { 
                            dispatch(failure());
                            dispatch(alertActions.error(res.description)); 
                        }

                    } 
                }else if(user.status === false){
                    dispatch(alertActions.error(user.data.toString()));
                    dispatch(failure(user.data.toString()));
                    dispatch(notiActions.error({severity: 'error', summary: 'User Login', detail: user.data ,sticky: true}));
                
                }else{
                    dispatch(notiActions.error({severity: 'error', summary: 'User Login', detail: user.data.message,sticky: true}));
                } 
               
            },error => {
                dispatch(userActions.loading_success());
                dispatch(failure(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
        );     
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function Logout() {
    userService.Logout();
    return { type: userConstants.LOGOUT };
}

function Forgotpwd(user) {
    return dispatch => {
        dispatch(request(user));
        userService.Forgotpwd(user)
            .then(
                user => { 
                    if(user.status === true){
                        if(user.data === 'Not Found'){
                            dispatch(alertActions.error('กรุณาตรวจสอบข้อมูลในการ Resigter ของท่าน')); 
                        }else{
                            const res  = user.data;
                            if(res.type === 'success'){
                                dispatch(success());
                                history.push('/login');
                                dispatch(alertActions.success(res.msg));
                            }else if (res.type === 'error'){
                               
                                dispatch(failure(res.toString()));
                                dispatch(alertActions.error(res.msg)); 
                            }
                        } 
                    }else if(user.status === false){
                        dispatch(failure(user.data.toString()));
                        dispatch(notiActions.error({severity: 'error', summary: 'Forgot PWD', detail: user.data ,sticky: true}));
                    }
                },error => {
                    dispatch(failure(error.message.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}

function Registeroutsource(user){
    return dispatch => {
        dispatch(request(user));
        userService.Registeroutsource(user).then(
            user => { 
                if(user.status === true){
                    if(user.data === 'Not Found'){
                        dispatch(alertActions.error('กรุณาตรวจสอบข้อมูลในการ Resigter ของท่าน')); 
                    }else{
                        const res  = user.data;
                        if(res.type === 'success'){
                            dispatch(success());
                            history.push('/login');
                            dispatch(alertActions.success(res.msg));
                        }else if (res.type === 'error'){
                            dispatch(failure(res.toString()));
                            dispatch(alertActions.error(res.msg)); 
                        }
                    }  
                }else if(user.status === false){
                    dispatch(failure(user.data.toString()));
                    dispatch(alertActions.error(user.data));
                }
            }, error => {
                dispatch(failure(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
        );
    };
    function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}


function dispAccount(data){
    return dispatch => { 
        dispatch(notiActions.clear());
        dispatch(success(data));
    };
    function success(user) { return { type: userConstants.USER_SUCCESS, user } }
}


function mapState(state){
    const { users} = state;
    const { bypass  } = users;

    return {
        bypass
    };
};




function getAccount(key){
    let d_users = mapState(store.getState());
    if(d_users.bypass !== undefined ){
        let bypass = {...d_users.bypass};
        if(bypass.connects === 'tc'){
            key = JSON.parse(localStorage.getItem('wah'));
        }
    }
   
    return dispatch => {
        dispatch(notiActions.clear());
        dispatch(userActions.loading());
        dispatch(request(key));
        if(key === '' || key === undefined){
            dispatch(userActions.loading_success());
            dispatch(notiActions.success({severity: 'warn', summary: 'User Login', detail: 'กรุณา Logout แล้วทำการ Login ใหม่อีกครั้งค่ะ' , sticky: true })); 
            setTimeout(()=>{
                localStorage.removeItem("wah");
                history.push('/login');
            },3000);
          
        }else{
            userService.getAccount(key).then(res => {
          
                dispatch(userActions.loading_success());
                
                if(res.status === true){
                    if(res.data === 'Not Found'){
                        
                        dispatch(notiActions.success({severity: 'warn', summary: 'User Login', detail: 'กรุณา Logout แล้วทำการ Login ใหม่อีกครั้งค่ะ/ให้ Admin ตรวจสอบ Username' ,sticky: true}));
                        setTimeout(()=>{
                            localStorage.removeItem("wah");
                            history.push('/login');
                        },3000);
                    }else{
                        if(res.data.method === 'getbyToken'){
                            history.push('/login');
                        }else{

                            dispatch(success(res.data));
                        }
                    }
                }else if(res.status === false){
                    dispatch(failure(res.data.toString()));
                    dispatch(notiActions.error({severity: 'error', summary: 'User Login', detail: res.data ,sticky: true}));

                }else{
                    dispatch(notiActions.error({severity: 'error', summary: 'User Login', detail: res.data.message,sticky: true}));
                } 
            },error => {
                dispatch(failure(error.toString()));
                dispatch(notiActions.error({severity: 'error', summary: 'User Login', detail: error.toString()}));
            });
        }
    };
    function request(key) { return { type: userConstants.GETUSER_REQUEST,key } }
    function success(user) { return { type: userConstants.GETUSER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.GETUSER_FAILURE, error } }
}

function updateAccount(data) { 
    return dispatch => {
        dispatch(notiActions.clear());
        dispatch(userActions.loading());
        dispatch(request({ data }));
        userService.updateAccount(data).then(
            user => { 
                if(user === 'Not Found'){
                    dispatch(notiActions.success({severity: 'warn', summary: 'Account', detail: 'Upload Failed.' ,sticky: true}));
               
                }else if(user.status === false){
                    dispatch(failure(user.data.toString()));
                    dispatch(notiActions.success({severity: 'warn', summary: 'Account', detail: 'Upload Failed.' ,sticky: true}));
                }else{
                    dispatch(success(user));
                    dispatch(notiActions.success({severity: 'success', summary: 'Success', detail: 'Update Account' })); 
                }
                dispatch(userActions.loading_success());
            },error => {
                dispatch(userActions.loading_success());
                dispatch(failure(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
        ); 
    };

    function request(user) { return { type: userConstants.UPDATE_REQUEST, user } }
    function success(user) { return { type: userConstants.UPDATE_SUCCESS, user } }
    function failure(error) { return { type: userConstants.UPDATE_FAILURE, error } }
}


function Tracking(data){
    return dispatch => {
        dispatch(notiActions.clear());
        //dispatch(userActions.loading());
        dispatch(request(data));
            userService.Tracking(data).then(res => {
                //dispatch(userActions.loading_success());
                if(res.status === true){
                    if(res.data === 'Not Found'){
                      
                    }else{
                        dispatch(success(res.data));
                    }
                }
            });
      
    };
    function request() { return { type: userConstants.TRACKING_REQUEST } }
    function success(user) { return { type: userConstants.TRACKING_SUCCESS, user } }
}

function Bypass(data){
    return dispatch => { 
        dispatch(notiActions.clear());
        dispatch(success(data));
    };
    function success(data) { return { type: userConstants.BYPASS_SUCCESS, data } }
}


function getAccountBypass(key){
    return dispatch => {
        dispatch(notiActions.clear());
        dispatch(userActions.loading());
        dispatch(request(key));
        if(key === '' || key === undefined){
            dispatch(userActions.loading_success());
            dispatch(notiActions.success({severity: 'warn', summary: 'User Login', detail: 'กรุณา Logout แล้วทำการ Login ใหม่อีกครั้งค่ะ' , sticky: true })); 
            setTimeout(()=>{
                localStorage.removeItem("wah");
                localStorage.removeItem("avt");
                history.push('/login');
            },3000);
          
        }else{
            userService.getAccount(key).then(res => {
                dispatch(userActions.loading_success());
                
                if(res.status === true){
                    if(res.data === 'Not Found'){
                        
                        dispatch(notiActions.success({severity: 'warn', summary: 'User Login', detail: 'กรุณา Logout แล้วทำการ Login ใหม่อีกครั้งค่ะ/ให้ Admin ตรวจสอบ Username' ,sticky: true}));
                        setTimeout(()=>{
                            localStorage.removeItem("wah");
                            localStorage.removeItem("avt");
                            history.push('/login');
                        },3000);
                    }else{
                        if(res.data.method === 'getbyToken'){
                            history.push('/login');
                        }else{

                            dispatch(success(res.data));
                        }
                    }
                }else if(res.status === false){
                    dispatch(failure(res.data.toString()));
                    dispatch(notiActions.error({severity: 'error', summary: 'User Login', detail: res.data ,sticky: true}));

                }else{
                    dispatch(notiActions.error({severity: 'error', summary: 'User Login', detail: res.data.message,sticky: true}));
                } 
            },error => {
                dispatch(failure(error.toString()));
                dispatch(notiActions.error({severity: 'error', summary: 'User Login', detail: error.toString()}));
            });
        }
    };
    function request() { return { type: userConstants.GETUSER_REQUEST } }
    function success(user) { return { type: userConstants.GETUSER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.GETUSER_FAILURE, error } }
}

function dispatchpRasValidation(data){
    return dispatch => { 
        dispatch(success(data));
    };
    function success(data) { return { type: userConstants.RASVALIDATION_SUCCESS, data } }
}

