import { userConstants  } from '../_constants';
import { userService } from '../_services';
import { notiActions ,userActions} from './';
import { history } from '../_helpers';
import nl2br from 'react-newline-to-break';
export const ticketActions = {

    upfile
    ,newTicket
};


function upfile(data) {
    return dispatch => {
        dispatch(notiActions.clear());
        dispatch(success(data));
    };
    function success(data) { return { type: userConstants.UPFILE_SUCCESS, data } }
}


function newTicket(data) {
    return dispatch => {
        dispatch(notiActions.clear());
        dispatch(request({ data }));
        userService.newTicket(data).then(res => {
                if(res.status === true){
                    if(res.data.type === 'success'){
                        history.push('/');
                        dispatch(success(res.data));
                        let msgtt = res.data.msg+'\n(หมายเลข TT นี้จะถูกส่งไปยัง email ของท่าน)';
                        
                        dispatch(notiActions.success({severity: 'success', summary: 'Ticket', detail: nl2br(msgtt) , sticky: true })); 
                        
                    }else if(res.data.type === 'waring'){
                        dispatch(success(res.data));
                        dispatch(notiActions.success({severity: 'warn', summary: 'Ticket', detail: res.data.msg , sticky: true })); 
                    }

                }else{
                   dispatch(userActions.loading_success());
                   dispatch(notiActions.error({severity: 'error', summary: 'Error Message', detail: res.data.msg,sticky: true}));
                } 
            },
            error => {
                dispatch(failure(error.toString()));
                dispatch(notiActions.error({severity: 'error', summary: 'Error Message', detail: error.toString()}));
            }
        ); 
    };

    function request(data) { return { type: userConstants.NEWTICKET_REQUEST, data } }
    function success(data) { return { type: userConstants.NEWTICKET_SUCCESS, data } }
    function failure(error) { return { type: userConstants.NEWTICKET_FAILURE, error } }
}



