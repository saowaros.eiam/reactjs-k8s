import { notiConstants } from '../_constants';

export const notiActions = {
    success,
    info,
    warn,
    error,
    clear
};

function success(message) {
    return { type: notiConstants.SUCCESS, message };
}
function info(message) {
    return { type: notiConstants.INFO , message};
}
function warn(message) {
    return { type: notiConstants.WARN , message};
}
function error(message) {
    return { type: notiConstants.ERROR, message };
}
function clear() {
    return { type: notiConstants.CLEAR };
}

