export function authHeader() {
    //return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('wah'));

    if (user) {
        return { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' 
                ,'Authorization': 'Bearer ' + user };
    } else {
        return  { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' };
    }
}