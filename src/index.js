import 'react-app-polyfill/ie11';
import React from 'react';
import ReactDOM from 'react-dom';
import {App} from './App';
import { HashRouter } from 'react-router-dom'
import ScrollToTop from './ScrollToTop';
import { Provider } from "react-redux";
import { store } from "./_helpers";


ReactDOM.render(
<Provider store={store}>
    <HashRouter>
        <ScrollToTop>
            <App />
        </ScrollToTop>
    </HashRouter>
</Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.unregister();