import React, {Component} from 'react';
import {Router, Route , Switch} from 'react-router-dom';
import { connect } from 'react-redux';
import { history } from './_helpers';
import { alertActions , notiActions ,userActions} from './_actions';

import { PrivateRoute } from './_privateroute';
import { DefaultLayout }  from './_containers';

import { Login } from './Pages/Login';
import { Outsource } from './Pages/Outsource';
import { RegisterOutsource } from './Pages/RegisterOutsource';
import { ForgotPWD } from './Pages/ForgotPWD';
import { LoginBypass } from './Pages/LoginBypass';
import { TrueShopUPC } from './Pages/TrueShopUPC';
import { TrueShopBMA } from './Pages/TrueShopBMA';

import { Messages} from 'primereact/messages';

import './assets/layout/nova-colored/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';
import '@fullcalendar/timegrid/main.css';
import './assets/layout/layout.scss';
import './App.scss';



class App extends Component {

    constructor(props) {
        super(props);
        this.state = {};
        const { dispatch  } = this.props;
        history.listen((location, action) => {
            //clear alert on location change
            dispatch(alertActions.clear());
            dispatch(notiActions.clear());
        });

        this.showAlret    = this.showAlret.bind(this);
        this.clear        = this.clear.bind(this);
    }

    componentDidMount() {
        if(this.props.bypass === undefined){
            let user = {...this.props.user};
            if(user.login_name === undefined || user.login_name === ''){
                if(this.props.user){
                    this.props.dispatch(userActions.getAccount(this.props.user));
                }
            }
        }
    }

    showAlret(type,title,msg) {
        this.messages.clear();
        this.messages.show({severity: type, summary: title, detail: msg , sticky: true});
    }


    clear() {
        this.messages.clear();
    }

    componentWillReceiveProps(nextProps) {
        const { alert } = nextProps;
        if(alert.type){
            this.messages.clear();
            if(alert.type === 'alert-success' || alert.type ===  'alert-success')
                this.showAlret('success','Success Message -',alert.message);
            if(alert.type === 'alert-error' || alert.type === 'alert-danger')
                this.showAlret('error','Error Message -',alert.message);  
        }else{
            this.messages.clear();
        }
    }



    render() {

        return (
            <React.Fragment>
                <Messages ref={(el) => this.messages = el} />
                <Router history={history}>
                    <Switch>
                        <Route exact path="/trueshop-bma"    component={TrueShopBMA} /> 
                        <Route exact path="/trueshop-all"    component={TrueShopUPC} /> 
                        <Route exact path="/loginbypass" component={LoginBypass} />
                        <Route exact path="/login"       component={Login} />
                        <Route exact path="/outsource"   component={Outsource} />
                        <Route exact path="/register"    component={RegisterOutsource} /> 
                        <Route exact path="/forgotpwd"   component={ForgotPWD} />
                        {/* <PrivateRoute  path="/"          component={DefaultLayout} /> */}
                    </Switch>
                </Router> 
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
  const { alert , authentication ,users} = state;
  const { user  } = authentication;
  const { bypass  } = users;

  return {
      alert,
      user,
      users
  };
}
const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 
