import React, { Component  } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import classNames from 'classnames';
import {  alertActions ,notiActions  } from '../../_actions';
// sidebar nav config
import navigation from "../_nav";

// routes config
import routes from "../routes";
import { DefaultFooter } from "./DefaultFooter";
import { DefaultHeader } from "./DefaultHeader";
import { DefaultProfile } from "./DefaultProfile";
import { AppMenu  } from './AppMenu';
import { Growl } from 'primereact/growl';
import { ProgressSpinner } from 'primereact/progressspinner';
import "./DefaultLayout.css";

class DefaultLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      layoutMode         : 'overlay',
      layoutColorMode    : 'dark',
      staticMenuInactive : false,
      overlayMenuActive  : false,
      mobileMenuActive   : false,
      chkredirect        : false,
      addClass           : '',
      navigation_role    : navigation.user,
      redirect_menu      : '',
      d_user             : '',
      add_navigation     : ''
    };

    this.onWrapperClick  = this.onWrapperClick.bind(this);
    this.onToggleMenu    = this.onToggleMenu.bind(this);
    this.onSidebarClick  = this.onSidebarClick.bind(this);
    this.onMenuItemClick = this.onMenuItemClick.bind(this);
    this.Notifications   = this.Notifications.bind(this);
  }

  onWrapperClick(event) {
    if (!this.menuClick) {
        this.setState({
            overlayMenuActive: false,
            mobileMenuActive: false
        });
    }
    this.menuClick = false;
  }

  onToggleMenu(event) {
    this.menuClick = true;
    
    if (this.isDesktop()) {
      if (this.state.layoutMode === 'overlay') {
          this.setState({
              overlayMenuActive: this.state.overlayMenuActive
              ,layoutMode: 'static'
  
          });
      }
      else if (this.state.layoutMode === 'static') {
          this.setState({
              staticMenuInactive: this.state.staticMenuInactive
              ,layoutMode: 'overlay'
          });
      }
    }else {
      const mobileMenuActive = this.state.mobileMenuActive;
      this.setState({
          mobileMenuActive: !mobileMenuActive
      });
    }
    event.preventDefault();
  }

  onSidebarClick(event) {
      this.menuClick = true;
  }

  onMenuItemClick(event) {
    if(event.item.type === 'static'){
      this.setState({ layoutMode: 'static' });
    }

    if(event.item.type === 'overlay'){
      this.setState({ layoutMode: 'overlay' });
    }

    if(event.item.type === 'dark'){
      this.setState({ layoutColorMode: 'dark' });
    }

    if(event.item.type === 'light'){
      this.setState({ layoutColorMode: 'light' });
    }
    
    if(!event.item.items) {
      this.setState({
          overlayMenuActive: false,
          mobileMenuActive: false
      });
    }
  }

  addClass(element, className) {
    if (element.classList)
      element.classList.add(className);
    else
      element.className += ' ' + className;
  }

  removeClass(element, className) {
    if (element.classList)
      element.classList.remove(className);
    else
      element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
  }

  isDesktop() {
    return window.innerWidth > 1024;
  }

  componentDidUpdate(prevProps) {

    if(prevProps.notification !== this.props.notification){
      this.Notifications(this.props.notification);
    }  
    if (this.state.mobileMenuActive)
      this.addClass(document.body, 'body-overflow-hidden');
    else
      this.removeClass(document.body, 'body-overflow-hidden');
  }

  Notifications(data) {
    if(data.message) 
      this.growl.show(data.message);
    else 
      this.growl.clear();
  }

  componentDidMount(){
    if(this.props.alert){
      this.props.dispatch(alertActions.clear());
    }

    this.props.dispatch(notiActions.clear());

    if (this.props.user !== undefined 
      && this.props.user !== this.state.d_user
      && this.state.chkredirect === false ){

      this.setState({ d_user: this.props.user });
      let d_user = this.props.user;
      let redirect_menu = '';
      if (d_user.roles === "admin") {
        redirect_menu = <Redirect from="/" to="/dashboard" /> ;
        this.setState({ navigation_role : navigation.admin  , redirect_menu : redirect_menu  ,chkredirect: true});
        
      }else if (d_user.roles === "support") {
        redirect_menu = <Redirect from="/" to="/console" />;
        this.setState({navigation_role : navigation.support, redirect_menu : redirect_menu  ,chkredirect: true});
      }else {
        redirect_menu = <Redirect from="/" to="/home" />;
        this.setState({navigation_role : navigation.user  , redirect_menu : redirect_menu  ,chkredirect: true});
      }
    }
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.user !== undefined 
      && nextProps.user.request_id 
      && this.state.chkredirect === false ) {
       
        this.setState({ d_user: nextProps.user });
        let d_user = nextProps.user;
        let redirect_menu = '';

        if (d_user.roles === "admin") {
          redirect_menu = <Redirect from="/" to="/dashboard" /> ;
          this.setState({ navigation_role : navigation.admin  , redirect_menu : redirect_menu  ,chkredirect: true});
          
        }else if (d_user.roles === "support") {
          redirect_menu = <Redirect from="/" to="/console" />;
          this.setState({navigation_role : navigation.support, redirect_menu : redirect_menu  ,chkredirect: true});
        }else {
          redirect_menu = <Redirect from="/" to="/home" />;
          this.setState({navigation_role : navigation.user  , redirect_menu : redirect_menu  ,chkredirect: true});
        }
  
    }
  }

  render() {
    const { loading } = this.props;
    const logo = this.state.layoutColorMode === 'dark' ? 'assets/layout/images/logo-white.png': 'assets/layout/images/logo-black.png';

    const wrapperClass = classNames('layout-wrapper', {
        'layout-overlay': this.state.layoutMode === 'overlay',
        'layout-static': this.state.layoutMode === 'static',
        'layout-static-sidebar-inactive': this.state.staticMenuInactive && this.state.layoutMode === 'static',
        'layout-overlay-sidebar-active': this.state.overlayMenuActive && this.state.layoutMode === 'overlay',
        'layout-mobile-sidebar-active': this.state.mobileMenuActive,
        'layout-main-dark': this.state.layoutColorMode === 'dark',
        'layout-main-light': this.state.layoutColorMode === 'light'
    });

    const sidebarClassName = classNames("layout-sidebar", {
        'layout-sidebar-dark': this.state.layoutColorMode === 'dark',
        'layout-sidebar-light': this.state.layoutColorMode === 'light'
    });


    let spinner =  '';
    if (loading) {
      spinner = ( <div className="loading_center">
                    <ProgressSpinner style={{width: '5.5em', height: '5.5em'}} strokeWidth="8" fill="#EEEEEE" animationDuration=".5s"/>
                  </div>);
      this.state.addClass = "loading_div";
    }else {
      this.state.addClass = "";
    }

    return (
          <React.Fragment> 
            <div className={` ${this.state.addClass} `}>
            {spinner}
              <div className={wrapperClass} onClick={this.onWrapperClick}>
                <DefaultHeader onToggleMenu={this.onToggleMenu}/>
                <div ref={(el) => this.sidebar = el} className={sidebarClassName} onClick={this.onSidebarClick}>
                  <div className="layout-logo" >
                    <img alt="Logo" src={logo} style={{width:'13em'}}/>
                  </div>
                  <DefaultProfile />
                  <AppMenu model={this.state.navigation_role.items} onMenuItemClick={this.onMenuItemClick} />
                </div>
                <div className="layout-main">
                  <Switch>
                    {routes.map((route, idx) => {
                      return route.component ? (
                        <Route
                          key={idx}
                          path={route.path}
                          exact={route.exact}
                          name={route.name}
                          render={props => <route.component {...props} />}
                        />
                      ) : null;
                    })}
                    {this.state.redirect_menu}
                  </Switch>           
                </div>
                <Growl ref={el => (this.growl = el)} />
                <DefaultFooter />
                <div className="layout-mask"></div>
                
              </div>
            </div>
          </React.Fragment>
    );
  }
}
function mapStateToProps(state) {
  const { users, authentication, alert, notification } = state;
  const { user } = authentication;
  const { loading } = users;
  return {
    user,
    users,
    alert,
    notification,
    loading
  };
}

const connectedDefaultLayout = connect(mapStateToProps)(DefaultLayout);
export { connectedDefaultLayout as DefaultLayout };
