import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { history } from '../../_helpers';
import { isAndroid } from 'react-device-detect';
import "./DefaultProfile.css";

class DefaultProfile extends Component {
  constructor() {
      super();
      this.state = {
        expanded   : false,
        full_name  : '',
        image      : '',
      };
      this.onClick        = this.onClick.bind(this);
      this.onClickLogout  = this.onClickLogout.bind(this);
      this.onClickAccount = this.onClickAccount.bind(this);
  }

  onClick(event) {
      this.setState({expanded: !this.state.expanded});
      event.preventDefault();
  }

  onClickLogout() {
    localStorage.removeItem("wah");
    history.push('/login');
  }

  onClickAccount() {
    history.push('/account');
  }
  
  componentDidMount(){
    if (this.props.user && this.props.user.full_name && this.props.user.full_name !== undefined
      && this.state.full_name !== this.props.user.full_name) {
        this.setState({ full_name : this.props.user.full_name});
        let image = '';
        if(this.props.bypass && this.props.bypass.image !== undefined){
          image = this.props.bypass.image;
          if(isAndroid === true){
            image = 'assets/layout/images/profile.png';
          }else{
            //check an URL is valid or broken
            let request = new XMLHttpRequest();
            let status       = '';

            request.open("GET", image, true);
            request.send();
            request.onload = function(){
                status = request.status;
            }

            if (request.readyState === 4) {
                //check to see whether request for the file failed or succeeded
                if ((status === 200) || (status === 0)) {
                    //your required operation on valid URL
                    image = this.props.bypass.image;
                }else{
                    image = 'assets/layout/images/profile.png';
                }
            }
          }
        }else if(this.props.user.image !== undefined && this.props.user.image !== ''){
          image = this.props.user.image;
          if(this.props.user.type === 'External'){
            image = 'assets/layout/images/profile.png';
          }
          //check an URL is valid or broken
          let request = new XMLHttpRequest();
          let status       = '';

          request.open("GET", image, true);
          request.send();
          request.onload = function(){
              status = request.status;
          }

          if (request.readyState === 4) {
              //check to see whether request for the file failed or succeeded
              if ((status === 200) || (status === 0)) {
                  //your required operation on valid URL
                  image = this.props.user.image;
              }else{
                  image = 'assets/layout/images/profile.png';
              }
          }
        }else{
          image = 'assets/layout/images/profile.png';
        }
        this.setState({ image : image});
    }
    
  }



  componentDidUpdate(prevProps , prevState){
    if (this.props.user && this.props.user.full_name !== prevProps.user.full_name
      && this.props.user.full_name !== undefined
      && this.state.full_name !== this.props.user.full_name) {
        this.setState({ full_name : this.props.user.full_name});
        let image = '';

        if(this.props.bypass && this.props.bypass.image !== undefined){
          image = this.props.bypass.image;
          if(isAndroid === true){
            image = 'assets/layout/images/profile.png';
          }else{
            //check an URL is valid or broken
            let request = new XMLHttpRequest();
            let status       = '';

            request.open("GET", image, true);
            request.send();
            request.onload = function(){
                status = request.status;
            }

            if (request.readyState === 4) {
                //check to see whether request for the file failed or succeeded
                if ((status === 200) || (status === 0)) {
                    //your required operation on valid URL
                    image = this.props.bypass.image;
                }else{
                    image = 'assets/layout/images/profile.png';
                }
            }
          }

        }else if(this.props.user.image !== undefined && this.props.user.image !== ''){
          image = this.props.user.image;
          if(this.props.user.type === 'External'){
            image = 'assets/layout/images/profile.png';
          }
          //check an URL is valid or broken
          let request = new XMLHttpRequest();
          let status       = '';

          request.open("GET", image, true);
          request.send();
          request.onload = function(){
              status = request.status;
          }

          if (request.readyState === 4) {
              //check to see whether request for the file failed or succeeded
              if ((status === 200) || (status === 0)) {
                  //your required operation on valid URL
                  image = this.props.user.image;
              }else{
                  image = 'assets/layout/images/profile.png';
              }
          }
        }else{
          image = 'assets/layout/images/profile.png';
        }
        this.setState({ image : image});
    } 
  }

  render() {
    // const { user } = this.props;

    return (
      <React.Fragment>
        <div className="layout-profile">
          <div>
            { this.state.image 
              ? <img src={this.state.image} alt="profile" className="circle"/>
              : <img src="assets/layout/images/profile.png" alt="profile" />
            }
          </div>
          <button className="p-link layout-profile-link" onClick={this.onClick}>
            <span className="username">{this.state.full_name} </span>
            <i className="pi pi-fw pi-cog"/>
          </button>
          <ul className={classNames({'layout-profile-expanded': this.state.expanded})}>
              <li onClick={this.onClickAccount}><button className="p-link"><i className="pi pi-fw pi-user"/><span>Account</span></button></li>
              <li onClick={this.onClickLogout}> <button className="p-link"><i className="pi pi-fw pi-power-off"/><span>Logout</span></button></li>
          </ul>
        </div>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  const {  authentication, alert, notification ,users} = state;
  const { user } = authentication;
  const { bypass } = users;
  return {
    user,
    alert,
    notification,
    bypass
  };
}

const connectedDefaultProfile = connect(mapStateToProps)(DefaultProfile);
export { connectedDefaultProfile as DefaultProfile }; 