import React, { Component } from 'react';
import { connect } from 'react-redux';

class DefaultFooter extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  
  render() {

    return (
      <React.Fragment>
        <div className="layout-footer">
          <span className="footer-text" style={{'marginRight': '5px'}}>True Work From Home</span>
          <span className="footer-text" style={{'marginLeft': '5px'}}>Powered by ITSM ©2019.</span>
        </div>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
  };
}

const connectedDefaultFooter = connect(mapStateToProps)(DefaultFooter);
export { connectedDefaultFooter as DefaultFooter }; 