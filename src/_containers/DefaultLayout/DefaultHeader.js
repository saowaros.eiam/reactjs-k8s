import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { history } from '../../_helpers';
// import FontAwesome   from 'react-fontawesome';
import "./DefaultHeader.css";
import "../../assets/layout/animate.css";

const defaultProps = {
  onToggleMenu: null
}

const propTypes = {
  onToggleMenu: PropTypes.func.isRequired
}


class DefaultHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      full_name : ''
    };
  }

  componentDidMount(){
    
    if (this.props.user && this.props.user.full_name && this.props.user.full_name !== undefined
      && this.state.full_name !== this.props.user.full_name) {
        this.setState({ full_name : this.props.user.full_name});
    }
  }

  componentDidUpdate(prevProps) {
    if(this.props.titleBar && this.props.titleBar !== this.state.titleBar){
      this.setState({titleBar : this.props.titleBar});
    }

    if (this.props.user &&  this.props.user.full_name !== prevProps.user.full_name
      && this.props.user.full_name !== undefined
      && this.state.full_name !== this.props.user.full_name) {

        this.setState({ full_name : this.props.user.full_name});
    } 
  }
  
  
  onClickLogout() {
    localStorage.removeItem("wah");
    history.push('/login');
  }

  render() {
    const { bypass } = this.props;
    return ( 
      <React.Fragment>
      <div className="layout-topbar clearfix">
        <button className="p-link layout-menu-button" onClick={this.props.onToggleMenu}>
          <span className="pi pi-bars"/>
        </button>
        { bypass.connects ?
         <span style={{ padding: '0em 1em',verticalAlign: 'super',top: '0.1em' , position: 'relative',}}>
            <a rel="noopener noreferrer"  href="https://true-h1.ekoapp.com/webview/">
              <span style={{ color: '#ffffff'}}>Go To Portal</span>
            </a>
        </span>
        : ''}
       

        <span style={{ verticalAlign: 'super', marginLeft: '1em'}}>{this.state.titleBar}</span>
        <div className="layout-topbar-icons" >
          
        
          <button className="p-link but-name">
          <Link to="/account">  
            <span className="layout-topbar-icon pi pi-user" style={{ color:'#FFFFFF'}}></span>
            <span style={{ verticalAlign: 'super',color:'#FFFFFF'}}> 
             { this.state.full_name ? 
              this.state.full_name
                : ''
              }
            </span>
            </Link>
          </button>
          <button className="p-link" onClick={this.onClickLogout.bind(this)}>
            <span className="layout-topbar-icon pi pi-sign-out"></span>
            <span style={{ verticalAlign: 'super'}}>Logout</span>
          </button>  
        </div>
      </div>
    </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

function mapStateToProps(state) {
  const { users, authentication } = state;
  const { user } = authentication;
  const { bypass } = users;
  return {
      user,
      users,
      bypass
  };
}

const connectedDefaultHeader = connect(mapStateToProps)(DefaultHeader);
export { connectedDefaultHeader as DefaultHeader };

