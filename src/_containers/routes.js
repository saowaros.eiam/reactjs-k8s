import React from 'react';
import Loadable from "react-loadable";

import { DefaultLayout } from "./DefaultLayout";

function Loading() {
  return <div>Loading...</div>;
}

// const DefaultLayout = Loadable({
//   loader: () => import("./DefaultLayout"),
//   loading: Loading
// });

const Dashboard = Loadable({
  loader: () => import("../views/Dashboard"),
  loading: Loading
});

const EmptyPage = Loadable({
  loader: () => import("../views/EmptyPage"),
  loading: Loading
});

const SampleDemo = Loadable({
  loader: () => import("../views/SampleDemo"),
  loading: Loading
});

const DataDemo = Loadable({
  loader: () => import("../views/DataDemo"),
  loading: Loading
});

const FormsDemo = Loadable({
  loader: () => import("../views/FormsDemo"),
  loading: Loading
});

const MenusDemo = Loadable({
  loader: () => import("../views/MenusDemo"),
  loading: Loading
});

const MessagesDemo = Loadable({
  loader: () => import("../views/MessagesDemo"),
  loading: Loading
});

const MiscDemo = Loadable({
  loader: () => import("../views/MiscDemo"),
  loading: Loading
});

const OverlaysDemo = Loadable({
  loader: () => import("../views/OverlaysDemo"),
  loading: Loading
});

const ChartsDemo = Loadable({
  loader: () => import("../views/ChartsDemo"),
  loading: Loading
});

const PanelsDemo = Loadable({
  loader: () => import("../views/PanelsDemo"),
  loading: Loading
});



const Account = Loadable({
  loader: () => import("../views/Account"),
  loading: Loading
});

const Outsource = Loadable({
  loader: () => import("../Pages/Outsource"),
  loading: Loading
});

const RegisterOutsource = Loadable({
  loader: () => import("../Pages/RegisterOutsource"),
  loading: Loading
});

const ForgotPWD = Loadable({
  loader: () => import("../Pages/ForgotPWD"),
  loading: Loading
});


const Home = Loadable({
  loader: () => import("../views/Home"),
  loading: Loading
});

const CreateTT = Loadable({
  loader: () => import("../views/CreateTT"),
  loading: Loading
});
const Tracking = Loadable({
  loader: () => import("../views/Tracking"),
  loading: Loading
});
const LoginBypass = Loadable({
  loader: () => import("../Pages/LoginBypass"),
  loading: Loading
});

const TrueShopUPC = Loadable({
  loader: () => import("../Pages/TrueShopUPC"),
  loading: Loading
});

const TrueShopBMA = Loadable({
  loader: () => import("../Pages/TrueShopBMA"),
  loading: Loading
});

const routes = [
   { path: "/", exact: true , name: "Home"                , component: DefaultLayout }
  ,{ path: "/home"          , name: "Home"                , component: Home }
  ,{ path: "/dashboard"     , name: "Dashboard"           , component: Dashboard }
  ,{ path: "/emptypage"     , name: "Blank Pages"         , component: EmptyPage }
  ,{ path: "/account"       , name: "Account"             , component: Account }
  ,{ path: "/outsource"     , name: "Outsource"           , component: Outsource }
  ,{ path: "/register"      , name: "RegisterOutsource"    , component: RegisterOutsource }
  ,{ path: "/forgotpwd"     , name: "ForgotPWD"           , component: ForgotPWD }
  ,{ path: "/loginbypass"   , name: "LoginBypass"       , component: LoginBypass }
  ,{ path: "/trueshop-all"   , name: "TrueShopUPC"       , component: TrueShopUPC }
  ,{ path: "/trueshop-bma"   , name: "TrueShopBMA"       , component: TrueShopBMA }

  
  // showcase
  ,{ path: "/sample"        , name: "Sample Page"  , component: SampleDemo }
  ,{ path: "/forms"         , name: "Forms"        , component: FormsDemo }
  ,{ path: "/data"          , name: "Data"         , component: DataDemo }
  ,{ path: "/panels"        , name: "panels"       , component: PanelsDemo }
  ,{ path: "/overlays"      , name: "Overlays"     , component: OverlaysDemo }
  ,{ path: "/menus"         , name: "Menus"        , component: MenusDemo }
  ,{ path: "/messages"      , name: "Messages"     , component: MessagesDemo }
  ,{ path: "/charts"        , name: "Charts"       , component: ChartsDemo }
  ,{ path: "/misc"          , name: "Misc"         , component: MiscDemo }
  ,{ path: '/creatett',   name: 'Create Ticket'    , component: CreateTT }
  ,{ path: '/tracking',   name: 'Tracking'         , component: Tracking }



 
];

export default routes;
