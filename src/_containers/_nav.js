export default {
      user:{
       items: [
          {label: 'Home', icon: 'pi pi-fw pi-home',to: '/home'},
          // { label: 'Setting', icon: 'pi pi-fw pi-cog',
          //   items: [
          //     { label: 'Menu Modes', icon: 'pi pi-fw pi-cog',
          //       items: [
          //           {label: 'Static Menu', icon: 'pi pi-fw pi-bars',  type : 'static' },
          //           {label: 'Overlay Menu', icon: 'pi pi-fw pi-bars',  type : 'overlay' }
          //       ]
          //     },
          //     {label: 'Menu Colors', icon: 'pi pi-fw pi-align-left',
          //         items: [
          //             {label: 'Dark', icon: 'pi pi-fw pi-bars',  type : 'dark' },
          //             {label: 'Light', icon: 'pi pi-fw pi-bars', type : 'light'  }
          //         ]
          //     }, 
          //   ]
          // }
          // {label: 'Components', icon: 'pi pi-fw pi-globe', badge: '9',
          //     items: [
          //       {label: 'Sample Page', icon: 'pi pi-fw pi-th-large', to: '/sample'},
          //       {label: 'Forms', icon: 'pi pi-fw pi-file', to: '/forms'},
          //       {label: 'Data', icon: 'pi pi-fw pi-table', to: '/data'},
          //       {label: 'Panels', icon: 'pi pi-fw pi-list', to: '/panels'},
          //       {label: 'Overlays', icon: 'pi pi-fw pi-clone', to: '/overlays'},
          //       {label: 'Menus', icon: 'pi pi-fw pi-plus', to: '/menus'},
          //       {label: 'Messages', icon: 'pi pi-fw pi-spinner',to: '/messages'},
          //       {label: 'Charts', icon: 'pi pi-fw pi-chart-bar', to: '/charts'},
          //       {label: 'Misc', icon: 'pi pi-fw pi-upload', to: '/misc'}
          //     ]
          // },
          // {label: 'Template Pages', icon: 'pi pi-fw pi-file',
          //     items: [
          //         {label: 'Empty Page', icon: 'pi pi-fw pi-circle-off', to: '/emptypage'}
          //     ]
          // }
        ]
      },
    
      support:{
        items: [
          {label: 'Dashboard', icon: 'pi pi-fw pi-home',to: '/dashboard'},

          { label: 'Setting', icon: 'pi pi-fw pi-cog',
            items: [
              { label: 'Menu Modes', icon: 'pi pi-fw pi-cog',
                items: [
                    {label: 'Static Menu', icon: 'pi pi-fw pi-bars',  type : 'static' },
                    {label: 'Overlay Menu', icon: 'pi pi-fw pi-bars',  type : 'overlay' }
                ]
              },
              {label: 'Menu Colors', icon: 'pi pi-fw pi-align-left',
                  items: [
                      {label: 'Dark', icon: 'pi pi-fw pi-bars',  type : 'dark' },
                      {label: 'Light', icon: 'pi pi-fw pi-bars', type : 'light'  }
                  ]
              }, 
            ]
          },
          {label: 'Components', icon: 'pi pi-fw pi-globe', badge: '9',
              items: [
                {label: 'Sample Page', icon: 'pi pi-fw pi-th-large', to: '/sample'},
                {label: 'Forms', icon: 'pi pi-fw pi-file', to: '/forms'},
                {label: 'Data', icon: 'pi pi-fw pi-table', to: '/data'},
                {label: 'Panels', icon: 'pi pi-fw pi-list', to: '/panels'},
                {label: 'Overlays', icon: 'pi pi-fw pi-clone', to: '/overlays'},
                {label: 'Menus', icon: 'pi pi-fw pi-plus', to: '/menus'},
                {label: 'Messages', icon: 'pi pi-fw pi-spinner',to: '/messages'},
                {label: 'Charts', icon: 'pi pi-fw pi-chart-bar', to: '/charts'},
                {label: 'Misc', icon: 'pi pi-fw pi-upload', to: '/misc'}
              ]
          },
          {label: 'Template Pages', icon: 'pi pi-fw pi-file',
              items: [
                  {label: 'Empty Page', icon: 'pi pi-fw pi-circle-off', to: '/emptypage'}
              ]
          }
        ]
      },

      admin:{ 
         items: [
          {label: 'Dashboard', icon: 'pi pi-fw pi-home',to: '/dashboard'},

          { label: 'Setting', icon: 'pi pi-fw pi-cog',
            items: [
              { label: 'Menu Modes', icon: 'pi pi-fw pi-cog',
                items: [
                    {label: 'Static Menu', icon: 'pi pi-fw pi-bars',  type : 'static' },
                    {label: 'Overlay Menu', icon: 'pi pi-fw pi-bars',  type : 'overlay' }
                ]
              },
              {label: 'Menu Colors', icon: 'pi pi-fw pi-align-left',
                  items: [
                      {label: 'Dark', icon: 'pi pi-fw pi-bars',  type : 'dark' },
                      {label: 'Light', icon: 'pi pi-fw pi-bars', type : 'light'  }
                  ]
              }, 
            ]
          },
          {label: 'Components', icon: 'pi pi-fw pi-globe', badge: '9',
              items: [
                {label: 'Sample Page', icon: 'pi pi-fw pi-th-large', to: '/sample'},
                {label: 'Forms', icon: 'pi pi-fw pi-file', to: '/forms'},
                {label: 'Data', icon: 'pi pi-fw pi-table', to: '/data'},
                {label: 'Panels', icon: 'pi pi-fw pi-list', to: '/panels'},
                {label: 'Overlays', icon: 'pi pi-fw pi-clone', to: '/overlays'},
                {label: 'Menus', icon: 'pi pi-fw pi-plus', to: '/menus'},
                {label: 'Messages', icon: 'pi pi-fw pi-spinner',to: '/messages'},
                {label: 'Charts', icon: 'pi pi-fw pi-chart-bar', to: '/charts'},
                {label: 'Misc', icon: 'pi pi-fw pi-upload', to: '/misc'}
              ]
          },
          {label: 'Template Pages', icon: 'pi pi-fw pi-file',
              items: [
                  {label: 'Empty Page', icon: 'pi pi-fw pi-circle-off', to: '/emptypage'}
              ]
          }
        ]
      }    
};
