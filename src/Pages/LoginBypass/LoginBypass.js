import React, { Component } from 'react';
import { connect } from 'react-redux';
import { history } from '../../_helpers';
import { userActions,alertActions } from '../../_actions';
import { Base64 } from 'js-base64';
import { ProgressSpinner } from 'primereact/progressspinner';
import { isAndroid } from 'react-device-detect';

import './LoginBypass.css'

class LoginBypass extends Component {
    constructor(props) {
        super(props);
        this.props.dispatch(alertActions.clear());
        this.state = {
            addClass       : '',
            spinner        : '',
            loading        : '',
            page        : '',
            displayloading : false,
            image  :''
        };

    }

    isBase64(str) {
      try {
          return btoa(atob(str)) === str;
      } catch (err) {
          return false;
      }
    } 

    componentDidMount() {
        let url_string = process.env.REACT_APP_URL_TC+this.props.location.search; 

        let url_str = url_string.replace(/amp;/g,"");

        const url = new URL(url_str);

        let d_token = url.searchParams.get("token");
        let d_targetPage = url.searchParams.get("target_page");
        let d_avt = url.searchParams.get("avt");



        if(this.isBase64(d_token) && d_token !== ''){

          let token         = Base64.decode(d_token);
          let targetPage    = Base64.decode(d_targetPage);
          if(token){
       
            if(targetPage){
              this.setState({ page: targetPage});
            }else{
              this.setState({ page: 'home'});
            }

            if(d_avt){
              localStorage.setItem("avt", JSON.stringify(d_avt));
              let de_img = Base64.decode(d_avt);
              let image = '';
              if(isAndroid === true){
                image = 'assets/layout/images/profile.png';
              }else{
                image = 'https://true-h1.ekoapp.com/file/view/'+de_img+'?size=small';
              }
              
              let Data = {};
                Data['connects']  = 'tc';
                Data['image']     = image;
              this.props.dispatch(userActions.Bypass(Data));
            }

            let tc_token    = "tc-"+token;
            localStorage.setItem("wah", JSON.stringify(Base64.encode(tc_token)));

            this.props.dispatch(userActions.getAccountBypass(Base64.encode(tc_token)));
            
          }else{
            // reset login status
            localStorage.removeItem("wah");
            localStorage.removeItem("avt");
            history.push('/login');
          }
          
        }else{
          //reset login status
          localStorage.removeItem("wah");
          localStorage.removeItem("avt");
          history.push('/login');
        }
    
    }

    componentDidUpdate(prevProps) {
      if(this.props.user !== undefined && this.props.user.request_id !== undefined){
          history.push(this.state.page);
      }
    
      if(this.state.addClass === ''  &&  this.props.loading !== undefined){
        let spinner = <div className="loading_center">
                        <ProgressSpinner style={{width: '5em', height: '5em'}} strokeWidth="4" fill="#EEEEEE" animationDuration=".5s"/>
                      </div>;
        this.setState({spinner : spinner});
        this.setState({addClass : 'loading'});
      }   
    }
    
    render() {      
        return (
            <React.Fragment > 
              <div className={`loginbypass ${this.state.addClass} `}>
                { this.state.spinner }
                {this.state.image}
              
            </div>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
  const { users, authentication } = state;
    const { loggingIn,user  } =     authentication;
    const { loading}                  = users;
    return {
        loggingIn
        ,loading
        ,user
    };
}

const connectedLoginBypass = connect(mapStateToProps)(LoginBypass);
export { connectedLoginBypass as LoginBypass }; 
