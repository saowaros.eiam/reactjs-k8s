import '../polyfill.js'
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Card, CardBody, CardGroup, Col, Container, Row } from 'reactstrap';
import { notiActions ,userActions  ,alertActions} from '../_actions';
import { userService } from '../_services/user.service';
import logo from '../../assets/images/login/logo.png';
import './VerifiedRegister.css'

class VerifiedRegister extends Component {
    constructor(props) {
      super(props);     
      //reset login status
    
      this.props.dispatch(userActions.Logout());
      this.state = {
          display       : false,
          data_verify   : []
      };
    }

    componentDidMount() {
      if(this.props.match.params.key1 && this.props.match.params.key2 && this.props.match.params.key3){
        this.props.dispatch(notiActions.clear());
        this.props.dispatch(userActions.loading());
        const Data = new FormData();
              Data['key2']  = this.props.match.params.key2;
              Data['key3']  = this.props.match.params.key3;
        this.props.dispatch(userActions.loading());
        userService.VerifyLogin(Data).then( res => {
          this.props.dispatch(userActions.loading_success());
          if(res.status === true){
            if(res.data === 'Not Found'){
              this.setState({data_verify: []});
            }else{
              this.setState({display: true });
              this.setState({data_verify: res.data });
            }
          }else if(res.status === false){
            this.props.dispatch(notiActions.error({severity: 'error', summary: 'Verify Login', detail: res.data ,sticky: true}));
          }           

        },error => {
          this.props.dispatch(userActions.loading_success());
          this.props.dispatch(alertActions.error(error.toString())); 
        });
      }

      if(logo){
        this.setState({ logo :logo });
      } 
    }

    render() {
        const { display,data_verify } = this.state;
       
        return (
        <div className="app flex-row align-items-center">
          <Container>
            <Row className="justify-content-center">
              <Col md="8">
                <CardGroup>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: 44 + '%' }}>
                    <CardBody className="text-center">
                      <div className="images">
                          <img src={this.state.logo} alt="logo"/>
                      </div>
                    </CardBody>
                  </Card>
                  <Card className="p-4">
                    <CardBody>
                      { display && data_verify &&
                        <div className="col-md-12">
                          { data_verify.title &&
                            <h4>{data_verify.title}</h4>
                          }
                          <p className="text-muted">{data_verify.msg}
                          <Link to="/login" className="btn btn-link">( Login Click.) </Link></p>
                        </div>
                        
                      }
                      { 
                        !display &&
                         
                        <p> 'Compatible with Google Chrome' 
                          <Link to="/login" className="btn btn-link">( Login Click.) </Link> </p>
                      }
                    </CardBody>
                  </Card>
                </CardGroup>
              </Col>
            </Row>
          </Container>
        </div> 
      );
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedVerifiedRegister = connect(mapStateToProps)(VerifiedRegister);
export { connectedVerifiedRegister as VerifiedRegister }; 