import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions ,alertActions } from '../../_actions';
import {InputText} from 'primereact/inputtext';
import {Button} from 'primereact/button';

import './Login.css'

class Login extends Component {
    constructor(props) {
        super(props);
        // reset login status
        this.props.dispatch(userActions.Logout());
        this.props.dispatch(alertActions.clear());

        this.state = {
            username  : '',
            password  : '',
            loading   : '',
            submitted : false,
            displayloading : false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ submitted: true });
        const { username, password } = this.state;
        const { dispatch } = this.props;
        if (username && password) {
          this.setState({ displayloading : true});
          dispatch(alertActions.clear());
          dispatch(userActions.Login(username, password));
        }
    }
    
    componentDidMount() {
      if(this.state.loading === ''){
        let loading = "data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA=="; 
        this.setState({ loading :  loading});
      }

    }

    componentDidUpdate(prevProps) {
      if(prevProps.loggingIn === false && this.state.displayloading === true){
        this.setState({ displayloading : false});
      }
    }
    
    render() {      
        const { username, password, submitted ,displayloading} = this.state;

        return (
            <div className="login-body">
              <div className="login-type">
                  <div className="card login-panel p-fluid">
                      <div className="p-grid">
                          <div className="p-col-12" style={{paddingBottom: '0px'}}>
                            <img src="assets/layout/images/login/logo.png" alt="True Work from Home" style={{width:'10em'}}/>
                            <p className="">TRUE HR LOGIN</p>
                            {/* <p className="">True Staff / Non Staff</p> */}
                          
                            <form name="form" onSubmit={this.handleSubmit}> 
                            <div className="p-col-12">
                                <span className="md-inputfield">
                                    <InputText name="username"  minLength="3" value={this.state.username} onChange={this.handleChange}/>
                                    <label>Username</label>
                                </span>
                                {submitted && !username &&
                                  <div className="help-block red">* Username is required</div>
                                }
                            </div>
                            
                            <div className="p-col-12">
                              <span className="md-inputfield">
                                <InputText type="password" minLength="5" name="password" value={this.state.password} onChange={this.handleChange}/>
                                <label>Password</label>
                              </span>
                              {submitted && !password &&
                                <div className="help-block red">* Password is required</div>
                              }
                            </div>
                            <div className="p-col-12">
                                <Button label="Sign In" className="red-btn" />
                                
                            </div>
                            { submitted && displayloading &&
                              <span> <img src={this.state.loading} alt="Loading"/> </span>
                            }
                            {/* <Link to="/outsource" className="btn btn-link">[ สำหรับพนักงาน Outsource ]</Link> */}
                            <div style={{textAlign:'center'}}>
                              <p >Compatible with 
                                <span>  &nbsp;&nbsp;
                                <img src="assets/images/chrome.png" alt="Chrome"  className="banner-icon" style={{width:'20px', opacity: '0.8'}}/> 
                                 </span> 
                                </p> 
                            </div> 
                          </form>
                          </div>  
                      </div>
                  </div>
              </div>
              <div className="footer">
                <p>Tel. 02-700-9555
                  <span>&nbsp;&nbsp;E-Mail : ITSC@Truecorp.co.th</span>
                </p>
                <p style={{color:'#848484'}}>IT Service Management / Helpdesk & Incident Command Center</p>
              </div>              
           </div>
        );
    }
}

function mapStateToProps(state) {
    const { alert ,loggingIn } = state.authentication;
    return {
        loggingIn
        ,alert
    };
}

const connectedLogin = connect(mapStateToProps)(Login);
export { connectedLogin as Login }; 