import React from 'react';
import ReactDOM from 'react-dom';
import RegisterOutsource from './RegisterOutsource';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<RegisterOutsource />, div);
  ReactDOM.unmountComponentAtNode(div);
});
