import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions  ,alertActions} from '../../_actions';
import { userService } from '../../_services/user.service';
import { Outsource_dpmt } from '../../service/Outsource_dpmt';

import {InputText} from 'primereact/inputtext';
import {Button} from 'primereact/button';
import {Dropdown} from 'primereact/dropdown';

import './RegisterOutsource.css';


class RegisterOutsource extends Component {
  constructor(props) {
      super(props);
      this.state = {
          user: {
              username       : '',
              code_shop      : '',
              emp_id         : '',
              first_name     : '',
              last_name      : '',
              email          : '',
              id_card        : '',
              phone_number   : '',
              outsource_dpmt  : ''
              
          },

          msg_err_card       : false,
          ch_user            : true ,
          submitted          : false,
          msg_err_mail       : false,
          loading_username   : false,
          display_msg_error  : false,
          op_outsource_dpmt  : [],
          msg_error_username : '',
          loading            : 'data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA=='
      };

      this.Outsource_dpmt     = new Outsource_dpmt();
      this.handleChange       = this.handleChange.bind(this);
      this.handleSubmit       = this.handleSubmit.bind(this);
      this.checkID            = this.checkID.bind(this);
      this.checkUsername      = this.checkUsername.bind(this);
  }


  componentDidMount() {
    this.Outsource_dpmt.getData().then( data =>  this.setState({op_outsource_dpmt  : data }));
  }

  checkID(id) {
    if(id.length !== 13) 
      return false;
      let i;
      let sum;
    for( i=0,  sum=0; i < 12; i++)
      sum += parseFloat(id.charAt(i))*(13-i); if((11-sum%11)%10!==parseFloat(id.charAt(12)))
      return false; 
    return true;
  }

  checkEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  handleChange(event) {
    if(event.target === undefined){
      this.setState({outsource_dpmt  : event.value});
      const name     = 'outsource_dpmt';
      const value    = event.value;
      const { user } = this.state;

      this.setState({
        user: {
            ...user,
            [name]: value
        }
      });

    }else{

      const { name, value  } = event.target;
      const { user } = this.state;

      this.setState({
          user: {
              ...user,
              [name]: value
          }
      }, () => {
        if (name === 'username' )
          this.setState({ ch_user : this.CharacterUser(value) });
        }
      );
    }
  }

  CharacterUser(value) {
    const re = /^[a-zA-Z0-9]+\.*$/;
    return re.test(value);
  }

  checkUsername(data){
    if(data){
      this.setState({ loading_username : true});
      const Data = new FormData();
            Data['login_name']  = data;

      let c_mail     = this.checkEmail(this.state.user.email.trim());
      let c_card     = this.checkID(this.state.user.id_card.trim());

      userService.chkLoginname(Data).then(res => { 
        this.setState({ loading_username : false});
          if(res.status === true){
            if(res.data === 'Not Found'){
              this.setState({ msg_error_username : 'Not Found' });
              this.setState({ display_msg_error  : true});
            }else if(res.data.type === 'error'){
              this.setState({ msg_error_username : res.data.msg});
              this.setState({ display_msg_error  : true});
            }else if(res.data.type === 'success'){
                this.setState({ display_msg_error  : false});
                if(c_card === true && c_mail === true ){
                  this.props.dispatch(userActions.Registeroutsource(this.state.user));
                  this.setState({msg_err_card : false});
                  this.setState({msg_err_mail : false});
                }else{
                  if(c_card === false){
                    this.setState({msg_err_card : true});
                  }
                  if(c_mail === false){
                    this.setState({msg_err_mail : true});
                  }
                }
            }else{
              this.setState({ display_msg_error  : false});
            }
            this.setState({ submitted: false });
          }  
        },error => {
          this.props.dispatch(alertActions.error(error.toString()));   
      });
    }
  }

  handleSubmit(event) {
    event.preventDefault();
   
    this.setState({ submitted: true });
    const { user }     = this.state;

    if (user.username && user.first_name && user.last_name && user.outsource_dpmt 
      && user.email && user.id_card  && user.phone_number ) {
     
      if(user.username){
        this.checkUsername(user.username.trim());
      }else{
        let c_card = this.checkID(user.id_card.trim());
        let c_mail = this.checkEmail(user.email.trim());

        if(c_card === true && c_mail === true){
          this.setState({msg_err_card : false});
          this.setState({msg_err_mail : false});
        }else{
          if(c_card === false){
            this.setState({msg_err_card : true});
          }
          if(c_mail === false){
            this.setState({msg_err_mail : true});
          }
        }
      }
    }
  }


  render() {
    const { user, submitted,  msg_err_card, msg_err_mail, loading_username, 
          display_msg_error, ch_user } = this.state;
    return (
        <div className="register-body">
          <div className="register-type">
            <div className="card register-panel p-fluid">
              <div className="p-grid">
                <div className="p-col-12" style={{paddingBottom: '0px'}}>
                  <img src="assets/layout/images/login/logo.png" alt="eHelpdesk for CDS" />
                  <h1>Register - outsource </h1>
                  <p className="text-muted">Create your account</p>
                    <form name="form" onSubmit={this.handleSubmit}> 
                      <div className="p-grid card card-w-title non-shadow-card">
                        <div className="p-col-12 p-md-8">
                            <h2>หน่วยงาน</h2>
                            <Dropdown  autoWidth={false}  name="outsource_dpmt" value={user.outsource_dpmt} 
                            onChange={this.handleChange}  options={this.state.op_outsource_dpmt} />
                          {submitted && !user.outsource_dpmt &&
                            <div className="help-block red">* Department id is required</div>
                          }
                        </div>

                        <div className="p-col-12 p-md-6">
                          <span className="md-inputfield">
                            <InputText type="text" name="first_name" value={user.first_name} onChange={this.handleChange}  className={(submitted && !user.first_name ? ' p-error' : '')} />
                            <label>ชื่อ (ภาษาอังกฤษ)</label>
                          </span>
                          {submitted && !user.first_name &&
                            <div className="help-block red">* First name is a required field</div>
                          }
                        </div>

                        <div className="p-col-12 p-md-6">
                          <span className="md-inputfield">
                            <InputText type="text" name="last_name" value={user.last_name} onChange={this.handleChange}  className={(submitted && !user.last_name ? ' p-error' : '')} />
                            <label>นามสกุล (ภาษาอังกฤษ)</label>
                          </span>
                          {submitted && !user.last_name &&
                            <div className="help-block red">* Last name is a required field</div>
                          }
                        </div>  
                        
                        <div className="p-col-12 p-md-6">
                          <span className="md-inputfield">
                            <InputText type="text" name="emp_id" value={user.emp_id} onChange={this.handleChange}  className={(submitted && !user.emp_id ? ' p-error' : '')} />
                            <label>รหัสพนักงาน (39xxxxxx or 38xxxxxx)</label>
                          </span>
                          {submitted && !user.emp_id &&
                            <div className="help-block red">* Employee ID is required</div>
                          }
                        </div>
                        <div className="p-col-12 p-md-6">
                          <span className="md-inputfield">
                            <InputText type="text" name="email" value={user.email} onChange={this.handleChange} 
                            className={(submitted && !user.email ? ' p-error' : '')}/>
                            <label>E-mail</label>
                          </span>
                          {submitted && !user.email &&
                            <div className="help-block red">* E-mail is required</div>
                          }
                          { msg_err_mail  &&
                            <div className="help-block red">* E-mail ไม่ถูกต้อง</div>
                          }
                        </div>
                        <div className="p-col-12 p-md-6">
                          <span className="md-inputfield">
                              <InputText type="text" name="phone_number" value={user.phone_number} onChange={this.handleChange} 
                              className={(submitted && !user.phone_number ? ' p-error' : '')}/>
                              <label>Phone</label>
                          </span>
                          {submitted && !user.phone_number &&
                            <div className="help-block red">* Phone is required</div>
                          }
                        </div>
                        <div className="p-col-12 p-md-6">
                          <span className="md-inputfield">
                            <InputText type="text" name="id_card" value={user.id_card} onChange={this.handleChange} 
                            className={(submitted && !user.id_card ? ' p-error' : '')}/>
                            <label>ID Card</label>
                          </span>
                          {submitted && !user.id_card &&
                            <div className="help-block red">* ID Card is required</div>
                          }
                          { msg_err_card  &&
                            <div className="help-block red">* รหัสประชาชนไม่ถูกต้อง</div>
                          }
                        </div>
                        <div className="p-col-12 p-md-6">
                          <span className="md-inputfield">
                            <InputText type="text" name="code_shop" value={user.code_shop} onChange={this.handleChange}  />
                            <label>Code Shop</label>
                          </span>
                        </div>
                        <div className="p-col-12 p-md-6">
                          <span className="md-inputfield">
                              <InputText name="username" value={user.username} onChange={this.handleChange} 
                              className={(submitted && !user.username ? ' p-error' : '')}/>
                              <label>Username</label>
                          </span>
                          {loading_username && 
                            <img src={this.state.loading} alt="loading"  />
                          }
                          {submitted && !user.username &&
                            <div className="help-block red">* username is required</div>
                          }
                          { display_msg_error &&
                            <div className="help-block red">* {this.state.msg_error_username}</div> 
                          }
                          { !ch_user &&
                            <div className="help-block red">* username valid a-z, A_Z ,0-9 </div> 
                          }
                        </div>
                        <div className="p-col-12">
                          <hr  />
                          <div className="p-grid">
                            <div className="p-col-12 p-md-3 regis-pad-btn">
                              <Button type="submit" label="Register" icon="pi pi-user" className="red-btn"/>
                              {submitted && 
                                <img src={this.state.loading} alt="loading" />
                              }
                            </div>
                            <div className="p-col-12 p-md-1 cancel-pad-btn">
                              <Link to="/login" className="btn btn-link">Cancel</Link>  
                            </div>
                          </div>
                        </div>
                      </div> 
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }

function mapStateToProps(state) {
  const { registering } = state.registration;
  return {
      registering
  };
}


const connectedRegisterOutsource = connect(mapStateToProps)(RegisterOutsource);
export { connectedRegisterOutsource as RegisterOutsource }; 
