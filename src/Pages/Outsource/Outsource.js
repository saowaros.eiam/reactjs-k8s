import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../../_actions';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import './Outsource.css'

class Outsource extends Component {
    constructor(props) {
      super(props);
      
      // reset login status
      this.props.dispatch(userActions.Logout());

      this.state = {
        username       : '',
        password       : '',
        submitted      : false,
        loading        : '',
        displayloading : false
      };

      this.handleChange    = this.handleChange.bind(this);
      this.handleSubmit    = this.handleSubmit.bind(this);
    }

    handleChange(e) {
      const { name, value } = e.target;
      this.setState({ [name]: value });
    }

    handleSubmit(e) {
      e.preventDefault();

      this.setState({ submitted: true });
      const { username, password } = this.state;
      const { dispatch } = this.props;
      if(username && password){
        dispatch(userActions.Login(username, password));
      }
    }

  
    
    render() {
        const { username, submitted ,displayloading} = this.state;
        return (
            <div className="login-body">
              <div className="login-type">
                  <div className="card login-panel p-fluid">
                  <form name="form" onSubmit={this.handleSubmit}> 
                      <div className="p-grid">
                          <div className="p-col-12" style={{paddingBottom: '0px'}}>
                            <img src="assets/layout/images/login/logo.png" alt="eHelpdesk for CDS" />
                            <p className="">Outsource</p>
                          </div>
                          
                            <div className="p-col-12">
                                <span className="md-inputfield">
                                    <InputText name="username" onChange={this.handleChange}/>
                                    <label>Username</label>
                                </span>
                            </div>
                            {submitted && !username &&
                              <div className="help-block">Username is required</div>
                            }
                            <div className="p-col-12">
                                <span className="md-inputfield">
                                    <InputText type="password" name="password" onChange={this.handleChange} />
                                    <label>Password</label>
                                </span>
                            </div>
                            <div className="p-col-12 signin-div">
                                <Button label="Sign In" className="red-btn"/>
                            </div>
                            { submitted && displayloading &&
                              <span> <img src={this.state.loading} alt="loading"/> </span>
                            }
                            
                          <div className="p-col-12 fotget-div">
                            <p><Link to="/register" className="btn btn-link">[Register]</Link></p>
                            <p><Link to="/forgotpwd" className="btn btn-link">[Forgot Password]</Link></p>
                            <p>Compatible with Google Chrome </p>
                          </div>

                      </div>
                   </form>
                  </div>
              </div>
          
          
              {/* <div className="app flex-row align-items-center">
                <Container>
                <Row className="justify-content-center">
                  <Col md="8">
                    <CardGroup>

                      <Card className="p-4">
                        <CardBody>
                          <div className="col-md-12">
                              <h4>Login</h4>
                              <p className="text-muted">Outsource</p>
                              <form name="form" onSubmit={this.handleSubmit}>
                                <InputGroup  className={'mb-3 form-group' + (submitted && !username ? ' has-error' : '')}>
                                  <InputGroupAddon addonType="prepend">
                                      <InputGroupText>
                                        <i className="icon-user"></i>
                                      </InputGroupText>
                                  </InputGroupAddon>
                                  <Input type="text" placeholder="Username"  name="username" value={username} onChange={this.handleChange}  />
                                </InputGroup> 
                               
                                <InputGroup className={'mb-4 form-group' + (submitted && !password ? ' has-error' : '')}>
                                  <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                      <i className="icon-lock"></i>
                                    </InputGroupText>
                                  </InputGroupAddon>
                                  <Input type="password" name="password" placeholder="Password" value={password} onChange={this.handleChange} />      
                                </InputGroup>
                                {submitted && !password &&
                                  <div className="help-block">Password is required</div>
                                }
    
                                <div className="form-group">
                                      <button className="btn btn-primary">Login</button>
                                      {loggingIn &&
                                          <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                                      } 
                                       <Link to="/register" className="btn btn-link">Register</Link>
                                </div>
                              </from>
                           
                                <br/>
                                <Link to="/registeroutsource" className="btn btn-link">[ ลงทะเบียนขอสิทธิ์เข้าใช้งาน ]</Link>
                                <br/>
                                <Link to="/forgotPWD" className="btn btn-link">[ ลืมรหัสผ่าน ]</Link>
                           
                                  <p>Compatible with Google Chrome</p>
                            </div>
                        </CardBody>
                      </Card>
                    </CardGroup>
                  </Col>
                </Row>
              </Container>
              </div> */}

            </div>    
        );
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedOutsource = connect(mapStateToProps)(Outsource);
export { connectedOutsource as Outsource }; 