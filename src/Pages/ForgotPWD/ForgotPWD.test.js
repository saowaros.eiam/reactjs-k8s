import React from 'react';
import ReactDOM from 'react-dom';
import ForgotPWD from './ForgotPWD';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ForgotPWD />, div);
  ReactDOM.unmountComponentAtNode(div);
});
