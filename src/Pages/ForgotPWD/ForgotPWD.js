import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions  } from '../../_actions';

import RCG from 'react-captcha-generator';
import {InputText} from 'primereact/inputtext';
import {Button} from 'primereact/button';


import './ForgotPWD.css';

class ForgotPWD extends Component {
  constructor(props) {
      super(props);

      this.state = {
        user: {
            username       : '',
            email          : '',
            password       : '',
            password_cf    : '',
        },

        loading             : '',
        msg_err_mail        : false,
        submitted           : false,
        password_has_error  : false,
        weakLabel           : 'Weak',
        mediumLabel         : 'Medium',
        strongLabel         : 'Strong',
        weak_pass           : false,
        captcha             : '',
        res_captcha         : false,
        loading_username    : '',
        display_msg_error   : '',
        ch_user             : '',
        captchaEnter  : ''

      };
  
      this.handleChange     = this.handleChange.bind(this);
      this.handleSubmit     = this.handleSubmit.bind(this);
      this.checkPassword    = this.checkPassword.bind(this);
      this.checkStrength    = this.checkStrength.bind(this);
      this.normalize        = this.normalize.bind(this);

      this.checkCaptcha     = this.checkCaptcha.bind(this);
      this.resultCaptcha    = this.resultCaptcha.bind(this);
  }

  componentDidMount() { 
    if(this.state.loading === ''){
      let loading = "data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA=="; 
      this.setState({ loading :  loading});
    }

  }

  checkEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  checkPassword() {
    if(this.state.user.password ){
      let score = this.checkStrength(this.state.user.password);
      let label = null;
      if(score < 30) {
          label = this.state.weakLabel;
      } else if(score >= 30 && score < 80) {
          label = this.state.mediumLabel;
      } else if(score >= 80) {
          label = null;
          // label = this.state.strongLabel;
      }
      if(label){
        this.setState({weak_pass : true}); 
      }else{
        this.setState({weak_pass : false});
      }
    }

    if(!this.state.user.password || this.state.user.password !== this.state.user.password_cf) {
        this.setState({password_has_error:true});
    }else {
        this.setState({password_has_error:false});
    }
  }
  
  checkStrength(str) {
    let grade = 0;
    let val;

    val = str.match('[0-9]');
    grade += this.normalize(val ? val.length : 1/4, 1) * 25;

    val = str.match('[a-zA-Z]');
    grade += this.normalize(val ? val.length : 1/2, 3) * 10;

    val = str.match('[!@#$%^&*?_~.,;=]');
    grade += this.normalize(val ? val.length : 1/6, 1) * 35;

    val = str.match('[A-Z]');
    grade += this.normalize(val ? val.length : 1/6, 1) * 30;

    grade *= str.length / 8;

    return grade > 100 ? 100 : grade;
  }

  normalize(x, y) {
    let diff = x - y;

    if(diff <= 0)
        return x / y;
    else
        return 1 + 0.5 * (x / (x + y/4));
  }

  handleChange(event) {

      const { name, value } = event.target;
      const { user } = this.state;
     
      this.setState({
          user: {
              ...user,
              [name]: value
          }
      }, () => {
        if (name === 'password' || name === 'password_cf')
          this.checkPassword();
        }
      );
  }

  
  handleSubmit(event) {
    event.preventDefault();
    let res_captcha = this.checkCaptcha();
         this.setState({ submitted: true });
    if(res_captcha === true){
      // this.props.dispatch(alertActions.clear()); 
     
      const { user } = this.state;
      const { dispatch } = this.props;

      if (user.username && user.email && user.password) {
        
        let c_mail = this.checkEmail(user.email.trim());
        if(c_mail === true){
          if(this.state.password_has_error === false  && this.state.weak_pass === false ){
            dispatch(userActions.Forgotpwd(user));
          }
          this.setState({loading : true});
          this.setState({msg_err_mail : false});
        }else{
          this.setState({msg_err_mail : true});
          this.setState({loading : ''});
        }
      }
    }
  }

  resultCaptcha(text) {
    this.setState({ captcha: text });
  }
  
  captchaEnter(event){
    this.setState({captchaEnter  : event.target.value  });
  }
  
  checkCaptcha() {
    
    let resCaptcha = this.state.captcha === this.state.captchaEnter ;

    if(resCaptcha === false){
      this.setState({ res_captcha : true });
    }else{
      this.setState({ res_captcha : false });
    }

    return resCaptcha;
  
  }

  

  render() {
    const { registering  } = this.props;
    const { user, submitted, password_has_error, weak_pass } = this.state;

    return (
              <div className="forgotpwd-body">
                <div className="forgotpwd-type">
                  <div className="card forgotpwd-panel p-fluid">
                    <div className="p-grid">
                      <div className="p-col-12" style={{paddingBottom: '0px'}}>
                        <img src="assets/layout/images/login/logo.png" alt="eHelpdesk for CDS" />
                        <h4>Forgot Your Password?</h4>
                        <form name="form" onSubmit={this.handleSubmit}> 
                          <div className="p-grid forgot-form">
                            <div className="p-col-12 p-sm-12 p-md-6 p-lg-6 p-xl-6 ">
                              <span className="md-inputfield">
                                  <InputText name="username" value={user.username} onChange={this.handleChange} className={(submitted && !user.username ? ' p-error' : '')}/>
                                  <label>Username</label>
                              </span>
                              { submitted && !user.username &&
                                <div className="help-block red">* Username is required</div>
                              }
                            </div>
                            <div className="p-col-12 p-sm-12 p-md-6 p-lg-6 p-xl-6">
                              <span className="md-inputfield">
                                  <InputText name="email" value={user.email} onChange={this.handleChange} className={(submitted && !user.email ? ' p-error' : '')}/>
                                  <label>E-mail</label>
                              </span>
                              { submitted && !user.email &&
                                <div className="help-block red">* E-mail is required</div>
                              }
                            </div>
                            <div className="p-col-12 p-sm-12 p-md-6 p-lg-6 p-xl-6">
                              <span className="md-inputfield">
                                <InputText type="password" name="password" value={user.password} onChange={this.handleChange} className={(submitted && !user.password ? ' p-error' : '')}/>
                                <label>Password</label>
                              </span>
                              { submitted && !user.password &&
                                <div className="help-block red">* Password is required</div>
                              }
                              { user.password && weak_pass === true && 
                                  <div className="help-block red">* Password ต้องมีความยาวอย่างน้อย 8 หลัก 
                                  <br></br> รหัสผ่านควรประกอบด้วย ตัวอักษรพิมพ์เล็ก, ตัวอักษรพิมพ์ใหญ่, ตัวเลข, 
                                  <br></br> สัญลักษณ์พิเศษ ประกอบกัน</div>
                              }
                            </div>
                            <div className="p-col-12 p-sm-12 p-md-6 p-lg-6 p-xl-6">
                              <span className="md-inputfield">
                                  <InputText type="password" name="password_cf" value={user.password_cf} onChange={this.handleChange} className={(submitted && !user.password_cf ? ' p-error' : '')}/>
                                  <label>Confirm password</label>
                              </span>
                              { submitted && !user.password_cf &&
                                <div className="help-block red">* Confirm Password is required</div>
                              }
                              { password_has_error &&
                                <div className="help-block red">* Your password and confirmation password do not match.</div>
                              }
                            </div>
                            <div className="p-col-12 p-sm-12 p-md-6 p-lg-6 p-xl-6">
                              <span className="md-inputfield"> 
                              {/* ref={ref => this.captchaEnter = ref} */}
                                  <InputText type="text"  onChange={this.captchaEnter.bind(this)} value={this.state.captchaEnter}
                                  className={'input_captcha' + (submitted && (this.state.res_captcha === true ) ? ' p-error' : '')} />
                                  <label>Captcha</label>
                              </span>
                              { this.state.res_captcha  &&
                                <div className="help-block red">* The Captcha code does not match.</div>
                                }
                              <br/>
                              <div style={{width:'100%'}}>
                                <span className="md-inputfield form-group captcha" >
                                  <RCG result={this.resultCaptcha} />
                                </span>
                                 
                                 {/* <span className="captcha-reload">
                                  <Button  icon="pi pi-refresh" className="blue-btn" onClick={this.handleRefreshCaptcha.bind(this)} /> 
                                 </span> */}
                              </div>

                              <div style={{width:'100%',float: 'left'}} >
                                <div className="p-sm-6 p-md-4 p-lg-5 p-xl-5">
                                  <Button type='submit' label="Submit" icon="pi pi-user"  />
                                  { registering && 
                                    <span></span>
                                  }
                                </div> 
                              </div> 
                               
                             

                            </div> 
                          </div>
                        </form>
                        
                        <Link to="/outsource" className="btn btn-link">[ สำหรับพนักงาน Outsource ]</Link>
                        <div>Compatible with Google Chrome </div> 

                      </div> 
                    </div>
                  </div>
                </div>
              </div>
            );
  } 
}

function mapStateToProps(state) {
  const { registering } = state.registration;
  return {
      registering
  };
}

const connectedForgotPWD = connect(mapStateToProps)(ForgotPWD);
export { connectedForgotPWD as ForgotPWD }; 
