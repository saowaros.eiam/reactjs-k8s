import React from 'react';
import { Route, Redirect } from 'react-router-dom';


export const PrivateRoute = ({ component: Component, ...rest }) => (   
    <Route {...rest} render={props => (
        localStorage.getItem('wah')
            ? <Component {...props} />
            : <Redirect  from="/"  to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
)


