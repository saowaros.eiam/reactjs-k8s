import { userConstants } from '../_constants';
import { Base64 } from 'js-base64';
import { isAndroid } from 'react-device-detect';
import { UtilService } from '../_services/util.service';

let avt = JSON.parse(localStorage.getItem('avt'));
let bypass = {};
if(avt){
  if(UtilService.isBase64(avt)){
    let de_img = Base64.decode(avt);
    let image = '';
    if(isAndroid === true){
      image = 'assets/layout/images/profile.png';
    }else{
      image = 'https://true-h1.ekoapp.com/file/view/'+de_img+'?size=small';
    }
    bypass['connects']  = 'tc';
    bypass['image']     = image;
  }
}
const initialState = bypass ? { bypass } : {};

export function users(state = initialState, action) {
// export function users(state = {}, action) {
  switch (action.type) {
    case userConstants.LOADING_REQUEST:
      return {
        ...state,
        loading: true
      };
    case userConstants.LOADING_SUCCESS:
      return {
        ...state,
        loading: false
      };
    case userConstants.GETALL_REQUEST:
      return {
        ...state,
        loading: true
      };
    case userConstants.GETALL_SUCCESS:
      return {
        ...state,
        user: action.user
      };
    case userConstants.GETALL_FAILURE:
      return { 
        ...state,
        error: action.error
      };
    case userConstants.DELETE_REQUEST:
      return {
        ...state,
        items: state.items.map(user =>
          user.id === action.id
            ? { ...user, deleting: true }
            : user
        )
      };
    case userConstants.DELETE_SUCCESS:
      return {
        ...state,
        items: state.items.filter(user => user.id !== action.id)
      };
    case userConstants.DELETE_FAILURE:
      return {
        ...state,
        items: state.items.map(user => {
          if (user.id === action.id) {
            const { deleting, ...userCopy } = user;
            return { ...userCopy, deleteError: action.error };
          }
          return user;
        })
      };
    case userConstants.APPS_REQUEST:
      return {
        ...state,
        loading: true
      };
    case userConstants.APPS_SUCCESS:
      return {
        ...state,
        loading: false,
        app: action.user
      };
    case userConstants.APPS_FAILURE:
      return { 
        ...state,
        error: action.error
      };

    case userConstants.CHECKAPPS_REQUEST:
      return {
        ...state,
        loading: true
      };
    case userConstants.CHECKAPPS_SUCCESS:
      return {
        ...state,
        loading: false,
        checkApps: action.Appsname
      };

    case userConstants.UPFILE_SUCCESS:
      return {
        ...state,
        loading: false,
        file: action.data
      };


    case userConstants.NEWTICKET_REQUEST:
      return {
        ...state,
        loading: true
      };
    case userConstants.NEWTICKET_SUCCESS:
      return {
        ...state,
        loading: false,
        user: action.user
      };
    case userConstants.NEWTICKET_FAILURE:
      return { 
        ...state,
        error: action.error
      };

    case userConstants.TICKETS_REQUEST:
      return {
        ...state,
        loading: true
      };
    case userConstants.TICKETS_SUCCESS:
      return {
        ...state,
        loading: false,
        tickets: action.data
      };
    case userConstants.TICKETS_FAILURE:
      return { 
        ...state,
        error: action.error
      };


    case userConstants.PROFSTATS_SUCCESS:
      return {
        ...state,
        loading: false,
        prof_stats: action.prof_stats
      };

      case userConstants.TT_SUCCESS:
      return {
        ...state,
        tt: action.error
      };

   
      case userConstants.FOLLOWTICKET_REQUEST:
      return {
        ...state,
        loading: true
      };
    case userConstants.FOLLOWTICKET_SUCCESS:
      return {
        ...state,
        loading: false,
        follow: action.data
      };
    case userConstants.FOLLOWTICKET_FAILURE:
      return { 
        ...state,
        error: action.error
      };

    case userConstants.REOPENTICKET_REQUEST:
      return {
        ...state,
        loading: true
      };
    case userConstants.REOPENTICKET_SUCCESS:
      return {
        ...state,
        loading: false,
        reopen: action.data
      };
    case userConstants.REOPENTICKET_FAILURE:
      return { 
        ...state,
        error: action.error
      };
    //APPLICATIONS_SUCCESS
    case userConstants.APPLICATIONS_SUCCESS:
      return { 
        ...state,
        applications: action.data
      };
      
    case userConstants.SEARCHCONSOLE_SUCCESS:
      return { 
        ...state,
        searchConsole: action.data
      };
      case userConstants.TRACKING_SUCCESS:
        return { 
          ...state,
          trackung: action.data
        };
      case userConstants.BYPASS_SUCCESS:
        return { 
          ...state,
          bypass: action.data
        };

      case userConstants.RASVALIDATION_REQUEST:
      return {
        ...state,
        loading: true
      };
    case userConstants.RASVALIDATION_SUCCESS:
      return {
        ...state,
        ras_validation: action.data
      };
    case userConstants.RASVALIDATION_FAILURE:
      return { 
        ...state,
        error: action.error
      };
    default:
      return state
  }
}