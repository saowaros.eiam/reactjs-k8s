import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { notification } from './notifications.reducer';
import { cti } from './cti.reducer';




const rootReducer = combineReducers({
  alert,
  notification,
  authentication,
  registration,
  users,
  cti
});


export default rootReducer;