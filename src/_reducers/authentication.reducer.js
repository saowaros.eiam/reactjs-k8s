import { userConstants } from '../_constants';

    
let user = JSON.parse(localStorage.getItem('wah'));
const initialState = user ? { loggedIn: true, user } : {};

export function authentication(state = initialState, action) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        ...state,
        loggingIn: true
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        ...state,
        loggedIn: true,
        user: action.user
      };
    case userConstants.LOGIN_FAILURE:
      return {
        ...state,
        loggingIn: false
      };
    case userConstants.USER_REQUEST:
      return {
        ...state,
        loggingIn: true
      };
    case userConstants.USER_SUCCESS:
      return {
        ...state,
        loggedIn: true,
        user: action.user
      };
    case userConstants.USER_FAILURE:
      return {
        ...state
      };
    case userConstants.LOGOUT:
      return {};
    case userConstants.GETUSER_REQUEST:
      return {
        ...state,
        loggingIn: true
      };
    case userConstants.GETUSER_SUCCESS:
      return {
        ...state,
        loggedIn: true,
        user: action.user
      };
    case userConstants.GETUSER_FAILURE:
      return {
        ...state
      };
    case userConstants.UPDATE_REQUEST:
      return { 
        ...state,
        loggingIn: true
      };
    case userConstants.UPDATE_SUCCESS:
      return {
        ...state,
        loggedIn: true,
        user: action.user
      };
    case userConstants.UPDATE_FAILURE:
      return {
        ...state
      };
    default:
      return state
  }
}