import { notiConstants } from '../_constants';

export function notification(state = {}, action) {
  switch (action.type) {
    case notiConstants.SUCCESS:
      return {
        type: 'success',
        message: action.message
      };
    case notiConstants.INFO:
      return {
        type: 'info',
        message: action.message
      };
    case notiConstants.WARN:
      return {
        type: 'warn',
        message: action.message
      };
    case notiConstants.ERROR:
      return {
        type: 'error',
        message: action.message
      };
    case notiConstants.CLEAR:
      return {
        type: 'clear'
      };
    default:
      return state
  }
}