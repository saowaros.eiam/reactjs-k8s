import { ctiConstants } from '../_constants';

export function cti(state = {}, action) {
  switch (action.type) {
      // case ctiConstants.GETSYMPTOM_REQUEST:
      //   return {
      //     loading: true
      //   };
      // case ctiConstants.GETSYMPTOM_SUCCESS:
      //   return {
      //     ...state,
      //     loading: false,
      //     symptom: action.data
      //   };
      // case ctiConstants.GETSYMPTOM_FAILURE:
      //   return { 
      //     error: action.error
      //   };


      case ctiConstants.GETCATS_REQUEST:
        return {
          ...state,
          loading: true
        };
      case ctiConstants.GETCATS_SUCCESS:
        return {
          ...state,
          loading: false,
          cates:    action.data
        };
      case ctiConstants.GETCATS_FAILURE:
        return { 
          ...state,
          error: action.error
        };

      case ctiConstants.GETTYPES_REQUEST:
        return {
          ...state,
          loading: true
        };
      case ctiConstants.GETTYPES_SUCCESS:
        return {
          ...state,
          loading: false,
          types:    action.data
        };
      case ctiConstants.GETTYPES_FAILURE:
        return { 
          ...state,
          error: action.error
        };


      case ctiConstants.GETITEMS_REQUEST:
        return {
          ...state,
          loading: true
        };
      case ctiConstants.GETITEMS_SUCCESS:
        return {
          ...state,
          loading: false,
          items:    action.data
        };
      case ctiConstants.GETITEMS_FAILURE:
        return { 
          ...state,
          error: action.error
        };

    case ctiConstants.GETSUMMARYS_REQUEST:
          return {
            ...state,
            loading: true
          };
        case ctiConstants.GETSUMMARYS_SUCCESS:
          return {
            ...state,
            loading: false,
            items   : state.items,
            summarys: action.data
          };
        case ctiConstants.GETSUMMARYS_FAILURE:
          return { 
            ...state,
            error: action.error
          };
    default:
      return state
  }
}