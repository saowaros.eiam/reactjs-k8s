import React from 'react';
import ReactDOM from 'react-dom';
import SampleDemo from './SampleDemo';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SampleDemo />, div);
});
