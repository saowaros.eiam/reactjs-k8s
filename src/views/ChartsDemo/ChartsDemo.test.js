import React from 'react';
import ReactDOM from 'react-dom';
import ChartsDemo from './ChartsDemo';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ChartsDemo />, div);
});
