import React, { Component } from 'react';
import { connect } from 'react-redux';
import { userActions } from '../../_actions';
// import {Button} from 'primereact/button';

import './SoftwareInstallerShop.css';

class SoftwareInstallerShop extends Component {
  constructor() {
    super();
    this.state = {
    };
  }

  onClickFile(value){
    if(value.file){
        const data = new FormData();
            data['user_action']     =  this.props.user.login_name;
            data['page']         =  'home';
            data['event']        =  value.event;
            data['score']        =  '1';
            data['filename']     =  value.file;
        this.props.dispatch(userActions.Tracking(data)); 
    }
  }


  render() {
    return (
      <React.Fragment>
        <div className="p-col-12 widget-body shop">
          <div className="dd ">
          
            <ol className="dd-list">
              <li className="dd-item  bordered-danger" data-id="8">
                <div className="dd-handle row-item ">
                  <i className="pi pi-list" style={{ fontSize: '1.3em' }}></i>{' '}
                  <span className="icon-load " onClick={this.onClickFile.bind(this,{'file' : 'Self_PulseSecure_V9_Clear_GUID.exe'  ,'event' : 'download' })} >
                    <a href="assets/install/Self_PulseSecure_V9_Clear_GUID.exe"  rel="noopener noreferrer">
                      <i className="pi pi-download red wave in "  style={{ fontSize: '2em', position: 'absolute' }}></i>
                    </a>
                  </span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <span className="icon-load "onClick={this.onClickFile.bind(this,{'file' : 'https://terra.truecorp.co.th/rascrisis'  ,'event' : 'web' })} >
                    <a href="https://terra.truecorp.co.th/rascrisis"   target="_blank"   rel="noopener noreferrer" >
                      <i className="pi pi-globe red wave in "  style={{ fontSize: '2em', position: 'absolute' }}></i>
                    </a>
                  </span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  Install Pulse Secure (RAS VPN)
                </div>
              </li>
            </ol>  

            <ol className="dd-list">
              <li className="dd-item  bordered-danger" data-id="9">
                <div className="dd-handle row-item ">
                  <i className="pi pi-list" style={{ fontSize: '1.3em' }}></i>{' '}
                  <span className="icon-load " onClick={this.onClickFile.bind(this,{'file' : 'Self_Avaya_One-X.exe'  ,'event' : 'download' })}>
                    <a href="assets/install/Self_Avaya_One-X.exe"    rel="noopener noreferrer" >
                      <i   className="pi pi-download red wave in"   style={{ fontSize: '2em', position: 'absolute' }} ></i>
                    </a>
                  </span> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; 
                  Install Avaya One-X
                </div>
              </li>
            </ol>    

            <ol className="dd-list">
              <li className="dd-item  bordered-danger" data-id="10">
                <div className="dd-handle row-item ">
                  <i className="pi pi-list" style={{ fontSize: '1.3em' }}></i>{' '}
                  <span className="icon-load " onClick={this.onClickFile.bind(this,{'file' : 'Self_Shop_Crisis_Software_Outbound.exe'  ,'event' : 'download' })} >
                    <a href="assets/install/Self_Shop_Crisis_Software_Outbound.exe"  rel="noopener noreferrer" >
                      <i   className="pi pi-download red wave in"   style={{ fontSize: '2em', position: 'absolute' }} ></i>
                    </a>
                  </span> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; 
                  Outbound Software (เมื่อกด Run เครื่องจะ Auto Restart จากนั้นให้ Login เข้าเครื่องอีกครั้ง)
                </div>
              </li>
            </ol>    

            <br/>
            <ol className="dd-list">
              <li className="dd-item  bordered-danger" data-id="11">

                <div className="dd-handle row-item "  >
                  <i className="pi pi-list" style={{ fontSize: '1.3em' }}></i>{' '} &nbsp;&nbsp; 
                  คู่มือการตั้งค่า 
                </div>

                <ol className="dd-list">

                  <li className="dd-item bordered-inverse" data-id="12">
                    <div className="dd-handle">
                      <span className="icon-load " onClick={this.onClickFile.bind(this,{'file' : 'คู่มือการ configuration one-x และการใช้งาน.pdf'  ,'event' : 'read-file' })} >
                        <a href="assets/file/คู่มือการ configuration one-x และการใช้งาน.pdf"  target="_blank"  rel="noopener noreferrer">
                          <i  className="pi pi-file red wave in "  style={{ fontSize: '2em', position: 'absolute' }}></i>
                        </a>
                      </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                      คู่มือการ Configuration One-X  และการใช้งาน
                    </div>
                  </li>


                  <li className="dd-item bordered-inverse" data-id="13">
                    <div className="dd-handle">
                      <span className="icon-load " onClick={this.onClickFile.bind(this,{'file' : 'คู่มือการ map Extension เครื่อง shop.pdf'  ,'event' : 'read-file' })} >
                        <a href="assets/file/คู่มือการ map Extension เครื่อง shop.pdf"  target="_blank"  rel="noopener noreferrer">
                          <i className="pi pi-file red wave in "  style={{ fontSize: '2em', position: 'absolute' }}></i>
                        </a>
                      </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                      คู่มือการ Map Extension เครื่อง Shop
                    </div>
                  </li>


                  <li className="dd-item bordered-inverse" data-id="13">
                    <div className="dd-handle">
                      <span className="icon-load " onClick={this.onClickFile.bind(this,{'file' : 'คู่มือวิธีการ login PDS Genesys.pdf'  ,'event' : 'read-file' })} >
                        <a href="assets/file/คู่มือวิธีการ login PDS Genesys.pdf"  target="_blank"  rel="noopener noreferrer">
                          <i className="pi pi-file red wave in "  style={{ fontSize: '2em', position: 'absolute' }}></i>
                        </a>
                      </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                      คู่มือการ Login PDS Genesys
                    </div>
                  </li>

                </ol>
              </li>
            </ol>
          </div>
        </div>
    
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  const { authentication} = state;
  const { user  }               = authentication;
  return {
      user
  };
}
export default connect(mapStateToProps)(SoftwareInstallerShop);
