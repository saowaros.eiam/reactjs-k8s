import React, { Component } from 'react';
import { connect } from 'react-redux';
import { userActions } from '../../_actions';
import './MeetingSoftware.css';

class MeetingSoftware extends Component {
  onClickFile(value){
    if(value.file){
        const data = new FormData();
            data['user_action']     =  this.props.user.login_name;
            data['page']         =  'home';
            data['event']        =  value.event;
            data['score']        =  '1';
            data['filename']     =  value.file;
        this.props.dispatch(userActions.Tracking(data)); 
    }
  }
  
  render() {

   
    return (
      <div className="p-col-12 widget-body ras">
        <div className="dd ">
          <ol className="dd-list">
            <li className="dd-item bordered-danger row-item" data-id="7">
              <div className="dd-handle row-item">
                <i  className="pi pi-align-right" style={{ fontSize: '1.3em' }} ></i>
                &nbsp;
                <span className="icon-load "
                 onClick={this.onClickFile.bind(this,{'file' : 'https://vroom.truevirtualworld.com'  ,'event' : 'web' })}
                >
                  <a href="https://vroom.truevirtualworld.com"   target="_blank"   rel="noopener noreferrer" >
                    <img alt="vroom" src="assets/images/vroom.png" style={{  width: '2em', position: 'absolute' }}/>
                    {/* <i   className="pi pi-globe red wave in "   style={{ fontSize: '2em', position: 'absolute' }} ></i> */}
                  </a>
                </span>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span className="icon-load "
                 onClick={this.onClickFile.bind(this,{'file' : 'trueVROOM.pdf'  ,'event' : 'read-file' })}
                >
                  <a   href="assets/file/trueVROOM.pdf"   target="_blank"   rel="noopener noreferrer" >
                    <i  className="pi pi-file red wave in "  style={{ fontSize: '2em', position: 'absolute' }}></i>
                  </a>
                </span>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; True VROOM
              </div>
            </li>
            
            <li className="dd-item bordered-danger row-item" data-id="7">
              <div className="dd-handle row-item">
                <i  className="pi pi-align-right" style={{ fontSize: '1.3em' }} ></i>
                &nbsp;
                <span className="icon-load "
                onClick={this.onClickFile.bind(this,{'file' : 'https://www.webex.com/downloads.html'  ,'event' : 'web' })}
                >
                  <a href="https://www.webex.com/downloads.html"   target="_blank"   rel="noopener noreferrer" >
                    {/* <i   className="pi pi-globe red wave in "   style={{ fontSize: '2em', position: 'absolute' }} ></i> */}
                    <img alt="vroom" src="assets/images/webex.png" style={{  width: '2em', position: 'absolute'}}/>
                  </a>
                </span>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span className="icon-load "
                 onClick={this.onClickFile.bind(this,{'file' : 'WebEx%20Manual_EN-SCW.pdf'  ,'event' : 'read-file' })}>
                  <a   href="assets/file/WebEx%20Manual_EN-SCW.pdf"   target="_blank"   rel="noopener noreferrer" >
                    <i  className="pi pi-file red wave in "  style={{ fontSize: '2em', position: 'absolute' }}></i>
                  </a>
                </span>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Video Call (WebEx)
              </div>
            </li>
           
          </ol>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { authentication} = state;
  const { user  }               = authentication;
  return {
      user
  };
}
export default connect(mapStateToProps)(MeetingSoftware);
