import React from 'react';
import ReactDOM from 'react-dom';
import MeetingSoftware from './MeetingSoftware';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MeetingSoftware />, div);
});
