import React, {Component} from 'react';
import { connect } from 'react-redux';

import './Tracking.css';

class Tracking extends Component {
    render() {
        return (
            <div className="p-grid">
                <div className="p-col-12">
                    <div className="card">
                        <h1>Tracking Page</h1>
                        <p>Use this page to start from scratch and place your custom content.</p>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
  return {
  };
}
export default connect(mapStateToProps)(Tracking); 