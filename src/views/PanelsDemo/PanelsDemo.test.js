import React from 'react';
import ReactDOM from 'react-dom';
import PanelsDemo from './PanelsDemo';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<PanelsDemo />, div);
});
