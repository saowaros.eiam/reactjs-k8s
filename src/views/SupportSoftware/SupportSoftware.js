import React, { Component } from 'react';
import { connect } from 'react-redux';
import { userActions } from '../../_actions';
import './SupportSoftware.css';

class SupportSoftware extends Component {
  onClickFile(value){
    if(value.file){
        const data = new FormData();
            data['user_action']     =  this.props.user.login_name;
            data['page']         =  'home';
            data['event']        =  value.event;
            data['score']        =  '1';
            data['filename']     =  value.file;
        this.props.dispatch(userActions.Tracking(data)); 
    }
  }
  render() {
    return (
      <div className="p-col-12 widget-body supp">
        <div className="dd ">
          <ol className="dd-list">
            <li className="dd-item bordered-danger row-item" data-id="7">
              <div className="dd-handle row-item">
                <i
                  className="pi pi-align-right"
                  style={{ fontSize: '1.3em' }}
                ></i>
                &nbsp;
                <span className="icon-load "
                 onClick={this.onClickFile.bind(this,{'file' : 'Self_Setup_JP1_Win7.exe'  ,'event' : 'download' })}>
                  <a
                    href="assets/install/Self_Setup_JP1_Win7.exe"
                    rel="noopener noreferrer"
                  >
                    <i
                      className="pi pi-download red wave in "
                      style={{ fontSize: '2em', position: 'absolute' }}
                    ></i>
                  </a>
                </span>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; JP1 Remote
                Support
              </div>
            </li>
            <li className="dd-item bordered-danger " data-id="8">
              <div className="dd-handle row-item">
                <i
                  className="pi pi-align-right"
                  style={{ fontSize: '1.3em' }}
                ></i>
                &nbsp;
                <span className="icon-load "
                 onClick={this.onClickFile.bind(this,{'file' : 'Start_Agent_JP1.exe'  ,'event' : 'download' })}>
                  <a
                    href="assets/install/Start_Agent_JP1.exe"
                    rel="noopener noreferrer"
                  >
                    <i
                      className="pi pi-download red wave in "
                      style={{ fontSize: '2em', position: 'absolute' }}
                    ></i>
                  </a>
                </span>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Start JP1
                Service
              </div>
            </li>
            <li className="dd-item bordered-danger" data-id="9">
              <div className="dd-handle row-item">
                <i className="pi pi-align-right"></i>
                &nbsp;
                <span className="icon-load "
                 onClick={this.onClickFile.bind(this,{'file' : 'Computer_Information.exe'  ,'event' : 'download' })}
                >
                  <a
                    href="assets/install/Computer_Information.exe"
                    rel="noopener noreferrer"
                  >
                    <i
                      className="pi pi-download red wave in "
                      style={{ fontSize: '2em', position: 'absolute' }}
                    ></i>
                  </a>
                </span>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Computer
                Information
              </div>
            </li>
          </ol>
          <ol className="dd-list">
            <li className="dd-item bordered-danger" data-id="1">
              <div className="dd-handle row-item">
                <i className="pi pi-list" style={{ fontSize: '1.3em' }}></i>{' '}
                &nbsp;&nbsp; Disable Firewall
              </div>
              <ol className="dd-list">
                <li className="dd-item bordered-inverse" data-id="2">
                  <div className="dd-handle">
                    &nbsp;
                    <span className="icon-load "
                    onClick={this.onClickFile.bind(this,{'file' : 'Self_Disable_FW.exe'  ,'event' : 'download' })}
                    >
                      <a
                        href="assets/install/Self_Disable_FW.exe"
                        rel="noopener noreferrer"
                      >
                        <i
                          className="pi pi-download red wave in "
                          style={{ fontSize: '2em', position: 'absolute' }}
                        ></i>
                      </a>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Windows 7
                  </div>
                </li>
                <li className="dd-item bordered-inverse" data-id="3">
                  <div className="dd-handle">
                    &nbsp;
                    <span className="icon-load "
                    onClick={this.onClickFile.bind(this,{'file' : 'Self_Disable_Firewall.exe'  ,'event' : 'download' })}
                    >
                      <a
                        href="assets/install/Self_Disable_Firewall.exe"
                        rel="noopener noreferrer"
                      >
                        <i
                          className="pi pi-download red wave in "
                          style={{
                            fontSize: '2em',
                            position: 'absolute',
                            top: '0.2em'
                          }}
                        ></i>
                      </a>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Windows 10
                  </div>
                </li>
              </ol>
            </li>
          </ol>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { authentication} = state;
  const { user  }               = authentication;
 return {
      user
  };
}
export default connect(mapStateToProps)(SupportSoftware);
