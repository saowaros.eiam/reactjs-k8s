import React from 'react';
import ReactDOM from 'react-dom';
import DataDemo from './DataDemo';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DataDemo />, div);
});
