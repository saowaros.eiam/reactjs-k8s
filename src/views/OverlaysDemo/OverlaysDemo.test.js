import React from 'react';
import ReactDOM from 'react-dom';
import OverlaysDemo from './OverlaysDemo';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<OverlaysDemo />, div);
});
