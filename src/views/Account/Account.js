import React, {Component} from 'react';
import { connect } from 'react-redux';
import { isAndroid } from 'react-device-detect';
import { userActions , notiActions} from '../../_actions';
import {InputText} from 'primereact/inputtext';
import {InputTextarea} from 'primereact/inputtextarea';
import {Card} from 'primereact/card';

// import {Button} from 'primereact/button';

import './Account.css';

class Account extends Component {
    constructor(props) {
        super(props);
        this.state = {
            d_user: {
                code_shop      : '',
                emp_id         : '',
                first_name     : '',
                last_name      : '',
                email          : '',
                phone_number   : '',  
                id             : '',  
            }
            ,display         : true
            ,submitted       : false
            ,msg_err_mail    : false
            ,image           : ''
        };

        this.handleChange       = this.handleChange.bind(this);
        this.handleSubmit       = this.handleSubmit.bind(this);
        this.checkEmail         = this.checkEmail.bind(this);
    }

    componentDidMount() {
        if(this.props.user.token && this.state.display){
            this.setState({ display : false });
            this.setState({  d_user: {
                    code_shop      : this.props.user.code_shop,
                    emp_id         : this.props.user.emp_id,
                    first_name     : this.props.user.first_name,
                    last_name      : this.props.user.last_name,
                    email_address  : this.props.user.email_address,
                    phone_number   : this.props.user.phone_number,  
                    id             : this.props.user.id  
                }
            });
        }

        if (this.props.user.token && this.state.image === '') {
                let image = '';
                if(this.props.bypass && this.props.bypass.image !== undefined){
                    image = this.props.bypass.image;
                    if(isAndroid === true){
                        image = 'assets/layout/images/profile.png';
                    }else{
                        //check an URL is valid or broken
                        let request = new XMLHttpRequest();
                        let status       = '';
                        
                        request.open("GET", image, true);
                        request.send();
                        request.onload = function(){
                            status = request.status;
                        }
                        if (request.readyState === 4) {
        
                            //check to see whether request for the file failed or succeeded
                            if ((status === 200) || (status === 0)) {
                                //your required operation on valid URL
                                image = this.props.bypass.image;
                            }else{
                                image = 'assets/layout/images/profile.png';
                            }
                        }
                    }
                    
    
                }else if(this.props.user.image !== undefined && this.props.user.image !== ''){
                    image = this.props.user.image;
                    if(this.props.user.type === 'External'){
                        image = 'assets/layout/images/profile.png';
                    }
                     //check an URL is valid or broken
                     let request = new XMLHttpRequest();
                     let status       = '';
     
                     request.open("GET", image, true);
                     request.send();
                     request.onload = function(){
                         status = request.status;
                     }
                     if(request.readyState === 4) {
     
                         //check to see whether request for the file failed or succeeded
                         if ((status === 200) || (status === 0)) {
                             //your required operation on valid URL
                             image = this.props.user.image;
                         }else{
                             image = 'assets/layout/images/profile.png';
                         }
                     }
                }else{
                    image = 'assets/layout/images/profile.png';
                }
              this.setState({ image : image});
        } 
    }
    
    componentDidUpdate(nextProps,prevProps) {
        if (nextProps.user !== undefined 
         && nextProps.user !== this.props.user
          && this.props.user.request_id 
          && this.state.display === true) {
            this.setState({ display : false });
            this.setState({  d_user: {
                    code_shop      : this.props.user.code_shop,
                    emp_id         : this.props.user.emp_id,
                    first_name     : this.props.user.first_name,
                    last_name      : this.props.user.last_name,
                    email_address  : this.props.user.email_address,
                    phone_number   : this.props.user.phone_number,  
                    id             : this.props.user.id  
                }
            });
        }

        if (this.props.user.token && this.state.image === '') {
              let image = '';
              if(this.props.bypass && this.props.bypass.image !== undefined){
                
                if(isAndroid === true){
                    image = 'assets/layout/images/profile.png';
                }else{
                    //check an URL is valid or broken
                    let request = new XMLHttpRequest();
                    let status       = '';
                    image = this.props.bypass.image;
                    request.open("GET", image, true);
                    request.send();
                    request.onload = function(){
                        status = request.status;
                    }

                    if (request.readyState === 4) {
                        //check to see whether request for the file failed or succeeded
                        if ((status === 200) || (status === 0)) {
                            //your required operation on valid URL
                            image = this.props.bypass.image;
                        }else{
                            image = 'assets/layout/images/profile.png';
                        }
                    }
                }

              }else if(this.props.user.image !== undefined && this.props.user.image !== ''){
                image = this.props.user.image;
                if(this.props.user.type === 'External'){
                    image = 'assets/layout/images/profile.png';
                }
                 //check an URL is valid or broken
                 let request = new XMLHttpRequest();
                 let status       = '';
 
                 request.open("GET", image, true);
                 request.send();
                 request.onload = function(){
                     status = request.status;
                 }
 
                 if (request.readyState === 4) {
                     //check to see whether request for the file failed or succeeded
                     if ((status === 200) || (status === 0)) {
                         //your required operation on valid URL
                         image = this.props.user.image;
                     }else{
                         image = 'assets/layout/images/profile.png';
                     }
                 }
              }else{
                image = 'assets/layout/images/profile.png';
              }
              this.setState({ image : image});
        } 
    }



    handleChange(event) {
        const { name, value  } = event.target;
        const { d_user } = this.state;
        this.setState({
            d_user: {
                ...d_user,
                [name]: value
            }
        },() => {
            if(name === 'email_address' ){
                let c_mail = this.checkEmail(value);
                if(c_mail === false)   
                    this.setState({msg_err_mail : true}); 
                else
                    this.setState({msg_err_mail : false}); 
            }
        });
    }

    checkEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.dispatch(notiActions.clear());
        this.setState({ submitted: true });
        const { d_user } = this.state;
        const Data = new FormData();
            Data['code_shop']       = d_user.code_shop;
            Data['emp_id']          = d_user.emp_id;
            Data['first_name']      = d_user.first_name;
            Data['last_name']       = d_user.last_name;
            Data['email_address']   = d_user.email_address;
            Data['phone_number']    = d_user.phone_number;
            Data['useraction']      = this.props.user.login_name;
            Data['token']           = this.props.user.token;
            Data['id']              = this.props.user.id;
        this.props.dispatch(userActions.updateAccount(Data),() => {
            this.setState({ submitted: false });
        });    
    }

    render() {
        const { user } = this.props;
        const {d_user , submitted ,msg_err_mail} = this.state;
        let header = (<div className="circle circleHr">
                <img  alt="profile" src={this.state.image} style={{width: '60px',height: '60px'}} className="imghr"/>
            </div>
        );
    
        return (
            <div className="layout-account p-fluid">
                <div className="p-grid">
                    <div className="p-col-12">
                        <div className="card card-w-title">
                            <h1>Account</h1>
                            <div className="p-grid layout-account-grid">
                                <div className="p-col-12 p-md-5">
                                    <Card title={user.first_name+`.`+user.last_name } header={header}  className="imghr">
                                        <p>{user.emp_id}</p>
                                        <p>{user.position}</p>
                                        <p>{user.department}</p>
                                        <p>{user.email_address}</p>
                                        <p>{user.phone_number}</p>
                                    </Card>
                                </div>
                                <div className="p-col-12 p-md-7 layout-account-connent">
                                    {/* <form name="form" onSubmit={this.handleSubmit}>  */}
                                   <form name="form" >
                                        <div className="p-grid form-group">
                                            <h1>edit account </h1>
                                            <div className="p-col-12">
                                                <div className="p-col-12 p-md-10">
                                                    <span className="md-inputfield">
                                                    <InputText   name="emp_id"  value={d_user.emp_id}  
                                                    onChange={this.handleChange}  className={(submitted && !user.emp_id ? ' p-error' : '')}/>
                                                        <label>Emp ID</label>
                                                    </span>
                                                </div>
                                                <div className="p-col-12 p-md-10">
                                                    <span className="md-inputfield">
                                                    <InputText   name="first_name"  value={d_user.first_name} 
                                                    onChange={this.handleChange}  className={(submitted && !user.first_name ? ' p-error' : '')} />
                                                        <label>First Name</label>
                                                    </span>
                                                </div>
                                                <div className="p-col-12 p-md-10">
                                                    <span className="md-inputfield">
                                                    <InputText   name="last_name"  value={d_user.last_name} 
                                                    onChange={this.handleChange}  className={(submitted && !user.last_name ? ' p-error' : '')} />
                                                        <label>Last Name</label>
                                                    </span>
                                                </div>
                                                <div className="p-col-12 p-md-10">
                                                    <span className="md-inputfield">
                                                    <InputTextarea  name="email_address" rows={2} cols={10} value={d_user.email_address} 
                                                        onChange={this.handleChange}  className={(submitted && !user.email_address ? ' p-error' : '')}  />
                                                        <label>Email</label>
                                                    </span>
                                                    { msg_err_mail  &&
                                                        <div className="help-block red">* E-mail ไม่ถูกต้อง</div>
                                                    }
                                                </div>
                                                <div className="p-col-12 p-md-10">
                                                    <span className="md-inputfield">
                                                    <InputText   name="phone_number"  value={d_user.phone_number} 
                                                    onChange={this.handleChange}  className={(submitted && !user.phone_number ? ' p-error' : '')} />
                                                        <label>Phone</label>
                                                    </span>
                                                </div>
                                                <div className="p-col-12 p-md-10">
                                                    <span className="md-inputfield">
                                                    <InputText   name="code_shop"  value={d_user.code_shop}  
                                                    onChange={this.handleChange}  className={(submitted && !user.code_shop ? ' p-error' : '')} />
                                                        <label>Code Shop</label>
                                                    </span>
                                                </div>
                                        </div>  
                                    </div> 
                                    {/* <div className="p-col-6 p-md-2 p-sm-2 p-sm-offset-6 p-md-offset-8"> 
                              
                                        <Button label="Save" className="p-button-danger" />
                                    </div> */}
                                   </form> 
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const {  authentication,  notification,users } = state;
    const { user } = authentication;
    const { bypass } = users;
    return {
        user,
        alert,
        notification,
        bypass
    };
}
export default connect(mapStateToProps)(Account); 