import React, { Component } from 'react';
import { connect } from 'react-redux';
import { TieredMenu } from 'primereact/tieredmenu';
// import ReactCssTooltip from 'react-css-tooltip';
// import 'react-css-tooltip/dist/index.css';

import './SoftwareInstaller.css';

class SoftwareInstaller extends Component {
  constructor() {
    super();
    this.state = {
      RAS: [
        {
          label: 'สำหรับ Windows',
          icon: 'pi pi-fw pi-save',
          command: e => {
            window.open(
              'http://www.truecorp.co.th/eus/install/Corporate/Self_PulseSecure.exe',
              '_blank'
            );
          }
        },
        {
          separator: true
        },
        {
          label: 'สำหรับ Mac (OSX)',
          icon: 'pi pi-fw pi-save ',
          command: e => {
            window.open(
              'http://www.truecorp.co.th/eus/install/Corporate/ps-pulse-mac-5.3r1.0-b587-installer.dmg',
              '_blank'
            );
          }
        },
        {
          separator: true
        },
        {
          label: 'คู่มือการติดตั้ง',
          icon: 'pi pi-fw pi-file',
          command: e => {
            window.open(
              'http://www.truecorp.co.th/eus/install/Corporate/RAS%20VPN%20Services%20%20Manual_v12.pdf',
              '_blank'
            );
          }
        },
        {
          separator: true
        },
        {
          label: 'สำหรับเครื่องไม่จอยโดเมน',
          icon: 'pi pi-fw pi-save ',
          command: e => {
            window.open(
              'http://selfinstall/EUS/Corporate/PulseSecure.exe',
              '_blank'
            );
          }
        }
      ],
      SAP: [
        {
          label: 'ติดตั้งโปรแกรม Sap GUI 750',
          icon: 'pi pi-fw pi-save',
          command: e => {
            window.location = 'assets/install/Self_SAPGUI_750.exe';
          }
        },
        {
          separator: true
        },
        {
          label: 'คู่มือติดตั้ง',
          icon: 'pi pi-fw pi-file',
          command: e => {
            window.open(
              'assets/file/%E0%B8%82%E0%B8%B1%E0%B9%89%E0%B8%99%E0%B8%95%E0%B8%AD%E0%B8%99%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%B4%E0%B8%94%E0%B8%95%E0%B8%B1%E0%B9%89%E0%B8%87%20SAP%20GUI%20750.pdf',
              '_blank'
            );
          }
        },
        {
          separator: true
        },
        {
          label: 'โปรแกรม Sap GUI 750 Patch12 (แก้ปัญหาภาษาไทย)',
          icon: 'pi pi-fw pi-save',
          command: e => {
            window.location = 'assets/install/Self_Sap_GUI_7.5_Patch12.exe';
          }
        },
        {
          separator: true
        },
        {
          label: 'คู่มือเปลี่ยน font เมนูให้เป็น ไทยหน้า SAP GUI',
          icon: 'pi pi-fw pi-file',
          command: e => {
            window.open(
              'assets/file/change%20font%20%E0%B9%80%E0%B8%A1%E0%B8%99%E0%B8%B9%E0%B9%83%E0%B8%AB%E0%B9%89%E0%B9%80%E0%B8%9B%E0%B9%87%E0%B8%99%20%E0%B9%84%E0%B8%97%E0%B8%A2%E0%B8%AB%E0%B8%99%E0%B9%89%E0%B8%B2%20SAP%20GUI.pdf',
              '_blank'
            );
          }
        },
        {
          separator: true
        },
        {
          label: 'สำหรับผู้ที่ใช้ BW.เท่านั้น',
          icon: 'pi pi-fw pi-save',
          items: [
            {
              label: 'ติดตั้งโปรแกรม Sap GUI 750 Business Explorer',
              icon: 'pi pi-fw pi-globe',
              command: e => {
                window.location = 'assets/install/Self_SAP_GUI_7.5_Bi.exe';
              }
            }
          ]
        }
      ],
      avaya: [
        {
          label: 'ติดตั้งโปรแกรม',
          icon: 'pi pi-fw pi-save',
          command: e => {
            window.location = 'assets/install/Self_Avaya_One-X.exe';
          }
        },
        {
          separator: true
        },
        {
          label: 'คู่มือติดตั้ง',
          icon: 'pi pi-fw pi-file',
          command: e => {
            window.open('assets/file/Manual_one-x_softphone.pdf', '_blank');
          }
        }
      ]
    };
  }

  render() {
    return (
      <React.Fragment>
        <div className="p-col-12 softIns widget-body">
          {/* Corporate Software */}
          <h6 className="row-title before-darkorange">Back Office</h6>
          <div className="p-grid">
            <TieredMenu
              className="width-item"
              model={this.state.SAP}
              popup={true}
              ref={el => (this.menuSAP = el)}
              id="overlay_SAP"
            />
            <div
              className="p-col-12 p-md-2"
              onClick={event => this.menuSAP.toggle(event)}
              aria-haspopup={true}
              aria-controls="overlay_SAP"
            >
              <div className="p-col-12 pad-top-1">
                <div className="ahvr-circle ahvr-animated animated-box effect-rotate ">
                  <div className="spinner"></div>
                  <img
                    className="img-fluid image"
                    src="assets/images/sap.png"
                    alt="Avatar"
                  />
                  <div className="overlay animated-bounce">
                    <div className="middle"></div>
                  </div>
                </div>
              </div>
              <div className="p-col-12">
                <div className="name-app">
                  <div className="name-app-text">SAP GUI 750</div>
                </div>
              </div>
            </div>

            {/* <TieredMenu style={{width:'auto'}} model={this.state.RAS} popup={true} ref={el => this.menuRAS = el} id="overlay_RAS" />
                    <div className="p-col-12 p-md-3 p-lg-3"  onClick={(event)=>this.menuRAS.toggle(event)} aria-haspopup={true} aria-controls="overlay_RAS">
                        <div className="p-col-12 p-md-12 p-lg-12 pad-top-1">
                            <div className="ahvr-circle ahvr-animated animated-box effect-rotate ">
                                <div className="spinner"></div>
                                <img className="img-fluid image" src="assets/images/pulse_secure.png" alt="Avatar" />
                                <div className="overlay animated-bounce">
                                    <div className="middle">
                                       
                                    </div>
                                </div>
                            </div>
                        </div>       
                        <div className="p-col-12 p-md-12 p-lg-12" >   
                            <div className="name-app">
                                <div className="name-app-text">Pulse Secure</div>
                            </div>
                        </div>
                    </div> */}

            {/* <div className="p-col-12 p-md-3 p-lg-3" >
                        <a target="_blank" rel="noopener noreferrer" href="http://www.truecorp.co.th/eus/install/Standard/Self_Setup_JP1_Win7.exe" >
                            <div className="p-col-12 p-md-12 p-lg-12 pad-top-1">
                                <div className="ahvr-circle ahvr-animated animated-box effect-rotate ">
                                    <div className="spinner"></div>
                                    <img className="img-fluid image" src="assets/images/jp1.png" alt="Avatar" />
                                    <div className="overlay animated-bounce">
                                        <div className="middle">
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>       
                            <div className="p-col-12 p-md-12 p-lg-12" >   
                                <div className="name-app">
                                    <div className="name-app-text">JP1</div>
                                </div>
                            </div>
                        </a>
                    </div> */}
          </div>

          <br />
          <br />
          <br />
        </div>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {};
}
export default connect(mapStateToProps)(SoftwareInstaller);
