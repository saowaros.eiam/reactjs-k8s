import React from 'react';
import ReactDOM from 'react-dom';
import SoftwareInstaller from './SoftwareInstaller';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SoftwareInstaller />, div);
});
