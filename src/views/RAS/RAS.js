import React, { Component } from 'react';
import { connect } from 'react-redux';
import { userActions } from '../../_actions';
import {Button} from 'primereact/button';
import { TieredMenu } from 'primereact/tieredmenu';
import './RAS.css';

class RAS extends Component {
  constructor() {
    super();
    this.state = {
      issue: [
        {
          label: 'VPN - พบปัญหาการ Connect หรือใช้งานแล้วหลุดบ่อย',
          icon: 'pi pi-file red',
          command: e => {
            window.open('assets/file/แก้ไขปัญหาใช้งานPulseSecure.pdf', '_blank');
          }
        },
        {
          separator: true
        },
        {
          label: 'VPN - RAS OTP - กดรับรหัส OTP ไม่สำเร็จ (95555)',
          icon: 'pi pi-file red',
          command: e => {
            window.open('assets/file/กรณีกดรับรหัส OTP(95555) ไม่สำเร็จ.pdf', '_blank');
          }
        },
        {
          separator: true
        },
        {
          label: 'VPN - RAS OTP - กดรับรหัส OTP ได้รับ SMS ล่าช้า (95555)',
          icon: 'pi pi-file red',
          command: e => {
            window.open('assets/file/กรณีกดรับรหัส OTP(95555) ไม่สำเร็จ.pdf', '_blank');
          }
        }

      ],
      InstallRAS: [
        {
          label: 'ติดตั้ง RAS Crisis',
          icon: 'pi pi-download red',
          url: 'assets/install/Self_PulseSecure_V9_Clear_GUID.exe'
        },
        {
          separator: true
        },
        {
          label: 'ติดตั้ง RAS OTP',
          icon: 'pi pi-download red',
          url: 'assets/install/SelfInstall_PulseSecure_V10_OTP.exe'
        }
      ]
    };
  }
  onClickFile(value){
    if(value.file){
        const data = new FormData();
            data['user_action']     =  this.props.user.login_name;
            data['page']         =  'home';
            data['event']        =  value.event;
            data['score']        =  '1';
            data['filename']     =  value.file;
        this.props.dispatch(userActions.Tracking(data)); 
    }
  }

  render() {
    return (
      <div className="p-col-12 widget-body ras">
         <TieredMenu  className=" width-item"  model={this.state.issue}  popup={true} ref={el => (this.menuissue = el)}  id="overlay_issue"/>
         <TieredMenu  className=" width-item"  model={this.state.InstallRAS}  popup={true} ref={el => (this.menuInstallRAS = el)}  id="overlay_InstallRAS"/>
           
        <div className="dd ">
        
          <ol className="dd-list">
            <li className="dd-item bordered-danger" data-id="1">
              <div className="dd-handle row-item">
                <i className="pi pi-list" style={{ fontSize: '1.3em' }}></i>
                <span className="icon-load " 
                onClick={this.onClickFile.bind(this,{'file' : 'Ras_Form_and_BYOD.pdf'  ,'event' : 'read-file' })}
                >
                  <a  href="assets/file/Ras_Form_and_BYOD.pdf"  target="_blank"  rel="noopener noreferrer" >
                    <i  className="pi pi-file red "  style={{ fontSize: '2em', position: 'absolute' }} ></i>
                  </a>
                </span>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;การขอเข้าใช้งาน
                <span className="txt-bol"> Work From Home </span>
              </div>
              
              <ol className="dd-list">
                <li className="dd-item bordered-inverse" data-id="2">
                  <div className="dd-handle">
                    <span className="icon-load "
                      onClick={this.onClickFile.bind(this,{'file' : 'https://iform.truecorp.co.th/jw/web/userview/rasRequest/v/_/newRequest' ,'event' : 'web' })}
                     >
                      <a
                        href="https://iform.truecorp.co.th/jw/web/userview/rasRequest/v/_/newRequest"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <i
                          className="pi pi-globe red wave in"
                          style={{ fontSize: '2em', position: 'absolute' }}
                        ></i>
                      </a>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ขอสร้าง User
                  </div>
                </li>

                <li className="dd-item bordered-inverse" data-id="3">
                  <div className="dd-handle">
                    <span className="icon-load "
                    onClick={this.onClickFile.bind(this,{'file' : 'https://iform.truecorp.co.th/jw/web/userview/rasRequest/v/_/newRequest'  ,'event' : 'web' })}
                    >
                      <a
                        href="https://iform.truecorp.co.th/jw/web/userview/rasRequest/v/_/newRequest"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <i
                          className="pi pi-globe red wave in"
                          style={{ fontSize: '2em', position: 'absolute' }}
                        ></i>
                      </a>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    ขอนำเครื่องส่วนตัวมาใช้งาน
                  </div>
                </li>

                <li className="dd-item bordered-inverse" data-id="4">
                  <div className="dd-handle">
                    <span className="icon-load "
                    onClick={this.onClickFile.bind(this,{'file' : 'RAS Crisis WFH.pdf'  ,'event' : 'read-file' })}
                    >
                      <a
                        href="assets/file/RAS Crisis WFH.pdf"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <i
                          className="pi pi-file red wave in "
                          style={{ fontSize: '2em', position: 'absolute' }}
                        ></i>
                      </a>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การตั้งค่า RAS
                    สำหรับใช้งาน (RAS Crisis)
                  </div>
                </li>
                <li className="dd-item bordered-inverse" data-id="4">
                  <div className="dd-handle">
                    <span className="icon-load "
                    onClick={this.onClickFile.bind(this,{'file' : 'RAS OTP WFH.pdf'  ,'event' : 'read-file' })}
                    >
                      <a
                        href="assets/file/RAS OTP WFH.pdf"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <i
                          className="pi pi-file red wave in "
                          style={{ fontSize: '2em', position: 'absolute' }}
                        ></i>
                      </a>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การตั้งค่า RAS
                    สำหรับใช้งาน (RAS OTP)
                  </div>
                </li>
                <li className="dd-item bordered-inverse" data-id="5">
                  <div className="dd-handle">
                    <i className="pi pi-list" style={{ fontSize: '1.3em' }}></i>{' '}
                    &nbsp;&nbsp; กรณีไม่ใช่พนักงาน True{' '}
                  </div>
                  <ol className="dd-list">
                    <li className="dd-item bordered-inverse" data-id="5">
                      <div className="dd-handle">
                        <span className="icon-load "
                        onClick={this.onClickFile.bind(this,{'file' : 'Memo-BYOD-wRisk.docx'  ,'event' : 'read-file' })}
                        >
                          <a
                            href="assets/file/Memo-BYOD-wRisk.docx"
                            rel="noopener noreferrer"
                          >
                            <i
                              className="pi pi-file red wave in "
                              style={{ fontSize: '2em', position: 'absolute' }}
                            ></i>
                          </a>
                        </span>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Memo
                        สำหรับการขออนุมัติ
                      </div>
                    </li>
                    <li className="dd-item bordered-inverse" data-id="6">
                      <div className="dd-handle">
                        <span className="icon-load "
                        onClick={this.onClickFile.bind(this,{'file' : 'FM-01-RAS for Crisis User Request(w RiskAssessment)_staff-templateVer1.0Rev18.2.xlsx'  ,'event' : 'read-file' })}
                        >
                          <a
                            href="assets/file/FM-01-RAS for Crisis User Request(w RiskAssessment)_staff-templateVer1.0Rev18.2.xlsx"
                            rel="noopener noreferrer"
                          >
                            <i
                              className="pi pi-file red wave in "
                              style={{ fontSize: '2em', position: 'absolute' }}
                            ></i>
                          </a>
                        </span>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        รายละเอียดประกอบการขออนุมัติ
                      </div>
                    </li>
                    <li className="dd-item bordered-inverse" data-id="6">
                      <div className="dd-handle">
                        <span className="icon-load "
                        onClick={this.onClickFile.bind(this,{'file' : 'ลงทะเบียนขอ_SMS_OTP_Intranet_Internet.pdf'  ,'event' : 'read-file' })}
                        >
                          <a
                            href="assets/file/ลงทะเบียนขอ_SMS_OTP_Intranet_Internet.pdf"
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            <i
                              className="pi pi-file red wave in "
                              style={{ fontSize: '2em', position: 'absolute' }}
                            ></i>
                          </a>
                        </span>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        การลงทะเบียนหมายเลขโทรศัพท์เพื่อรับ OTP **หมายเลข TMV-H
                        ของพนักงานเท่านั้น
                      </div>
                    </li>
                  </ol>
                </li>
              
              </ol>
            </li>
          </ol>
          <br />
          
          <ol className="dd-list">
            <li className="dd-item  bordered-danger" data-id="7">
   
            
              <div className="dd-handle row-item " >
                
                  <i className="pi pi-list" style={{ fontSize: '1.3em' }}></i>{' '}
                  &nbsp;&nbsp; โปรแกรมสำหรับติดตั้งเพื่อใช้งาน{' '}
                  <span className="txt-bol"> Work From Home (RAS/VPN)</span>

                  &nbsp;&nbsp; 
                  <a href="https://terra.truecorp.co.th/getotp"
                    target="_blank"rel="noopener noreferrer" >
                    <Button label="Request OTP via Browser" 
                    className="p-button btn-lnk-ras"   onClick={this.onClickFile.bind(this,{'file' : 'https://terra.truecorp.co.th/getotp'  ,'event' : 'web' })}/>
                  </a>
                  &nbsp;&nbsp; 
                  
                   <span> 
                     <Button  onClick={event => this.menuissue.toggle(event)}   aria-haspopup={true}
                      aria-controls="overlay_issue"
                    label=" ปัญหาที่พบบ่อย" className="p-button btn-lnk-issue" 
                    style={{width: '9em',fontSize: '14px'}}  /> 
                   
                    </span>
                   
                </div>
              
              <ol className="dd-list">
                <li className="dd-item bordered-inverse" data-id="5">
                  <div className="dd-handle" >
                    {/* <span className="icon-load "
                    onClick={this.onClickFile.bind(this,{'file' : 'Self_PulseSecure_V9_Clear_GUID.exe'  ,'event' : 'download' })}
                    >
                      <a
                        href="assets/install/Self_PulseSecure_V9_Clear_GUID.exe"
                        rel="noopener noreferrer"
                      >
                        <i
                          className="pi pi-download red wave in "
                          style={{ fontSize: '2em', position: 'absolute' }}
                        ></i>
                      </a>
                    </span> */}
                    <span  onClick={event => this.menuInstallRAS.toggle(event)}   aria-haspopup={true}
                      aria-controls="overlay_InstallRAS">
                    <i 
                          className="pi pi-download red wave in "
                          style={{ fontSize: '2em', position: 'absolute' }}
                        ></i>
                    &nbsp;&nbsp;&nbsp;
                    {/* <span className="icon-load "
                    onClick={this.onClickFile.bind(this,{'file' : 'RAS_Crisis_WFH.pdf'  ,'event' : 'read-file' })}
                    >
                      <a
                        href="assets/file/RAS_Crisis_WFH.pdf"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <i
                          className="pi pi-file red wave in "
                          style={{ fontSize: '2em', position: 'absolute' }}
                        ></i>
                      </a>
                    </span> */}
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; สำหรับ{' '}
                    <span className="txt-bol">Windows </span>
                    </span>
                  </div>
                </li>
                
                <li className="dd-item bordered-inverse" data-id="6">
                  <div className="dd-handle">
                    <span className="icon-load "
                    onClick={this.onClickFile.bind(this,{'file' : 'ps-pulse-mac-5.3r1.0-b587-installer.dmg'  ,'event' : 'download' })}
                    >
                      <a
                        href="assets/install/ps-pulse-mac-5.3r1.0-b587-installer.dmg"
                        rel="noopener noreferrer"
                      >
                        <i
                          className="pi pi-download red wave in "
                          style={{ fontSize: '2em', position: 'absolute' }}
                        ></i>
                      </a>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <span className="icon-load "
                     onClick={this.onClickFile.bind(this,{'file' : 'RAS_Crisis_WFH.pdf'  ,'event' : 'read-file' })}
                    >
                      <a
                        href="assets/file/RAS_Crisis_WFH.pdf"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <i
                          className="pi pi-file red wave in "
                          style={{ fontSize: '2em', position: 'absolute' }}
                        ></i>
                      </a>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; สำหรับ{' '}
                    <span className="txt-bol">Mac (OSX)</span>
                  </div>

                </li>
              </ol>
            </li>
          </ol>
        
         
          <br />
          <ol className="dd-list">
            <li className="dd-item  bordered-danger" data-id="7">
              <div className="dd-handle row-item ">

                <i className="pi pi-list" style={{ fontSize: '1.3em' }}></i>{' '}
                <span className="icon-load "
                onClick={this.onClickFile.bind(this,{'file' : 'https://iform.truecorp.co.th/jw/web/userview/ITO_Sec_ResetPassword/ITO_Sec_Reset_Password/_/newRequest'  ,'event' : 'web' })}
                >
                      <a href="https://iform.truecorp.co.th/jw/web/userview/ITO_Sec_ResetPassword/ITO_Sec_Reset_Password/_/newRequest"   target="_blank"   rel="noopener noreferrer" >
                        <i   className="pi pi-globe red wave in"   style={{ fontSize: '2em', position: 'absolute' }} ></i>
                      </a>
                    </span> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; 
                    กรณีลืม password เข้าใช้งานระบบต่างๆ ทำการ Reset password ผ่าน iForm
              </div>
            </li>
          </ol>  

           <br />
          <ol className="dd-list">

            <li className="dd-item  bordered-danger" data-id="7">
              <div className="dd-handle row-item ">

                <i className="pi pi-list" style={{ fontSize: '1.3em' }}></i>{' '}
                {/* <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : 'วิธีติดตั้ง Passthru.pdf'  ,'event' : 'read-file' })} >
                    <a href="assets/file/วิธีติดตั้ง Passthru.pdf"   target="_blank"   rel="noopener noreferrer" >
                      <i   className="pi pi-file red wave in"   style={{ fontSize: '2em', position: 'absolute' }} ></i>
                    </a>
                </span> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;  */}
                คู่มือการติดตั้ง Passthru  
                <span className="txt-bol"> (Lotus Notes) </span>
              </div>
              <ol className="dd-list">
                <li className="dd-item bordered-inverse" data-id="5">
                  <div className="dd-handle" >
                    <span className="icon-load " onClick={this.onClickFile.bind(this,{'file' : 'LTN PT_1.pdf'  ,'event' : 'read-file' })} >
                      <a href="assets/file/LTN PT_1.pdf" target="_blank"   rel="noopener noreferrer" >
                        <i className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }} ></i>
                      </a>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; สำหรับ{' '}
                    <span className="txt-bol">Online </span>
                   
                  </div>
                </li>
                
                <li className="dd-item bordered-inverse" data-id="6">
                  <div className="dd-handle">
                    <span className="icon-load " onClick={this.onClickFile.bind(this,{'file' : 'วิธีติดตั้ง Passthru.pdf'  ,'event' : 'read-file' })} >
                      <a href="assets/file/วิธีติดตั้ง Passthru.pdf" target="_blank"   rel="noopener noreferrer" >
                        <i className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }} ></i>
                      </a>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; สำหรับ{' '}
                    <span className="txt-bol">Offline</span>
                  </div>

                </li>
              </ol>
            </li>
          </ol>    
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { authentication} = state;
  const { user  }               = authentication;
 return {
      user
  };
}
export default connect(mapStateToProps)(RAS);
