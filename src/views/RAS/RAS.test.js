import React from 'react';
import ReactDOM from 'react-dom';
import RAS from './RAS';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<RAS />, div);
});
