import React, { Component } from 'react';
import { connect } from 'react-redux';
import { userActions } from '../../_actions';
import {Button} from 'primereact/button';
import { TieredMenu } from 'primereact/tieredmenu';
import './MicrosoftOutlook.css';

class MicrosoftOutlook extends Component {
  constructor() {
    super();
    this.state = {
      issue: [
        {
          label: 'VPN - พบปัญหาการ Connect หรือใช้งานแล้วหลุดบ่อย',
          icon: 'pi pi-file red',
          command: e => {
            window.open('assets/file/แก้ไขปัญหาใช้งานPulseSecure.pdf', '_blank');
          }
        },
        {
          separator: true
        },
        {
          label: 'VPN - RAS OTP - กดรับรหัส OTP ไม่สำเร็จ (95555)',
          icon: 'pi pi-file red',
          command: e => {
            window.open('assets/file/กรณีกดรับรหัส OTP(95555) ไม่สำเร็จ.pdf', '_blank');
          }
        },
        {
          separator: true
        },
        {
          label: 'VPN - RAS OTP - กดรับรหัส OTP ได้รับ SMS ล่าช้า (95555)',
          icon: 'pi pi-file red',
          command: e => {
            window.open('assets/file/กรณีกดรับรหัส OTP(95555) ไม่สำเร็จ.pdf', '_blank');
          }
        }

      ],
      InstallRAS: [
        {
          label: 'ติดตั้ง RAS Crisis',
          icon: 'pi pi-download red',
          url: 'assets/install/Self_PulseSecure_V9_Clear_GUID.exe'
        },
        {
          separator: true
        },
        {
          label: 'ติดตั้ง RAS OTP',
          icon: 'pi pi-download red',
          url: 'assets/install/SelfInstall_PulseSecure_V10_OTP.exe'
        }
      ]
    };
  }
  onClickFile(value){
    if(value.file){
        const data = new FormData();
            data['user_action']     =  this.props.user.login_name;
            data['page']         =  'home';
            data['event']        =  value.event;
            data['score']        =  '1';
            data['filename']     =  value.file;
        this.props.dispatch(userActions.Tracking(data)); 
    }
  }

  render() {
    return (
      <div className="p-col-12 widget-body ms-outlook"> 
        <div className="dd ">
        
          <ol className="dd-list">
            {/* title on Website */}
            <li className="dd-item bordered-danger" data-id="1">
              <div className="dd-handle row-item">
                <i className="pi pi-list" style={{ fontSize: '1.3em' }}></i>
                &nbsp;&nbsp; คู่มือการใช้งานผ่านเว็บไซต์
                <span className="txt-bol"> (on Website) </span>
              </div>
              
              <ol className="dd-list">

                <li className="dd-item bordered-inverse" data-id="1-1">
                  <div className="dd-handle">
                    <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '1 เอกสารใช้งาน Outlook on web_วิธีเข้าใช้งานOutlook on web (OWA).pdf'  ,'event' : 'read-file' })}>
                      <a href="assets/file/MicrosoftOutlook/manual_on_website/1 เอกสารใช้งาน Outlook on web_วิธีเข้าใช้งานOutlook on web (OWA).pdf" target="_blank" rel="noopener noreferrer" >
                        <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                      </a>
                    </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ลงชื่อเข้าใช้อีเมล์ใน Outlook web app (OWA)
                  </div>
                </li>

                <li className="dd-item bordered-inverse" data-id="1-2">
                  <div className="dd-handle">
                    <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '2 เอกสารใช้งาน Outlook on web_วิธีสร้างโฟลเดอร์ และแสดงโฟเดอร์ที่จำเป็นในส่วนของ Folder Favorites.pdf'  ,'event' : 'read-file' })}>
                      <a href="assets/file/MicrosoftOutlook/manual_on_website/2 เอกสารใช้งาน Outlook on web_วิธีสร้างโฟลเดอร์ และแสดงโฟเดอร์ที่จำเป็นในส่วนของ Folder Favorites.pdf" target="_blank" rel="noopener noreferrer" >
                        <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                      </a>
                    </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; สร้างโฟลเดอร์ และแสดงโฟลเดอร์ที่จำเป็นในส่วนของ Folder Favorites
                  </div>
                </li>

                <li className="dd-item bordered-inverse" data-id="1-3">
                  <div className="dd-handle">
                    <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '3 เอกสารใช้งาน Outlook on web_วิธีและเทคนิคการค้นหาอีเมล.pdf'  ,'event' : 'read-file' })}>
                      <a href="assets/file/MicrosoftOutlook/manual_on_website/3 เอกสารใช้งาน Outlook on web_วิธีและเทคนิคการค้นหาอีเมล.pdf" target="_blank" rel="noopener noreferrer" >
                        <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                      </a>
                    </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; เทคนิคการค้นหาอีเมล์
                  </div>
                </li>

                <li className="dd-item bordered-inverse" data-id="1-4">
                  <div className="dd-handle">
                    <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '4 เอกสารใช้งาน Outlook on web_วิธีการใช้งานอีเมล (Email).pdf'  ,'event' : 'read-file' })}>
                      <a href="assets/file/MicrosoftOutlook/manual_on_website/4 เอกสารใช้งาน Outlook on web_วิธีการใช้งานอีเมล (Email).pdf" target="_blank" rel="noopener noreferrer" >
                        <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                      </a>
                    </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การใช้งานอีเมล์ (E-mail)
                  </div>
                </li>
                
                <li className="dd-item bordered-inverse" data-id="1-5">
                  <div className="dd-handle">
                    <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '5 เอกสารใช้งาน Outlook on web_วิธีการแนบไฟล์ใน Outlook OWA.pdf'  ,'event' : 'read-file' })}>
                      <a href="assets/file/MicrosoftOutlook/manual_on_website/5 เอกสารใช้งาน Outlook on web_วิธีการแนบไฟล์ใน Outlook OWA.pdf" target="_blank" rel="noopener noreferrer" >
                        <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                      </a>
                    </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การแนบไฟล์ใน Outlook OWA
                  </div>
                </li>

                <li className="dd-item bordered-inverse" data-id="1-6">
                  <div className="dd-handle">
                    <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '7 เอกสารใช้งาน Outlook on web_วิธีปรับเปลี่ยนสถานะเมล (อ่านและไม่อ่าน).pdf'  ,'event' : 'read-file' })}>
                      <a href="assets/file/MicrosoftOutlook/manual_on_website/7 เอกสารใช้งาน Outlook on web_วิธีปรับเปลี่ยนสถานะเมล (อ่านและไม่อ่าน).pdf" target="_blank" rel="noopener noreferrer" >
                        <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                      </a>
                    </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ปรับเปลี่ยนสถานะอีเมล์ (อ่าน / ไม่อ่าน)
                  </div>
                </li>

                <li className="dd-item bordered-inverse" data-id="1-7">
                  <div className="dd-handle">
                    <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '8 เอกสารใช้งาน Outlook on web_วิธีการใช้ Filter กรองอีเมล.pdf'  ,'event' : 'read-file' })}>
                      <a href="assets/file/MicrosoftOutlook/manual_on_website/8 เอกสารใช้งาน Outlook on web_วิธีการใช้ Filter กรองอีเมล.pdf" target="_blank" rel="noopener noreferrer" >
                        <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                      </a>
                    </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การใช้ Filter กรองอีเมล์
                  </div>
                </li>

                <li className="dd-item bordered-inverse" data-id="1-8">
                  <div className="dd-handle">
                    <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '9 เอกสารใช้งาน Outlook on web_วิธีการติดตามอีเมลที่ส่งออก (Tracking E-mail).pdf'  ,'event' : 'read-file' })}>
                      <a href="assets/file/MicrosoftOutlook/manual_on_website/9 เอกสารใช้งาน Outlook on web_วิธีการติดตามอีเมลที่ส่งออก (Tracking E-mail).pdf" target="_blank" rel="noopener noreferrer" >
                        <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                      </a>
                    </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การติดตามอีเมล์อิเล็กทรอนิกส์ (Tracking E-mail)
                  </div>
                </li>

                <li className="dd-item bordered-inverse" data-id="1-9">
                  <div className="dd-handle">
                    <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '11 เอกสารใช้งาน Outlook on web_วิธีกำหนดอีเมลให้ตอบกลับอัตโนมัติ (Automatic replies _Out of Office Replies).pdf'  ,'event' : 'read-file' })}>
                      <a href="assets/file/MicrosoftOutlook/manual_on_website/11 เอกสารใช้งาน Outlook on web_วิธีกำหนดอีเมลให้ตอบกลับอัตโนมัติ (Automatic replies _Out of Office Replies).pdf" target="_blank" rel="noopener noreferrer" >
                        <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                      </a>
                    </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การกำหนดอีเมล์ให้ตอบกลับอัตโนมัติ (Automatic replies/Out of Office Replies)
                  </div>
                </li>

                <li className="dd-item bordered-inverse" data-id="1-10">
                  <div className="dd-handle">
                    <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '12 เอกสารใช้งาน Outlook on web_วิธีการตั้งค่าลายเซ็น (Signature).pdf'  ,'event' : 'read-file' })}>
                      <a href="assets/file/MicrosoftOutlook/manual_on_website/12 เอกสารใช้งาน Outlook on web_วิธีการตั้งค่าลายเซ็น (Signature).pdf" target="_blank" rel="noopener noreferrer" >
                        <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                      </a>
                    </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การตั้งค่าลายเซ็นต์ (Signature)
                  </div>
                </li>

                <li className="dd-item bordered-inverse" data-id="1-11">
                  <div className="dd-handle">
                    <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '14 เอกสารใช้งาน Outlook on web_วิธีการตั้งค่า การตั้งกฎ และเงื่อนไข (Rules and Condition).pdf'  ,'event' : 'read-file' })}>
                      <a href="assets/file/MicrosoftOutlook/manual_on_website/14 เอกสารใช้งาน Outlook on web_วิธีการตั้งค่า การตั้งกฎ และเงื่อนไข (Rules and Condition).pdf" target="_blank" rel="noopener noreferrer" >
                        <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                      </a>
                    </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การตั้งค่า การตั้งกฎ และเงื่อนไข (Rules and Condition)
                  </div>
                </li>

                <li className="dd-item bordered-inverse" data-id="1-12">
                  <div className="dd-handle">
                    <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '20 เอกสารใช้งาน Outlook on web_วิธีการใช้ People (Contact).pdf'  ,'event' : 'read-file' })}>
                      <a href="assets/file/MicrosoftOutlook/manual_on_website/20 เอกสารใช้งาน Outlook on web_วิธีการใช้ People (Contact).pdf" target="_blank" rel="noopener noreferrer" >
                        <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                      </a>
                    </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การสร้าง People (Contact)
                  </div>
                </li>
              </ol>
            
            </li>
          
            {/* title on Mobile Application */}
            <li className="dd-item bordered-danger" data-id="2">
              <div className="dd-handle row-item">
                <i className="pi pi-list" style={{ fontSize: '1.3em' }}></i>
                &nbsp;&nbsp; คู่มือการใช้งานผ่านแอปพลิเคชั่น 
                <span className="txt-bol"> (on Mobile Application) </span>
              </div>
              
              <ol className="dd-list">
                {/* ios */}
                <li className="dd-item bordered-inverse" data-id="2-1">
                  <div className="dd-handle">
                    <i className="pi pi-list" style={{ fontSize: '1.3em' }}></i>{' '}
                    <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '1 การเข้าใช้งานผ่าน IOS.pdf'  ,'event' : 'read-file' })}>
                      <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile IOS/1 การเข้าใช้งานผ่าน IOS.pdf" target="_blank" rel="noopener noreferrer" >
                        <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                      </a>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การเข้าใช้งานผ่าน 
                    <span className="txt-bol"> IOS </span>
                  </div>
                  <ol className="dd-list">
                    <li className="dd-item bordered-inverse" data-id="2-1-1">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '2 องค์ประกอบ และส่วนประกอบหน้าจอโปรแกรม.pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile IOS/2 องค์ประกอบ และส่วนประกอบหน้าจอโปรแกรม.pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; องค์ประกอบ และส่วนประกอบหน้าจอโปรแกรม
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-2">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '3 การแสดงโฟลเดอร์ใน Outlook mobile.pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile IOS/3 การแสดงโฟลเดอร์ใน Outlook mobile.pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การแสดงโฟลเดอร์ใน Outlook Mobile
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-3">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '4 การตั้งค่าต่างๆ ให้กับระบบ Outlook mobile.pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile IOS/4 การตั้งค่าต่างๆ ให้กับระบบ Outlook mobile.pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การตั้งค่าต่างๆ ให้กับระบบ Outlook Mobile
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-4">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '5 สร้างโฟลเดอร์ และแสดงโฟลเดอร์ที่จำเป็น.pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile IOS/5 สร้างโฟลเดอร์ และแสดงโฟลเดอร์ที่จำเป็น.pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; สร้างโฟลเดอร์ และแสดงโฟลเดอร์ที่จำเป็น
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-5">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '6 การค้นหาอีเมล จดหมาย หรือไฟล์ที่อยู่ใน Outlook.pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile IOS/6 การค้นหาอีเมล จดหมาย หรือไฟล์ที่อยู่ใน Outlook.pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การค้นหาอีเมล์ จดหมาย หรือไฟล์ที่อยู่ใน Outlook
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-6">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '7 การใช้งานอีเมล (Email).pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile IOS/7 การใช้งานอีเมล (Email).pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การใช้งานอีเมล์ (E-mail)
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-7">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '8 การแนบไฟล์ใน Outlook.pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile IOS/8 การแนบไฟล์ใน Outlook.pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การแนบไฟล์ใน Outlook
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-8">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '9 ปรับเปลี่ยนสถานะเมล (อ่าน-ไม่อ่าน).pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile IOS/9 ปรับเปลี่ยนสถานะเมล (อ่าน-ไม่อ่าน).pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ปรับเปลี่ยนสถานะอีเมล์ (อ่าน / ไม่อ่าน)
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-9">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '10 การกำหนดอีเมลให้ตอบกลับอัตโนมัติ (Automatic replies-Out of Office Replies).pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile IOS/10 การกำหนดอีเมลให้ตอบกลับอัตโนมัติ (Automatic replies-Out of Office Replies).pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การกำหนดอีเมล์ให้ตอบกลับอัตโนมัติ (Automatic Replies/Out of Office Replies)
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-10">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '11 การสร้าง People (Contact).pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile IOS/11 การสร้าง People (Contact).pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การสร้าง People (Contact)
                      </div>
                    </li>
                  </ol>
                </li>
                <br/>

                {/* android */}
                <li className="dd-item bordered-inverse" data-id="2-2">
                  <div className="dd-handle">
                    <i className="pi pi-list" style={{ fontSize: '1.3em' }}></i>{' '}
                    <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '1 การเข้าใช้งานผ่าน Android.pdf'  ,'event' : 'read-file' })}>
                      <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile Android/1 การเข้าใช้งานผ่าน Android.pdf" target="_blank" rel="noopener noreferrer" >
                        <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                      </a>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การเข้าใช้งานผ่าน 
                    <span className="txt-bol"> Android </span>
                  </div>
                  <ol className="dd-list">
                    <li className="dd-item bordered-inverse" data-id="2-1-1">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '2 องค์ประกอบ และส่วนประกอบหน้าจอโปรแกรม.pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile Android/2 องค์ประกอบ และส่วนประกอบหน้าจอโปรแกรม.pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; องค์ประกอบ และส่วนประกอบหน้าจอโปรแกรม
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-2">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '3 การแสดงโฟลเดอร์ใน Outlook mobile.pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile Android/3 การแสดงโฟลเดอร์ใน Outlook mobile.pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การแสดงโฟลเดอร์ใน Outlook Mobile
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-3">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '4 การตั้งค่าต่างๆ ให้กับระบบ Outlook mobile.pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile Android/4 การตั้งค่าต่างๆ ให้กับระบบ Outlook mobile.pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การตั้งค่าต่างๆ ให้กับระบบ Outlook Mobile
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-4">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '5 สร้างโฟลเดอร์ และแสดงโฟลเดอร์ที่จำเป็น.pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile Android/5 สร้างโฟลเดอร์ และแสดงโฟลเดอร์ที่จำเป็น.pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; สร้างโฟลเดอร์ และแสดงโฟลเดอร์ที่จำเป็น
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-5">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '6 การค้นหาอีเมล จดหมาย หรือไฟล์ที่อยู่ใน Outlook.pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile Android/6 การค้นหาอีเมล จดหมาย หรือไฟล์ที่อยู่ใน Outlook.pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การค้นหาอีเมล์ จดหมาย หรือไฟล์ที่อยู่ใน Outlook
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-6">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '7 การใช้งานอีเมล (Email).pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile Android/7 การใช้งานอีเมล (Email).pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การใช้งานอีเมล์ (E-mail)
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-7">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '8 การแนบไฟล์ใน Outlook.pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile Android/8 การแนบไฟล์ใน Outlook.pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การแนบไฟล์ใน Outlook
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-8">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '9 ปรับเปลี่ยนสถานะเมล (อ่าน-ไม่อ่าน).pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile Android/9 ปรับเปลี่ยนสถานะเมล (อ่าน-ไม่อ่าน).pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ปรับเปลี่ยนสถานะอีเมล์ (อ่าน / ไม่อ่าน)
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-9">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '10 การกำหนดอีเมลให้ตอบกลับอัตโนมัติ (Automatic replies-Out of Office Replies).pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile Android/10 การกำหนดอีเมลให้ตอบกลับอัตโนมัติ (Automatic replies-Out of Office Replies).pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การกำหนดอีเมล์ให้ตอบกลับอัตโนมัติ (Automatic Replies/Out of Office Replies)
                      </div>
                    </li>

                    <li className="dd-item bordered-inverse" data-id="2-1-10">
                      <div className="dd-handle">
                        <span className="icon-load "  onClick={this.onClickFile.bind(this,{'file' : '11 การสร้าง People (Contact).pdf'  ,'event' : 'read-file' })}>
                          <a href="assets/file/MicrosoftOutlook/manual_on_mobile/Mobile Android/11 การสร้าง People (Contact).pdf" target="_blank" rel="noopener noreferrer" >
                            <i  className="pi pi-file red wave in " style={{ fontSize: '2em', position: 'absolute' }}></i>
                          </a>
                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การสร้าง People (Contact)
                      </div>
                    </li>
                  </ol>
                </li>
                

               </ol>
            
            </li>
          
          </ol>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { authentication} = state;
  const { user  }               = authentication;
 return {
      user
  };
}
export default connect(mapStateToProps)(MicrosoftOutlook);
