import React from 'react';
import ReactDOM from 'react-dom';
import MiscDemo from './MiscDemo';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MiscDemo />, div);
});
