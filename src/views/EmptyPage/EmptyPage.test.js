import React from 'react';
import ReactDOM from 'react-dom';
import EmptyPage from './EmptyPage';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<EmptyPage />, div);
});
