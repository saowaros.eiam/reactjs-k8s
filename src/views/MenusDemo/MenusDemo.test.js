import React from 'react';
import ReactDOM from 'react-dom';
import MenusDemo from './MenusDemo';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MenusDemo />, div);
});
