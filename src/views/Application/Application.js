import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { userActions } from '../../_actions';
// import { TieredMenu } from 'primereact/tieredmenu';
// import ReactCssTooltip from 'react-css-tooltip';
import 'react-css-tooltip/dist/index.css';

import './Application.css';

class Application extends Component {
  constructor() {
    super();
    this.state = {
      eHelpdesk: [
        {
          label: 'WFH Manual',
          icon: 'pi pi-file',
          command: e => {
            window.open('assets/file/TrueWFH_manual.pdf', '_blank');
          }
        },
        {
          separator: true
        },
        {
          label: 'Intranet Link',
          icon: 'pi pi-fw pi-window-minimize',
          command: e => {
            window.open('https://itsm.true.th/ehelpdesk/#/login', '_blank');
          }
        },
        {
          separator: true
        },
        {
          label: 'Internet Link',
          icon: 'pi pi-fw pi-external-link',
          command: e => {
            window.open(
              'https://itsm.truecorp.co.th/ehelpdesk/#/login',
              '_blank'
            );
          }
        }
      ]
    };
  }

  onClickFile(value){
        const data = new FormData();
            data['user_action']     =  this.props.user.login_name;
            data['page']         =  'home';
            data['event']        =  'create-wfh-ticket';
            data['score']        =  '1';
        this.props.dispatch(userActions.Tracking(data)); 
  }

  render() {
    return (
      <div className="p-col-12 application">
        <div className="p-grid ">
          
        <div className="p-col-12">
              <div onClick={this.onClickFile.bind(this)}>
              <Link to="/creatett"  >
              <div className="p-grid  box-shadow-app hover-app" >
                <div className="p-col-3">
                  <div className="ahvr-circle ahvr-animated animated-box effect-rotate ">
                    <div className="spinner"></div>
                    <img  className="img-fluid image" src="assets/images/ehelpdesk-wfh.png"alt="Avatar" />
                    <div className="overlay animated-bounce">
                      <div className="middle"> </div>
                    </div>
                  </div>
                </div>
                <div className="p-col-9" style={{ top: '0em' }}>
                  <div className="name-app" style={{ top: '0em' }}>
                    <div className="name-app-text themesecondary">
                      Create WFH Ticket<br/>
                      <span className="subtxt-wfh">(แจ้งปัญหาการทำงานที่บ้าน)</span>
                      
                    </div>
                  </div>
                </div>
              </div>
              </Link>
              </div>
          </div>


          <div className="p-col-12 ">
            <a target="_blank" rel="noopener noreferrer"  href="https://itsm.truecorp.co.th/ehelpdesk/#/login">
              <div className="p-grid box-shadow-app hover-app">
                <div className="p-col-3">
                  <div className="ahvr-circle ahvr-animated animated-box effect-rotate ">
                    <div className="spinner"></div>
                    <img
                      className="img-fluid image"
                      src="assets/images/ehelpdesk.png"
                      alt="Avatar"
                    />
                    <div className="overlay animated-bounce">
                      <div className="middle"></div>
                    </div>
                  </div>
                </div>
                <div className="p-col-9">
                  <div className="name-app">
                    <div className="name-app-text">
                      eHelpdesk &nbsp;
                      {/* <span className="spaces-hpd-icon" >
                        <Link to="/creatett"  >
                           <ReactCssTooltip placement="bottom" className="txt-tooltip" text={`แจ้งปัญหา WFH ด้วยตัวเอง`} >
                          
                           <img
                            className="img-fluid image"
                            style={{ width: '1em' }}
                            src="assets/images/ticket-wfh.png"
                            alt=""
                          />
                           </ReactCssTooltip>
                        </Link>
                        &nbsp;&nbsp;
                        <a
                          href="assets/file/TrueWFH_manual.pdf"
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                           <ReactCssTooltip placement="bottom" className="txt-tooltip" text={`คู่มือ แจ้งปัญหา IT ด้วยตัวเอง`} >
                          <img
                            className="img-fluid image"
                            style={{ width: '1em' }}
                            src="assets/images/manual_ehelpdesk.png"
                            alt=""
                          />
                          </ReactCssTooltip>
                        </a>
                      </span> */}
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>

          <div className="p-col-12 ">
            {/* <a target="_blank" rel="noopener noreferrer"  href="https://itsm.truecorp.co.th/smart-chatbot/#/login"> */}
              <div className="p-grid box-shadow-app hover-app"
               onClick={()=> window.open(`${process.env.REACT_APP_URL_SMARTCHATBOT}access_token=${JSON.parse(localStorage.getItem('wah'))}&target_page=bWFqb3JpbmNpZGVudA==`, "_blank")}>
                <div className="p-col-3">
                  <div className="ahvr-circle ahvr-animated animated-box effect-rotate ">
                    <div className="spinner"></div>
                    <img
                      className="img-fluid image"
                      src="assets/images/smart-chatbot-2.png"
                      alt="Avatar"
                    />
                    <div className="overlay animated-bounce">
                      <div className="middle"></div>
                    </div>
                  </div>
                </div>
                <div className="p-col-9">
                  <div className="name-app">
                    <div className="name-app-text">
                      Smart Chatbot &nbsp;
                    </div>
                  </div>
                </div>
              </div>
            {/* </a> */}
          </div>
          <div className="p-col-12">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://imail.truecorp.co.th/iphone.nsf"
            >
              <div className="p-grid  box-shadow-app hover-app">
                <div className="p-col-3">
                  <div className="ahvr-circle ahvr-animated animated-box effect-rotate ">
                    <div className="spinner"></div>
                    <img
                      className="img-fluid image"
                      src="assets/images/mail.png"
                      alt="Avatar"
                    />
                    <div className="overlay animated-bounce">
                      <div className="middle">
                        {/* <i className="box-icon pi pi-cog"></i> */}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="p-col-8">
                  <div className="name-app">
                    <div className="name-app-text">Web Mail</div>
                  </div>
                </div>
              </div>
            </a>
          </div>

          <div className="p-col-12">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://hr.truecorp.co.th/home/th"
            >
              <div className="p-grid  box-shadow-app hover-app">
                <div className="p-col-3">
                  <div className="ahvr-circle ahvr-animated animated-box effect-rotate ">
                    <div className="spinner"></div>
                    <img
                      className="img-fluid image"
                      src="assets/images/true-hr.png"
                      alt="Avatar"
                    />
                    <div className="overlay animated-bounce">
                      <div className="middle">
                        {/* <i className="box-icon pi pi-cog"></i> */}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="p-col-8">
                  <div className="name-app">
                    <div className="name-app-text">True HR</div>
                  </div>
                </div>
              </div>
            </a>
          </div>

          <div className="p-col-12">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://trueconnect.ekoapp.com/login"
            >
              <div className="p-grid  box-shadow-app hover-app">
                <div className="p-col-3">
                  <div className="ahvr-circle ahvr-animated animated-box effect-rotate ">
                    <div className="spinner"></div>
                    <img
                      className="img-fluid image"
                      src="assets/images/true-connect.png"
                      alt="Avatar"
                    />
                    <div className="overlay animated-bounce">
                      <div className="middle">
                        {/* <i className="box-icon pi pi-cog"></i> */}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="p-col-8">
                  <div className="name-app">
                    <div className="name-app-text">True Connect</div>
                  </div>
                </div>
              </div>
            </a>
          </div>

          <div className="p-col-12">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://spaces.truecorp.co.th"
            >
              <div className="p-grid box-shadow-app hover-app">
                <div className="p-col-3">
                  <div className="ahvr-circle ahvr-animated animated-box effect-rotate ">
                    <div className="spinner"></div>
                    <img
                      className="img-fluid image"
                      src="assets/images/share-drive.png"
                      alt="Avatar"
                    />
                    <div className="overlay animated-bounce">
                      <div className="middle"></div>
                    </div>
                  </div>
                </div>

                <div className="p-col-9" style={{ top: '0em' }}>
                  <div className="name-app" style={{ top: '0em' }}>
                    <div className="name-app-text">
                      <span className="" >Spaces
                        <br/>
                        <span className="subtxt" > (Shared Drive) </span>
                      </span>
                      <span className="spaces-icon"  >
                        &nbsp;&nbsp;
                        <a
                          href="assets/file/True Cloud iOS.pdf"
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          <img
                            className="img-fluid image"
                            style={{ width: '1em' }}
                            src="assets/images/manual_ios.png"
                            alt=""
                          />
                        </a>
                        &nbsp;&nbsp;
                        <a
                          href="assets/file/True Cloud Android.pdf"
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          <img
                            className="img-fluid image"
                            style={{ width: '1em' }}
                            src="assets/images/manual_android.png"
                            alt=""
                          />
                        </a>
                        &nbsp;&nbsp;
                        
                        <a
                          href="assets/file/True Cloud PC.pdf"
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          <img
                            className="img-fluid image"
                            style={{ width: '1em' }}
                            src="assets/images/manual_pc.png"
                            alt=""
                          />
                        </a>
                      </span>
                    </div>

                    
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { authentication} = state;
  const { user  }               = authentication;
 return {
      user
  };
}
export default connect(mapStateToProps)(Application);
