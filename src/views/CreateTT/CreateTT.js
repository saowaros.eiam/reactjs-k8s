import React, {Component} from 'react';
import { connect } from 'react-redux';
import { environment } from '../../_environments/environment';
import { notiActions ,userActions ,ticketActions} from '../../_actions';
import nl2br from 'react-newline-to-break';


import { InputTextarea } from 'primereact/inputtextarea';
import { FileUpload } from 'primereact/fileupload';
import { Button } from 'primereact/button';
import { ProgressSpinner } from 'primereact/progressspinner';
import { Dropdown } from 'primereact/dropdown';
import { BreadCrumb } from 'primereact/breadcrumb';

import {RadioButton} from 'primereact/radiobutton';
import {Dialog} from 'primereact/dialog';

import { userSummarys } from '../../_services/summarys.service';
import { ItemWfh } from '../../service/ItemWfh';

import './CreateTT.css';

class CreateTT extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataItem: [],	
            dataSummary: [],
            item                : '',
            summary             : '',
            detail              : '',
            d_files             : [],
            d_files_delete      : [],
            files               : [],
            visible             : false,
            loading             : false,
            email_address       : '',
            addClass            : '',
            categoryname       : '',  
            categoryID         : '',
            typename           : '',
            typeID             : '',
            itemName           : '',
            itemID             : '',
            summaryName        : '',
            summaryID          : '',
            summaryDetail      : '',
            msg_err_mail       : false,
            msg_err_item       : false,
            msg_err_summary    : false,
            breadcrumdItems: [
                {label:'Home', url: './#/home'}
            ],home: {
                icon: 'pi pi-home', url: './#/home'
            },
            tasks               : [],
            checked             : false ,
            displayWorkaround   : false,
            position            : 'center',
            workaround          : '',
            type                : '',
            displayBasic2: false
        };

        this.onBasicUploadAuto   = this.onBasicUploadAuto.bind(this);
        this.remove              = this.remove.bind(this);
        this.isImage             = this.isImage.bind(this);

        this.onItemChange        = this.onItemChange.bind(this);
        this.onSummaryChange     = this.onSummaryChange.bind(this);

        this.handleSubmit        = this.handleSubmit.bind(this);
        this.checkEmail          = this.checkEmail.bind(this);

        this.d_userSummarys      = new userSummarys();
        this.itemWfh             = new ItemWfh();

        this.onClick = this.onClick.bind(this);
        this.onHide = this.onHide.bind(this);

    }

    componentDidMount() {
        if(this.props.user.email_address &&
            this.props.user.email_address !== undefined){
            this.setState({email_address : this.props.user.email_address});
        }
        this.itemWfh.getData().then( data =>  this.setState({dataItem  : data }));
        
    }

    componentDidUpdate(prevProps) {
        if (this.props.user.email_address !== prevProps.user.email_address
            && this.state.email_address === '') {
                this.setState({email_address : this.props.user.email_address});
        }
    }

    onItemChange(e){

        this.setState({item: e.value});
        let data = {...e.value};
        this.setState({categoryname: data.catname});
        this.setState({categoryID: data.catid});
        this.setState({typename: data.typename});
        this.setState({typeID: data.typeid});
        this.setState({itemName: data.value});
        this.setState({itemID: data.itemid});

        this.getSummaryData(data.value);
        this.setState({msg_err_item : false});

    }
    
    getSummaryData = (data) =>{
        this.props.dispatch(notiActions.clear());
        this.props.dispatch(userActions.loading());
        this.d_userSummarys.getSummarysByItem(data).then(res => {
            this.props.dispatch(userActions.loading_success());
            if(res.status === true){
                if(res.data === 'Not Found'){
                    this.props.dispatch(notiActions.success({severity: 'warn', summary: 'Summary', detail: res.data ,sticky: true}));
                }else{
                    this.setState({dataSummary: res.data});
                    // this.props.dispatch(userActions.dis_Summarys(res.data));
                }

            }else if(res.status === false){
                this.props.dispatch(notiActions.error({severity: 'error', summary: 'Summary', detail: res.data ,sticky: true}));
            
            }else{
                this.props.dispatch(notiActions.error({severity: 'error', summary: 'Summary', detail: res.data.message,sticky: true}));
            }
        },error => {
            this.props.dispatch(userActions.loading_success());
            this.props.dispatch(notiActions.error({severity: 'error', summary: 'Summary', detail: error.toString()  ,sticky: true }));
          
        });
    }

    onSummaryChange(e){
        this.setState({summary: e.value});
        
        let data = {...e.value};
        this.setState({summaryName: data.symptomname});
        this.setState({summaryID: data.symptomid});
        this.setState({summaryDetail: e.value});

        this.SummaryDetail(e.value);
        this.setState({msg_err_summary : false});
       
    }

    handleDetail(e){
        this.setState({description: e.target.value});
    }

    checkEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    handleEmail(e){
        this.setState({email_address: e.target.value});
        let c_mail = this.checkEmail(e.target.value);
        if(c_mail === false)  {
            this.setState({msg_err_mail : true}); 
        }else{
            this.setState({msg_err_mail : false}); 
        }
    }  

    formatSize(bytes) {
        if(bytes === 0) {
            return '0 B';
        }
        let k = 1000,
        dm = 3,
        sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
        i = Math.floor(Math.log(bytes) / Math.log(k));
        
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    isImage(file) {
        return /^image\//.test(file.type);
    }

    remove(index) {
        let currentFiles = [...this.state.d_files];
            currentFiles.splice(index, 1);
            this.setState({d_files: currentFiles});
            this.props.dispatch(ticketActions.upfile(currentFiles));
        
        let imagesView = [...this.state.files];
            imagesView.splice(index, 1);
            this.setState({files: imagesView});

        let file_delete = [];
            file_delete[index] = this.state.d_files[index];
            this.state.d_files_delete.push( file_delete[index]);
    }

    onBasicUploadAuto(event) {  
        this.props.dispatch(notiActions.clear()); 
        const result  = event.xhr.response;
        const result_arr = JSON.parse(result);
        if(result_arr.results.code === 200){
            if(result_arr.results.data){
                const datafiles  = event.files;
                if(this.state.files.length < "3" ){
                    let keyfile = this.state.files.length;
                    if(keyfile !== 0){
                
                        let arr      = [];
                        arr[keyfile] = datafiles[0];
                        this.state.files.push(arr[keyfile]);

                        let arr_dump      = [];

                        arr_dump[keyfile] = result_arr.results.data;
                        this.state.d_files.push(arr_dump[keyfile]);
                        this.props.dispatch(ticketActions.upfile(this.state.d_files));
                        this.props.dispatch(notiActions.success({severity: 'info', summary: 'Success', detail: 'File Uploaded with Auto Mode'})); 
                        this.setState({ s_image : true});
                    }else{
                        
                        let arr  = [];
                        arr[0] = result_arr.results.data;
                        this.setState({ d_files : arr });
                        this.setState({ files : datafiles });
                        this.props.dispatch(ticketActions.upfile(this.state.d_files));
                        this.props.dispatch(notiActions.success({severity: 'info', summary: 'Success', detail: 'File Uploaded with Auto Mode'})); 
                        this.setState({ s_image : true});
                    } 
                }else {
                    alert('Maximum of 3 files');
                }
                
            } 
        }else{
            this.props.dispatch(notiActions.success({severity: 'warn', summary: 'Warning', detail: result_arr.results.description ,sticky : true})); 
        }
    }


    SummaryDetail = (detail) => { 
        // description
        this.setState({ summaryDetail : detail });
        if(detail.callscript){
            let value     = detail.callscript.toString();
                value     = value.replace(/\\n/g,'\n');         
            if(value !== 'NULL'){
                const answer_array = value.split('\n');

                if(value.length > 0 && detail.require_field === '0'){
                    this.setState({requirefield : answer_array});

                    if(this.state.requirefield){
                        this.state.requirefield.map((item,i) =>  {
                            let result = '';
                            let patt = /Error/g;
                                result = item.match(patt);
                            if(result){
                                this.setState({inxError : i });
                                result = '';
                            }
                        });
                    }
                }else{
                    this.setState({requirefield : ''});
                    this.setState({inxError : null});
                }
            }else{
                this.setState({requirefield : ''});
                this.setState({inxError : null });
            }

            if(this.props.match.params.item){
                this.setState({visible: true}); 
                this.props.dispatch(userActions.loading_success()); 
            }
        }else{
            this.setState({requirefield : ''});
            this.setState({inxError : null});
            this.setState({callscript  : ''}); 
            if(this.props.match.params.item){
                this.setState({visible: true});
                this.props.dispatch(userActions.loading_success());
            }
        }

        if(detail.description){
            let value     = detail.description.toString();
                value     = value.replace(/\\n/g,'\n');         
            if(value !== 'NULL'){
                this.setState({description : value});   
            }else{
                this.setState({description : ''}); 
            }
        }else{
            this.setState({description : ''}); 
        }

        if(detail.manual_file || detail.workaround_text){
            let link = '';
            
            if(detail.manual_file && detail.workaround_text){
                link = <p>
                            <span style={{ color: '#333333'}} >{detail.manual_file_shortname} &nbsp;&nbsp;
                                {/* <a target='_blank'  rel="noopener noreferrer" 
                                    href={`assets/file/${detail.manual_file}`}> */}
                                    <i  className="pi pi-file red wave in"  style={{ fontSize: '2em' }} 
                                     onClick={this.onClickFile.bind(this,detail)}></i>
                                {/* </a> */}
                            </span> 
                          
                            <br/> <br/> <br/>
                            {/* <span  >{nl2br(detail.workaround_text)}</span>  */}
                            <span dangerouslySetInnerHTML={{__html: detail.workaround_text}} ></span>
                            <br/>
                            </p>;
            }else if(detail.manual_file){
                link = <p><span style={{ color: '#333333'}} >{detail.manual_file_shortname} &nbsp;&nbsp;
                                {/* <a target='_blank'  rel="noopener noreferrer" 
                                    href={`assets/file/${detail.manual_file}`}> */}
                                    <i  className="pi pi-file red wave in"  style={{ fontSize: '2em' }}
                                    onClick={this.onClickFile.bind(this,detail)} ></i>
                                {/* </a> */}
                            </span> 
                            <br/> <br/> <br/>
                        </p>;
                
            }else if(detail.workaround_text){
                link = <p>
                        {/* <span  > {nl2br(detail.workaround_text)}</span>  */}
                        <span dangerouslySetInnerHTML={{__html: detail.workaround_text}} ></span>
                        <br/>
                    </p>;
                
            }else if(detail.manual_file_shortname){
                link = <p>
                <span dangerouslySetInnerHTML={{__html: detail.manual_file_shortname}} ></span>
                <br/>
            </p>;
            }

            this.setState({workaround :  link });

            this.setState({displayWorkaround: true}); 
       
        }else{
            this.setState({workaround :  '' });
            this.setState({displayWorkaround: false}); 
            
        }

 
    }
    
    handleSubmit(e) {

        e.preventDefault();
        this.props.dispatch(notiActions.clear());

        let c_mail     = this.checkEmail(this.state.email_address.trim());

        if(this.state.summaryName === undefined || this.state.summaryName === ''
            || this.state.itemName === undefined || this.state.itemName === ''){

            if(this.state.itemName === undefined || this.state.itemName === ''){
                this.setState({msg_err_item: true});
            }
            if(this.state.summaryName === undefined || this.state.summaryName === ''){
                this.setState({msg_err_summary: true});
            }

            if(c_mail === false){
                this.setState({msg_err_mail : true});
            }
                
        }else if(c_mail === false){
            this.setState({msg_err_mail : true});
            
        }else{

            const data = new FormData();
                data['userdata']        =  this.props.user;
                data['category']        =  this.state.categoryname;
                data['category_id']     =  this.state.categoryID;
                data['typename']        =  this.state.typename;
                data['type_id']         =  this.state.typeID;
                data['item']            =  this.state.itemName;
                data['item_id']         =  this.state.itemID;
                data['summary']         =  this.state.summaryName;
                data['summary_id']      =  this.state.summaryID;
                data['summarydetail']   =  this.state.summaryDetail;
                data['source']          =  'Work From Home';
    
            if(this.state.description){
                this.state.description = this.state.description.replace(/'/g, "''");
                this.state.description = this.state.description.replace(/""/g, '"');
            }
        
                data['detail']      =  this.state.description;
            
                data['files']           =  this.state.d_files;
                data['files_delete']    =  this.state.d_files_delete;
                data['email_address']   =  this.state.email_address;

            this.props.dispatch(ticketActions.newTicket(data)); 
            this.setState({  d_files             : []
                             ,d_files_delete      : []
                             ,msg_err_mail : false
            }); 
            // let detail_sum = {...this.state.summaryDetail};
            // const value = new FormData();
            //     value['user_action']     =  this.props.user.login_name;
            //     value['page']         =  'create-tt';
            //     value['event']        =  'submit';
            //     value['score']        =  '1';
            //     value['filename']     =  detail_sum.manual_file;
            //     value['item']         =  detail_sum.itemname;
            //     value['summary']      =  detail_sum.symptomname;
            //this.props.dispatch(userActions.Tracking(value)); 


        }
           
    }

    onClick(name, position) {
        let state = {
            [`${name}`]: true
        };

        if (position) {
            state = {
                ...state,
                position
            }
        }

        this.setState(state);
    }
    
    onHide(name) {
        this.setState({
            [`${name}`]: false
        });
    }

    onClickFile(value){
        if(value.manual_file){
            const data = new FormData();
                data['user_action']     =  this.props.user.login_name;
                data['page']         =  'create-tt';
                data['event']        =  'read-file';
                data['score']        =  '1';
                data['filename']     =  value.manual_file;
                data['item']         =  value.itemname;
                data['summary']      =  value.symptomname;
            let link = 'assets/file/'+value.manual_file;
            window.open(link,  '_blank');
            this.props.dispatch(userActions.Tracking(data)); 
        }
    }

    render() {
        const timestamp = Date.now();
        const mykey = 'uploadedFiles['+ `${timestamp}` +']';
        //const mykey = 'uploadedFiles[312323123]';

        const { loading  } = this.props;
        const { msg_err_summary ,msg_err_item } = this.state;

        let spinner = null; 
        if(loading){
            spinner = ( <div className="loading_center">
                            <ProgressSpinner style={{width: '5.5em', height: '5.5em'}} strokeWidth="8" fill="#EEEEEE" animationDuration=".5s"/>
                        </div>);
            this.state.addClass = 'loading_div';
        }else{
            this.state.addClass = '';
        }
        
        return (
            <div className={`img-bg p-grid card creatett ${this.state.addClass} `} >
                 {spinner}
                 <Dialog header="Self Service ขั้นตอนการแก้ปัญหาด้วยตัวเอง" visible={this.state.displayWorkaround} 
                    onHide={() => this.onHide('displayWorkaround')} 
                    maximizable  className="wid-dialog"
                        >
                        {this.state.workaround}
                </Dialog>
                
                <div className="p-col-12  p-md-10">
                    <div className="title">
                        <img src="assets/images/red-true.png" alt="img-icon"  className="logo-img"/>
                        <img src="assets/layout/images/WFH_4.png" alt="logo"  className="logo-img"/>
                    </div>
                </div>
                <div className="p-col-12  p-md-2">
                    <BreadCrumb style={{backgroundColor: 'unset',border:'unset' ,float:'right'}} model={this.state.breadcrumdItems} home={this.state.home} />
                </div>
           
                <div className="p-col-12" >
                    <div className="pad-div">
                        <h1>Create Ticket</h1>
                        <form name="form"  encType="multipart/form-data"> 
                            <div className="p-grid" style={{paddingTop: '2em'}}>    
                                <div className="p-col-12 p-md-2 ">
                                    <label htmlFor="problem">  
                                        <span style={{fontWeight:'700'}}> Type </span>
                                        <span style={{color: '#757575', fontSize: '13px'}}> ประเภท </span>
                                    </label>
                                </div>
                                <div className="p-col-12 p-md-10 ">
                                <div className="p-grid">
                                        <div className="p-col-12">
                                            <RadioButton value="WFH - Work From Home" inputId="rb1" checked={true} 
                                                onChange={event => this.setState({type: event.value})} 
                                              
                                            />
                                            <label htmlFor="rb1" className="p-radiobutton-label">WFH - Work From Home</label>
                                        </div>
                                        <div className="p-col-12">
                                            <RadioButton disabled={true} value="Avalon" inputId="rb2"
                                             onChange={event => this.setState({type: event.value})} 
                                             checked={this.state.radioValue === "Avalon"} />
                                            <label htmlFor="rb2" className="p-radiobutton-label">
                                            <span style={{color: '#757575'}}>Virtual Connect (coming soon)</span></label>
                                        </div>
                                       
                                    </div>

                                </div>
                                <div className="p-col-12 p-md-2 ">
                                    <label htmlFor="problem">
                                        <span style={{fontWeight:'700'}}> Topic </span>
                                        <span style={{color: '#757575', fontSize: '13px'}}> หัวข้อ </span>
                                    </label>
                                </div>
                                <div className="p-col-12 p-md-10">
                                <Dropdown value={this.state.item} options={this.state.dataItem} 
                                        onChange={this.onItemChange} placeholder="--- Select ---" 
                                        optionLabel="name"  className="wid-item"  />
                                            { msg_err_item  &&
                                                    <div className="help-block themesecondary">* กรูณาเลือก System of a problem </div>
                                            }
                                </div>

                                <div className="p-col-12 p-md-2 ">
                                    <label htmlFor="input"> 
                                        <span style={{fontWeight:'700'}}> Symptom </span>
                                        <span style={{color: '#757575', fontSize: '13px'}}> อาการ/ปัญหา </span>
                                    </label>
                                </div>
                                <div className="p-col-10" >
                                    <Dropdown value={this.state.summary} options={this.state.dataSummary} 
                                    filter={true}  scrollHeight='500px'
                                    className="wid-sum" 
                                        onChange={this.onSummaryChange} placeholder="--- Select ---" 
                                        optionLabel="name"  
                                         />
                                    { msg_err_summary  &&
                                            <div className="help-block themesecondary">* กรูณาเลือก Symptom/Error</div>
                                    }
                                </div>
                                <div className="p-col-12 "><br/></div>
                                <div className="p-col-12 ">
                                    <label htmlFor="input">
                                    <span style={{fontWeight:'700'}}>  Detail : </span>
                                    <span style={{color: '#757575', fontSize: '13px'}}>   ข้อมูลที่จำเป็น เพื่อใช้ประกอบการแก้ไขปัญหา</span>
                                    </label>
                                </div>
                                <div className="p-col-12">
                                    <InputTextarea  autoResize={true} name="detail" className="wid-txtarea"
                                        value={this.state.description}  onChange={this.handleDetail.bind(this)} 
                                        rows={4} cols={70} required />    
                                </div>   
                                <div className="p-col-12">
                                    <label htmlFor="file-multiple-input">
                                    <span style={{fontWeight:'700'}}>Multiple File Input : </span>
                                    <span style={{color: '#757575', fontSize: '13px'}}> Capture Error หรือ ข้อมูลต่างๆ<br/>
                                        ขนาดไฟล์ละ 2.5 mb (ประเภทไฟล์ที่อนุญาตให้อัพโหลด jpg ,gif ,png ,docx ,xlsx ,pdf ,zip ) </span>
                                    </label>
                                </div>  
                                <div className="p-col-12">  
                                    <FileUpload mode="basic" name={mykey} url={`${environment.backendUrlupload}${environment.upload}`} 
                                    maxFileSize={2500000} onUpload={this.onBasicUploadAuto} auto={true}/>
                                    <div className="ui-fileupload-files">
                                        { this.state.files.map((file, index) => {
                                                let preview = this.isImage(file) ? <div><img alt={file.name} role="presentation"  src={file.objectURL} width="50" /></div> : null;
                                                let fileName = <div>{file.name}</div>;
                                                let size = <div>{this.formatSize(file.size)}</div>;
                                                let removeButton = <div><i className="pi pi-times"  onClick={() => this.remove(index)}></i> </div>

                                                return   <div className="ui-fileupload-row" key={file.name + file.type + file.size}>
                                                        {preview}
                                                        {fileName}
                                                        {size}
                                                        {removeButton}
                                                </div>
                                            })
                                        }
                                    </div>    
                                </div> 
                                {/* <div className="p-col-12 "><br/></div>               
                                <div className="p-col-12 p-md-1">
                                    <label htmlFor="textarea themesecondary">E-Mail</label>
                                </div>
                                <div className="p-col-12 p-md-10">
                                    <InputTextarea id="textarea" onChange={this.handleEmail.bind(this)} 
                                    rows={1} cols={50} autoResize={true} value={this.state.email_address} />
                                    { msg_err_mail  &&
                                            <div className="help-block themesecondary">* E-mail ไม่ถูกต้อง</div>
                                    }
                                </div>          */}
                                <div className="p-col-12 p-md-3 p-md-offset-9">
                                    <Button type="Submit" className="p-button-raised p-button-danger" 
                                        icon="pi pi-check "  label="Submit"  onClick={this.handleSubmit}/>
                                </div>
                            </div>
                                              
                        </form>
                    </div>
           
                </div>

            </div>

            
                

                               
        );
    }
}

function mapStateToProps(state) {
    const { authentication, notification ,cti } = state;
    const { user ,file }               = authentication;
    const { items ,summary}                  = cti;


   return {
        user,
        items,
        summary,
        file,
     notification,
    };
}
export default connect(mapStateToProps)(CreateTT); 