import React, { Component } from 'react';
import { connect } from 'react-redux';
import { notiActions ,userActions } from '../../_actions';
import { userService } from '../../_services';


import Application from '../Application';
import RAS from '../RAS';
import MicrosoftOutlook from '../MicrosoftOutlook';
import SupportSoftware from '../SupportSoftware';
import SoftwareInstaller from '../SoftwareInstaller';
import SoftwareInstallerShop from '../SoftwareInstallerShop';
import MeetingSoftware from '../MeetingSoftware';

import {Button} from 'primereact/button';
import { TieredMenu } from 'primereact/tieredmenu';

import ReactCssTooltip from 'react-css-tooltip';
import 'react-css-tooltip/dist/index.css';
import './Home.css';
import '../../wave.css';


class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
        visible          : false,
        setting_user     : true,
        submited         : false,
        checkboxValue    : [],
        d_user: {
          emp_id         : '',
          first_name     : '',
          last_name      : '',
          full_name      : '',
          email          : '',
          phone_number   : '',  
          id             : '',  
          image          : ''
        },
    
        d_validation: {
          id              : '',      
          full_name       : '',    
          emp             : '', 
          login_name      : '',   
          normal          : '',
          crisis          : '',
          sms             : '',
          mobile_1        : '',
          mobile_2        : '',
          mail            : '',
          email           : ''
        },

        type_user          : '' ,
        phone_1            : '' ,
        phone_2            : '' ,
        visiblePhone       : false,
        visibleEmail       : false,
        update_rv          : true,
        sms_otp: [
          {
            label: 'Internet',
            icon: 'pi pi-external-link red',
            command: e => {
              window.open('https://terra.truecorp.co.th/dana-na/auth/url_103/welcome.cgi', '_blank');
            }
          },
          {
            separator: true
          },
          {
            label: 'Intranet',
            icon: 'pi pi-window-minimize red',
            command: e => {
              window.open('https://pwdapprw1/iisadmpwdNew/CheckStatusN/LAN_PersonalPwd.asp?Domain=TRUE', '_blank');
            }
          }
        ]
        
    };
    this.onCheckboxChange           = this.onCheckboxChange.bind(this);
    this.handleSubmit               = this.handleSubmit.bind(this);
    this.checkDisplayRasValidation  = this.checkDisplayRasValidation.bind(this);
  }

  componentDidMount() {
      if(this.props.user !== undefined && this.props.user.request_id){ 
        if( this.state.setting_user === true){
          this.setState({ display : false });
          let image = 'assets/layout/images/profile.png';
          if(this.props.user.image){
            image = this.props.user.image;
          }
          let type_user = '';
          if(this.props.user.type === 'External'){
            type_user = this.props.user.type;
            image = 'assets/layout/images/profile.png';
          }
          this.setState({  d_user: {
                  emp_id         : this.props.user.emp_id,
                  first_name     : this.props.user.first_name,
                  last_name      : this.props.user.last_name,
                  email          : this.props.user.email_address,
                  phone_number   : this.props.user.phone_number,  
                  id             : this.props.user.id  ,
                  full_name      : this.props.user.full_name , 
                  image          : image  
              },
              type_user :  type_user
          });
        }

        if(false){
          if(this.state.update_rv === true ){
            this.setState({ update_rv : false  });
            userService.getRasValidation(this.props.user).then(res => {
              this.props.dispatch(userActions.loading_success());
            
              if(res.status === true){
                  if(res.data === 'Not Found'){
                    // this.props.dispatch(notiActions.success({severity: 'warn', summary: 'RAS Validation', detail: res.data ,sticky: true}));
                  }else{
                    this.props.dispatch(userActions.dispatchpRasValidation(res.data));
                    // this.props.dispatch(notiActions.success({severity: 'success', summary: 'Success', detail: 'RAS Validation.' }));
        
                    this.checkDisplayRasValidation(res.data);
                  }
              }else if(res.status === false){
                  this.props.dispatch(notiActions.error({severity: 'error', summary: 'RAS Check-Up', detail: res.data ,sticky: true}));
              }
            },error => {
                this.props.dispatch(notiActions.error({severity: 'error', summary: 'RAS Check-Up', detail: error.toString()}));
            });
          }
        }
      }
  }

  componentDidUpdate(nextProps,prevProps) {

    if (nextProps.user !== undefined  && nextProps.user !== this.props.user
      && this.props.user.request_id  && this.state.setting_user === true) {
        this.setState({ display : false });
          let image = 'assets/layout/images/profile.png';
          if(this.props.user.image){
            image = this.props.user.image;
          }
          let type_user = '';
          if(this.props.user.type === 'External'){
            type_user = this.props.user.type;
            image = 'assets/layout/images/profile.png';
          }
          this.setState({  d_user: {
                  emp_id         : this.props.user.emp_id,
                  first_name     : this.props.user.first_name,
                  last_name      : this.props.user.last_name,
                  email          : this.props.user.email_address,
                  phone_number   : this.props.user.phone_number,  
                  id             : this.props.user.id  ,
                  full_name      : this.props.user.full_name , 
                  image          : image  
              },
              type_user :  type_user
          });
    }

    if(false){
      if(this.props.ras_validation === undefined && this.props.user.request_id
        && this.state.update_rv === true){
        this.setState({ update_rv : false });
        userService.getRasValidation(this.props.user).then(res => {
        
          this.props.dispatch(userActions.loading_success());
          if(res.status === true){
              if(res.data === 'Not Found'){
                // this.props.dispatch(notiActions.success({severity: 'warn', summary: 'Ras Validation', detail: res.data ,sticky: true}));
              }else{
                this.props.dispatch(userActions.dispatchpRasValidation(res.data));
                // this.props.dispatch(notiActions.success({severity: 'success', summary: 'Success', detail: 'Ras Validation.' }));
                this.checkDisplayRasValidation(res.data);

              }
          }else if(res.status === false){
              this.props.dispatch(notiActions.error({severity: 'error', summary: 'RAS Check-Up', detail: res.data ,sticky: true}));
          }
        },error => {
            this.props.dispatch(notiActions.error({severity: 'error', summary: 'RAS Check-Up', detail: error.toString()}));
        });
      }
    }

    

    if (nextProps.ras_validation !== undefined  && nextProps.ras_validation !== this.state.d_validation
      && this.state.submited === true) {
        this.setState({ submited : false });
        this.checkDisplayRasValidation(nextProps.ras_validation);

      }
  }

  checkDisplayRasValidation(data){

    this.setState({d_validation :  {...data} 
    },() =>{ 
      let validation = {...this.state.d_validation};

     
      if(validation.email && (validation.email !== '' || validation.email  !== undefined)){
        
        this.setState({ visibleEmail : false  });
      }else{
       
        this.setState({ visibleEmail : true  });
      }

      if(validation.sms === 'Y'){
        if(validation.mobile_1 && validation.mobile_1 !== null){
          this.setState({ phone_1 : validation.mobile_1  });
        }

        if(validation.mobile_2 && validation.mobile_2 !== null){
          this.setState({ phone_2 : validation.mobile_2  });
        }
        this.setState({ visiblePhone : true  });

      }else if(validation.sms === 'N'){
        this.setState({ visiblePhone : false  });

      }
      
      if(( validation.mobile_1 === '' || validation.mobile_1 === null) 
      && ( validation.mobile_2 === '' || validation.mobile_2 === null)){
        this.setState({ visiblePhone : false  });
      }


    });


  }
  handleRasValidation(){
    this.setState({visible: !this.state.visible});
  }

  handlePhone(){
    this.setState({visiblePhone: !this.state.visiblePhone});
  }

  handleEmail(){
    this.setState({visibleEmail: !this.state.visibleEmail});
  }

  onCheckboxChange(event){
    let selected = [...this.state.checkboxValue];
    if (event.checked)
        selected.push(event.value);
    else
        selected.splice(selected.indexOf(event.value), 1);

    this.setState({checkboxValue: selected});
  }

  handleSubmit(e) {
      e.preventDefault();
      this.props.dispatch(notiActions.clear());
      // this.props.dispatch(userActions.loading());

      if(false){
        if(this.props.user.login_name !== undefined && this.props.user.login_name !== ''){

            let d_validation = {...this.state.d_validation};
            const data = new FormData();
                data['userdata']     =  this.props.user;
                data['id']           =  d_validation.id;
                data['full_name']    =  d_validation.full_name;
                data['emp']          =  d_validation.emp;
                data['login_name']   =  d_validation.login_name;
                data['crisis']       =  d_validation.crisis;
                data['sms']          =  d_validation.sms;
                data['web']          =  d_validation.web;
                data['email']        =  d_validation.email;
                data['mobile_1']          =  this.state.phone_1;
                data['mobile_2']          =  this.state.phone_2;
        
          userService.updateRasValidation(data).then(res => {
              // this.props.dispatch(userActions.loading_success());
              this.setState({checkboxValue : [] ,phone_1 : '' ,phone_2 : ''});

              if(res.status === true){
                  if(res.data === 'Not Found'){
                      // this.props.dispatch(notiActions.success({severity: 'warn', summary: 'RAS Validation', detail: res.data ,sticky: true}));
                  }else{
                    this.props.dispatch(userActions.dispatchpRasValidation(res.data));
                    this.props.dispatch(notiActions.success({severity: 'success', summary: 'Success', detail: 'RAS Check-Up' }));
                    this.setState({submited  :  true });
                    // setTimeout(() => {
                      this.setState({visible: false});
                    // },1000);
                  }
              }else if(res.status === false){
                  // this.props.dispatch(notiActions.error({severity: 'error', summary: 'RAS Validation', detail: res.data ,sticky: true}));
              }
          },error => {
            // setTimeout(() => {
              this.setState({visible: false});
            // },1000);
              // this.props.dispatch(notiActions.error({severity: 'error', summary: 'RAS Validation', detail: error.toString()}));
          });
        }else{
          // setTimeout(() => {
            this.setState({visible: false});
          // },1000);
          // this.props.dispatch(notiActions.warn({severity: 'warn', summary: 'Account', detail: 'กรุณาตรวจสอบข้อมูล Account หรือทำการ Login ใหม่ค่ะ' ,sticky: true}));          
        }
      }
       
  }

  render() {
    
    const {d_user ,d_validation } = this.state;

    let btn_class     = this.state.visible ? "block-overlay-ras  " : "none-overlay-ras ";
    let overlay_class = this.state.visible ? "p-tieredmenu p-component p-tieredmenu-dynamic p-menu-overlay p-menu-overlay-visible block-overlay-ras" : "p-tieredmenu p-component p-tieredmenu-dynamic p-menu-overlay ";
    let phone_class   = this.state.visiblePhone ? "show-div" : "hide-div ";
    let email_class   = this.state.visibleEmail ? "show-div" : "hide-div ";
    return (
      <div className="p-grid home card">
        {/* RAS Validationpopup */}
      
        <div className={btn_class} >
            {/* <form name="form"  > */}
            <div  id="overlay_tmenu" className={overlay_class}>   
              <div className="p-grid content-ras-vldt">
                <div className="p-col-12">
                  <div className="p-grid">
                    <div className="p-col-10 p-sm-10">
                        <div className="name-div" >
                          {d_user.full_name}
                          </div>
                    </div>
                    <div className="p-col-2 p-sm-2">
                        <div className="img-div">
                          <img  alt="profile" src={d_user.image} />
                        </div>
                    </div>
                  </div>
                </div>
              
                <div className="p-col-12 p-panel margin-bottom-1">
                    <div className="p-panel-titlebar ">
                      RAS Account { this.state.type_user ? '' : ' : True Staff'}
                    </div>
                    <div className="p-col-12">
                      <div className="p-grid">
                        <div className="p-col-12  p-md-7 padding-right-0">
                            <img  alt="checked" src={d_validation.normal === 'Y' ? "assets/images/checked.png" : "assets/images/cross.png"} className="icon-checked"/>
                            <div className="text-ras-vldt"> 
                                <span className="detail-div">RAS OTP</span>
                            </div>
                        </div>

                        <div className="p-col-12  p-md-5 padding-right-0">
                            <div className="img-icon-crisis">
                              <img  alt="checked" src={d_validation.crisis === 'Y' ? "assets/images/checked.png" : "assets/images/cross.png"} className="icon-checked" />
                            </div>
                            <div className="text-ras-vldt name-icon-crisis"> 
                                <span className="detail-div">RAS Crisis</span><br/>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
            
                {/* Secondary Password */}
                <div className="p-col-12 p-panel margin-bottom-1">
                  <div className="p-panel-titlebar">
                    Secondary Password
                  </div>

                  <div className="p-col-12">
                    <div className="p-grid">
                      <div className="p-col-12 p-md-7 padding-right-0">
                        <div className="p-grid">
                          <div className="p-col-12 padding-right-0">
                         
                              <div className="img-icon">
                                <img  alt="checked" src={d_validation.sms === 'Y' ? "assets/images/checked.png" : "assets/images/cross.png"} className="icon-checked" />
                              </div>
                              <div className="text-ras-vldt name-icon"  > 
                                <span className="detail-div">SMS</span>{' '} <br/>
                                <span className="font-size-12"> 
                                  {d_validation.sms === 'Y' ? "Registered  " : "Not Registered "}{'    '}
                                  <img  alt="true move h" src="assets/images/truemoveh.png" className="icon-truemoveh" />
                                </span>
                              </div>
                              <div className={ `phone-div ${phone_class}` }  > 
                                <div className="p-grid">
                                      {d_validation.sms === 'Y' && d_validation.mobile_1 &&
                                      <React.Fragment>
                                        <div className="p-col-3 p-md-offset-1 p-sm-offset-1 padding-right-0 padding-bottom-0">
                                            <label htmlFor="num1"  className="p-checkbox-label font-size-11 padding-left-05">
                                            Number 1 : 
                                            </label>
                                        </div>
                                        <div className="p-col-7 padding-left-0 padding-bottom-0">
                                            <span className=" font-size-12 " style={{color:'#000000' , fontWeight: '500',top: '0.17em', position: 'relative'}}>{this.state.phone_1}</span>
                                        </div>
                                        <div className="p-col-3  p-md-offset-1 p-sm-offset-1 padding-right-0 padding-bottom-0">
                                            <label htmlFor="num1"  className="p-checkbox-label font-size-11 padding-left-05">Number 2 :</label>
                                        </div>
                                        {/* <div className="p-col-7 padding-left-0 padding-bottom-0">
                                            <span className=" font-size-12 ">{this.state.phone_2 ? this.state.phone_2 : "Register Your Phone"}</span>
                                        </div> */}
                                        {this.state.phone_2 && 
                                          <React.Fragment>
                                          <div className="p-col-7 padding-left-0 padding-bottom-0">
                                              <span className=" font-size-12 " style={{ color:'#000000' , fontWeight: '500',top: '0.17em', position: 'relative'}}>{this.state.phone_2 }</span>
                                          </div>
                                          </React.Fragment>
                                        }

                                        {this.state.phone_2 === '' && 
                                          <React.Fragment>
                                          <div className="p-col-7 padding-left-0 padding-bottom-0">
                                              <span className=" font-size-11 " style={{ color:'#848484' ,top: '0.1em', position: 'relative'}}> Register Your Phone</span>
                                          </div>
                                          </React.Fragment>
                                        }   
                                      </React.Fragment>
                                    }
                                </div>
                              </div>
                              <div className="p-col-12  p-md-offset-1 p-sm-offset-1 padding-bottom-0">
                              
                                <a href="assets/file/ลงทะเบียนขอ_SMS_OTP_Intranet_Internet.pdf"
                                  target="_blank" rel="noopener noreferrer">
                                  <span className="icon-globe" >
                                    {/* <img className="img-fluid image" style={{ width: '1.3em' }}
                                        src="assets/images/manual_pdf.png" alt="manual_pdf"  /> */}
                                    <i className="pi pi-file red " style={{ fontSize: '1.8em' }}></i>
                                  </span>
                                </a>
                                {/* <a href="https://pwdapprw1/iisadmpwdNew/CheckStatusN/LAN_PersonalPwd.asp?Domain=TRUE"
                                  target="_blank" rel="noopener noreferrer"> */}
                                 
                                  <span className="icon-globe" onClick={event => this.menusms_otp.toggle(event)}   aria-haspopup={true}
                      aria-controls="overlay_sms_otp">
                                    <i className="pi pi-globe red " style={{ fontSize: '1.8em' }}></i>
                                   
                                  </span>
                                {/* </a> */}
                                <span className="red font-size-12">ลงทะเบียนหรือต้องการเปลี่ยนเบอร์มือถือ</span>
                              </div>     
                          </div>
                          
                          <div className="p-col-12 padding-right-0" >
                            <div className="img-icon">
                              <img  alt="checked" src={d_validation.email  ? "assets/images/checked.png" : "assets/images/cross.png"}  className="icon-checked" />
                            </div>
                            <div className="text-ras-vldt name-icon"> 
                              <span className="detail-div">e-Mail</span><br/>
                              <span className="font-size-12">
                                {d_validation.email ? "Registered: "+(d_validation.email ? d_validation.email : '') : "Not Registered"}
                              </span>
                              
                            </div>
                            <div  className={ `p-col-12  p-md-offset-1 p-sm-offset-1 padding-bottom-0 ${email_class}`} >
                              <a href="assets/file/คู่มือขอสิทธิLAN Lotus Notes สำหรับ Outsource หรือ Vendor.pdf"
                                target="_blank" rel="noopener noreferrer">
                                <span className="icon-globe">
                                  {/* <img className="img-fluid image" style={{ width: '1.3em' }}
                                      src="assets/images/manual_pdf.png" alt="manual_pdf"  /> */}
                                  <i className="pi pi-file red " style={{ fontSize: '1.8em' }}></i>
                                </span>
                              </a>
                              <a href="https://iform.truecorp.co.th/jw/web/login"
                                target="_blank" rel="noopener noreferrer">
                                <span className="icon-globe">
                                  <i className="pi pi-globe red " style={{ fontSize: '1.8em',  top: '0.1em' ,position: 'relative'}}></i>
                                </span>
                              </a>
                              <span className="red font-size-12">ขั้นตอนการลงทะเบียน e-Mail</span>
                            </div>     
                          </div>
                          
                        </div>
                      </div>
                      
                      <div className="p-col-12  p-md-5 padding-right-0">
                          <div className="img-icon-crisis">
                            <img  alt="checked" src={d_validation.crisis === 'Y' ? "assets/images/checked.png" : "assets/images/cross.png"} className="icon-checked" />
                          </div>
                          <div className="text-ras-vldt name-icon-crisis"> 
                              <span className="detail-div">Crisis Password</span><br/>
                          </div>
                      </div>
                    </div>
                  </div>
                
                </div>
                
                {/* Confirm Button */}
                <div className="p-col-12 ">
                  <div className="p-grid p-justify-center">
                    <div className="p-md-7 p-sm-12 center">
                      <span className="confirm-btn" >
                        <Button type="Submit" label="OK" className="p-button p-button-black  top-bottom-left-radius" onClick={this.handleSubmit}/>
                      </span>
                      <span className="cancel-btn">
                        <Button label="CLOSE" className="p-button p-button-secondary top-bottom-right-radius" onClick={this.handleRasValidation.bind(this)} />
                      </span>
                    </div>
                  </div>
                </div>

              </div>
            </div>
        </div>
        <TieredMenu  className="sms_otp"  model={this.state.sms_otp}  popup={true} ref={el => (this.menusms_otp = el)}  id="overlay_sms_otp"/>

        {/* Logo */}
        <div className="p-col-12  p-md-8">
          <div className="title">
            <img src="assets/images/red-true.png" alt="img-icon" className="logo-img"/>
            <img src="assets/layout/images/WFH_4.png"  alt="logo"   className="logo-img" />
            {/* <span className="title-text themesecondary " 
              style={{verticalAlign: '-webkit-baseline-middle'
              ,fontWeight: 'bold'}} >{'  '}
              <ReactCssTooltip placement="bottom" className="txt-tooltip" 
              text={`RAS Validation V.2.1`} >
              ( Beta ) 
              </ReactCssTooltip>
            </span>   */}
          </div>
        </div>


        {/* RAS Validation */}
        {/* <div className="p-col-12  p-md-3 ras-validation">
          <div className="float-right " aria-controls="overlay_tmenu" onClick={this.handleRasValidation.bind(this)}>
            <span  className="wave in red">
              <img src="assets/images/ras-validation.png" alt="img-icon" className="stamp-icon"/>
            </span>
            <span className="themesecondary text-ras-vld"  >RAS Check-Up</span>
          </div>
        </div> */}
        <div className="p-col-12  p-md-4">
          <img src="assets/images/contact.png"   alt="contact"   className="img-contact" />
        </div>

        <div style={{clear:'both'}}></div>

        <div className="p-col-12">
          <div className="p-grid">
            <div className="p-sm-12 p-md-3 p-lg-3 p-xl-3 "
                style={{ backgroundColor: '#fffffff', marginBottom: '5px' }} >
              <div className="p-grid wid-app">
                <div className="p-col-12">
                  <div className="widget-header bordered-bottom bordered-themesecondary">
                    <i
                      className="widget-icon pi pi-tags themesecondary"
                      style={{ fontSize: '1.5em' }}
                    ></i>
                    <span className="widget-caption themesecondary">
                      Internet Links
                    </span>
                  </div>
                </div>
                <Application />
              </div>
            </div>

            <div className="p-sm-12 p-md-9 p-lg-9 p-xl-9">
              <div className="p-grid second-div">
                <div className="p-col-12">
                  <div className="p-grid">
                    <div className="p-col-12">
                      <div className="widget-header bordered-bottom bordered-themesecondary">
                        <i className="widget-icon pi pi-tags themesecondary"
                          style={{ fontSize: '1.5em' }}></i>
                        <span className="widget-caption themesecondary">
                          Guidelines for Troubleshooting and FAQ
                          {/* Remote Access Services [RAS] */}
                          <br />
                          <span className="txt-under">
                          แนวทางการแก้ไขWFHด้วยตนเอง และคำถามที่ถูกต้อง
                            {/* การขอ และคู่มือการใช้งาน Ras Pluse Secure */}
                          </span>
                        </span>
                      </div>
                    </div>
                    <RAS />
                    
                  </div>
                </div>

                <div className="p-col-12">
                  <div className="p-grid">
                    <div className="p-col-12">
                      <div className="widget-header bordered-bottom bordered-themesecondary">
                        <i className="widget-icon pi pi-tags themesecondary"   style={{ fontSize: '1.5em' }} ></i>
                        <span className="widget-caption themesecondary">
                          เครื่องมือสนับสนุน Work From Home
                        </span>
                      </div>
                    </div>
                    <MeetingSoftware />
                  </div>
                </div>

                <div className="p-col-12">
                  <div className="p-grid">
                    <div className="p-col-12">
                      <div className="widget-header bordered-bottom bordered-themesecondary">
                        <i className="widget-icon pi pi-tags themesecondary"   style={{ fontSize: '1.5em' }} ></i>
                        <span className="widget-caption themesecondary">
                          แนะนำวิธีการใช้งาน Microsoft Outlook
                        </span>
                      </div>
                    </div>
                    <MicrosoftOutlook/>
                  </div>
                </div>

                <div className="p-col-12">
                  <div className="p-grid">
                    <div className="p-col-12">
                      <div className="widget-header bordered-bottom bordered-themesecondary">
                        <i className="widget-icon pi pi-tags themesecondary"   style={{ fontSize: '1.5em' }} ></i>
                        <span className="widget-caption themesecondary">
                          เครื่องมือสนับสนุนสำหรับให้ความช่วยเหลือ
                          {/* <br/><span className="txt-under">โปรแกรมในการใช้งาน ( Remote เครื่องคอมพิวเตอร์, ข้อมูลคอมพิวเตอร์ และปิดกั้นการใช้ไฟร์วอลล์ )</span> */}
                        </span>
                      </div>
                    </div>
                    <SupportSoftware />
                  </div>
                </div>

                <div className="p-col-12">
                  <div className="widget-header bordered-bottom bordered-themesecondary">
                    <i className="widget-icon pi pi-tags themesecondary"   style={{ fontSize: '1.5em' }} ></i>
                    <span className="widget-caption themesecondary">
                    Software และคู่มือ สนับสนุนการทำงานของ Shop
                    </span>
                  </div>
                  <SoftwareInstallerShop />
                </div>

                <div className="p-col-12">
                  <div className="widget-header bordered-bottom bordered-themesecondary">
                    <i className="widget-icon pi pi-tags themesecondary"   style={{ fontSize: '1.5em' }} ></i>
                    <span className="widget-caption themesecondary">
                      Software สนับสนุนการทำงานของหน่วยงาน
                    </span>
                  </div>
                  <SoftwareInstaller />
                </div>

               

              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
    const {  authentication,  notification ,users } = state;
    const { user } = authentication;
    const { ras_validation } = users;
    return {
        user,
        notification,
        ras_validation
    };
}
export default connect(mapStateToProps)(Home);
