import React from 'react';
import ReactDOM from 'react-dom';
import FormsDemo from './FormsDemo';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<FormsDemo />, div);
});
