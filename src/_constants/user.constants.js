export const userConstants = {
    REGISTER_REQUEST: 'USERS_REGISTER_REQUEST',
    REGISTER_SUCCESS: 'USERS_REGISTER_SUCCESS',
    REGISTER_FAILURE: 'USERS_REGISTER_FAILURE',

    LOGIN_REQUEST: 'USERS_LOGIN_REQUEST',
    LOGIN_SUCCESS: 'USERS_LOGIN_SUCCESS',
    LOGIN_FAILURE: 'USERS_LOGIN_FAILURE',
    
    LOGOUT: 'USERS_LOGOUT',

    GETALL_REQUEST: 'USERS_GETALL_REQUEST',
    GETALL_SUCCESS: 'USERS_GETALL_SUCCESS',
    GETALL_FAILURE: 'USERS_GETALL_FAILURE',

    DELETE_REQUEST: 'USERS_DELETE_REQUEST',
    DELETE_SUCCESS: 'USERS_DELETE_SUCCESS',
    DELETE_FAILURE: 'USERS_DELETE_FAILURE',    

    LOADING_REQUEST: 'LOADING_REQUEST',
    LOADING_SUCCESS: 'LOADING_SUCCESS',

    GETUSER_REQUEST: 'USERS_GET_REQUEST',
    GETUSER_SUCCESS: 'USERS_GET_SUCCESS',
    GETUSER_FAILURE: 'USERS_GET_FAILURE',

    UPDATE_REQUEST: 'USERS_UPDATE_REQUEST',
    UPDATE_SUCCESS: 'USERS_UPDATE_SUCCESS',
    UPDATE_FAILURE: 'USERS_UPDATE_FAILURE',

    UPFILE_SUCCESS: 'USERS_UPFILE_SUCCESS',

    NEWTICKET_REQUEST: 'USERS_NEWTICKET_REQUEST',
    NEWTICKET_SUCCESS: 'USERS_NEWTICKET_SUCCESS',
    NEWTICKET_FAILURE: 'USERS_NEWTICKET_FAILURE',

    TRACKING_REQUEST : 'USERS_TRACKING_REQUEST',
    TRACKING_SUCCESS : 'USERS_TRACKING_SUCCESS',

    BYPASS_SUCCESS : 'USERS_BYPASS_SUCCESS',


    RASVALIDATION_REQUEST: 'USERS_RASVALIDATION_REQUEST',
    RASVALIDATION_SUCCESS: 'USERS_RASVALIDATION_SUCCESS',
    RASVALIDATION_FAILURE: 'USERS_RASVALIDATION_FAILURE'
};
