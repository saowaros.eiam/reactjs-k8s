import { store } from '../_helpers';

function mapState(state){
    const  { authentication } = state;
    const  user               = authentication.user;
    return {
        user
    };
};
let d_user = mapState(store.getState());
let user = d_user.user ? d_user.user : {};

export const d_state = {
    user
};