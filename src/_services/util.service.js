import * as moment from 'moment/moment';
import { Base64 } from 'js-base64';
import md5 from 'md5';

export const UtilService = {
  getToken,
  randomString,
  removeAllWhitespace,
  encryptJson,
  decryptJson,
  decodeJson,
  isBase64
};

/**
 * getToken
 */
function getToken() {
  return md5(moment().format('YYYYMMDD')) + md5(moment().format('DDMMYYYY'));
}

/**
 * randomString
 *
 * @param length
 */
function randomString(length) {
  let chars = '123456789abcdefghjkmnpqrsuvwxyzABCDEFGHKLMNPQRSTUVWXYZ';
  let result = '';

  for (let i = length; i > 0; --i) {
    result += chars[Math.floor(Math.random() * chars.length)];
  }

  return result;
}

/**
 * removeAllWhitespace
 *
 * @param text
 */
function removeAllWhitespace(text) {
  let _return = '';
  if (typeof text === 'string' || text instanceof String) {
    _return = text.replace(/\s/g, '');
  }
  return _return;
}

function randomText(length = 12) {
  let alphabet = 'abcdefghjklmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ';
  let randomString = '';

  for (let i = length; i > 0; --i) {
    randomString += alphabet[Math.floor(Math.random() * alphabet.length)];
  }

  // -----------
  return randomString;
}

function randomNumber(length = 12) {
  let alphabet = '123456789';
  let randomNum = '';

  for (let i = length; i > 0; --i) {
    randomNum += alphabet[Math.floor(Math.random() * alphabet.length)];
  }

  // -----------
  return randomNum;
}

/*
 *  input data string
 *  pattern a2b5|yy|baseJson(xxxxx)|ccccc
 *   numLeft : 2
 *   ShiftLeft : 4 + 2
 *   numRight  : 5
 *   ShiftRight : length - 5
 *   decode
 */
function decryptJson(data) {
  let d_result = null;
  let ShiftLeft = null;
  let numLeft = null;
  let numRight = null;
  let dRight = null;
  let ShiftRight = null;
  let data_encode = null;
  let data_padleft = null;
  let de_result = null;

  if (data) {
    // data  =  data.trim();

    numLeft = data.substr(1, 1);
    ShiftLeft = parseInt(4) + parseInt(numLeft);
    numRight = data.substr(3, 1);
    data_padleft = data.substr(ShiftLeft);

    dRight = numRight * 1;
    ShiftRight = data_padleft.length - dRight;
    data_encode = data_padleft.substr(0, ShiftRight);

    de_result = Base64.decode(data_encode);
    d_result = JSON.parse(de_result);
  }
  return d_result;
}

/*
 *  input data string
 *  pattern random(str,length = 1)
 *         |random(num,length = 1) : 1
 *         |random(str,length = 1)
 *         |random(num,length = 1) : 2
 *         |:1 -> random(str)
 *         |baseJson(data)
 *         |:2 -> random(str)
 */
function encryptJson(data) {
  let d_result = null;
  let firstStr = null;
  let firstNum = null;
  let secondStr = null;
  let secondNum = null;
  let rdFirstNum = null;
  let d_base64 = null;
  let rdSecondNum = null;

  if (data.length > 0) {
    data = data.trim();
    firstStr = randomText(1);
    firstNum = randomNumber(1);
    secondStr = randomText(1);
    secondNum = randomNumber(1);
    rdFirstNum = randomText(firstNum);
    d_base64 = Base64.encode(data);
    rdSecondNum = randomText(secondNum);
    d_result =
      firstStr +
      firstNum +
      secondStr +
      secondNum +
      rdFirstNum +
      d_base64 +
      rdSecondNum;
  }
  return d_result;
}

function decodeJson(data) {
  let result = null;
  let decode_data = null;
  let arr_data = [];

  if (data) {
    arr_data = { ...data };
    decode_data = Base64.decode(arr_data.data);
    arr_data.data = JSON.parse(decode_data);

    // if (md5(decode_data) === arr_data.headers.authorization.trim()) {
    if (true) {
      result = arr_data;
    } else {
      let status = false;
      arr_data.data = { ...arr_data.data, status };
      result = arr_data;
    }
  }
  return result;
}

function isBase64(str) {
  try {
      return btoa(atob(str)) === str;
  } catch (err) {
      return false;
  }
} 

