import axios from 'axios';
import { authapi }  from './authapi.service';
import { UtilService } from './util.service';

let returnObj = {
    'status' : false
    ,'data'  : ''
};

export class userSummarys{
    getSummarysByItem(data) {
        return axios(authapi.HeaderPost('Summarys','getByItem',JSON.stringify(data)))
            .then((response) => {
                // response = UtilService.decryptJson(response.data);
                response = UtilService.decodeJson(response);
            
                if (response.data.status === false) {
                    returnObj = {
                        status: false,
                        data: 'Unsafe data.'
                    };
                    return returnObj; 

                }else {    
                    returnObj = {
                        'status' : true
                        ,'data'  : response.data.result
                    };
                    return returnObj;
                }
            }).catch((error) => {
                returnObj = {
                    'status' : false
                    ,'data'  : error
                };
                return returnObj;
            })
    }





    getSummarys(data) {
        return axios(authapi.HeaderPost('Summarys','',JSON.stringify(data)))
            .then((response) => {
                //response = UtilService.decryptJson(response.data);
                response = UtilService.decodeJson(response);
                if (response.data.status === false) {
                    returnObj = {
                        status: false,
                        data: 'Unsafe data.'
                    };
                    return returnObj; 
                }else {
                    returnObj = {
                        'status' : true
                        ,'data'  : response.data.result
                    };
                    return returnObj;
                }  
            }).catch((error) => {
                returnObj = {
                    'status' : false
                    ,'data'  : error
                };
                return returnObj;
            })
    }

    getSummarys_Layer(data) {
        return axios(authapi.HeaderPost('Summarys','getSummarys_Layer',JSON.stringify(data)))
            .then((response) => {
               // response = UtilService.decryptJson(response.data);
               response = UtilService.decodeJson(response);
               if (response.data.status === false) {
                    returnObj = {
                       status: false,
                       data: 'Unsafe data.'
                    };
                   return returnObj; 
               }else {
                    returnObj = {
                        'status' : true
                        ,'data'  : response.data.result
                    };
                    return returnObj;
                }
            }).catch((error) => {
                returnObj = {
                    'status' : false
                    ,'data'  : error
                };
                return returnObj;
            })
    }

    

    getSummarysByItemID(data) {
        return axios(authapi.HeaderPost('Summarys','getByItemID',JSON.stringify(data)))
            .then((response) => {
                // response = UtilService.decryptJson(response.data);
                response = UtilService.decodeJson(response);
                if (response.data.status === false) {
                    returnObj = {
                        status: false,
                        data: 'Unsafe data.'
                    };
                    return returnObj; 
                }else { 
                    returnObj = {
                        'status' : true
                        ,'data'  : response.data.result
                    };
                
                    return returnObj;
                }
            }).catch((error) => {
                returnObj = {
                    'status' : false
                    ,'data'  : error
                };
                return returnObj;
            })
    }
    
    getSummarysByItemID_Layer(data) {
        return axios(authapi.HeaderPost('Summarys','getSummarysByItemID_Layer',JSON.stringify(data)))
            .then((response) => {
                // response = UtilService.decryptJson(response.data);
                response = UtilService.decodeJson(response);
                if (response.data.status === false) {
                    returnObj = {
                       status: false,
                       data: 'Unsafe data.'
                    };
                    return returnObj; 

                }else { 
                    returnObj = {
                        'status' : true
                        ,'data'  : response.data.result
                    };
                
                    return returnObj;
                }
            }).catch((error) => {
                returnObj = {
                    'status' : false
                    ,'data'  : error
                };
                return returnObj;
            })
    }

    getItemSummary(data) {
        return axios(authapi.HeaderPost('Summarys','getItemSummary',JSON.stringify(data)))
        .then((response) => {
           // response = UtilService.decryptJson(response.data);
            response = UtilService.decodeJson(response);
            if (response.data.status === false) {
                returnObj = {
                    status: false,
                    data: 'Unsafe data.'
                };
                return returnObj; 

            }else { 
                returnObj = {
                    'status' : true
                    ,'data'  : response.data.result
                };
                return returnObj;
            }
        }).catch((error) => {
            returnObj = {
                'status' : false
                ,'data'  : error
            };
            return returnObj;
        })
    }

    
    getSummarytype() {
        return axios(authapi.HeaderPost('Summarys','getSummarytype'))
        .then((response) => {
            //response = UtilService.decryptJson(response.data);
            response = UtilService.decodeJson(response);
            if (response.data.status === false) {
                returnObj = {
                    status: false,
                    data: 'Unsafe data.'
                };
                return returnObj; 

            }else { 
                returnObj = {
                    'status' : true
                    ,'data'  : response.data.result
                };
                return returnObj;
            }
        }).catch((error) => {
            returnObj = {
                'status' : false
                ,'data'  : error
            };
            return returnObj;
        })
    }

    
    getErrortype() {
        return axios(authapi.HeaderPost('Summarys','getErrortype'))
        .then((response) => {
            //response = UtilService.decryptJson(response.data);
            response = UtilService.decodeJson(response);
            if (response.data.status === false) {
                returnObj = {
                    status: false,
                    data: 'Unsafe data.'
                };
                return returnObj; 

            }else { 
                returnObj = {
                    'status' : true
                    ,'data'  : response.data.result
                };
                return returnObj;
            }
        })
        .catch((error) => {
            returnObj = {
                'status' : false
                ,'data'  : error
            };
            return returnObj;
        })
    }

    
    getWAGrouptype() {
        return axios(authapi.HeaderPost('Summarys','getWAGrouptype'))
        .then((response) => {
           // response = UtilService.decryptJson(response.data);
            response = UtilService.decodeJson(response);
            if (response.data.status === false) {
                returnObj = {
                    status: false,
                    data: 'Unsafe data.'
                };
                return returnObj; 

            }else { 
                returnObj = {
                    'status' : true
                    ,'data'  : response.data.result
                };
                return returnObj;
            }
        }).catch((error) => {
            returnObj = {
                'status' : false
                ,'data'  : error
            };
            return returnObj;
        })
    }

    getItemSummaryError(data) {
        return axios(authapi.HeaderPost('Summarys','getItemSummaryError',JSON.stringify(data)))
        .then((response) => {
            //response = UtilService.decryptJson(response.data);
            response = UtilService.decodeJson(response);
            if (response.data.status === false) {
                returnObj = {
                    status: false,
                    data: 'Unsafe data.'
                };
                return returnObj; 

            }else {
                returnObj = {
                    'status' : true
                    ,'data'  : response.data.result
                };
                return returnObj;
            }
        })
        .catch((error) => {
            returnObj = {
                'status' : false
                ,'data'  : error
            };
            return returnObj;
        })
    }

    createSummary(data) {
        return axios(authapi.HeaderPost('Summarys','createSummary',JSON.stringify(data)))
        .then((response) => {
            //response = UtilService.decryptJson(response.data);
            response = UtilService.decodeJson(response);
            if (response.data.status === false) {
                returnObj = {
                    status: false,
                    data: 'Unsafe data.'
                };
                return returnObj; 

            }else {
                returnObj = {
                    'status' : true
                    ,'data'  : response.data.result
                };
                return returnObj;
            }
        }).catch((error) => {
            returnObj = {
                'status' : false
                ,'data'  : error
            };
            return returnObj;
        })
    }

    updateSummary(data) {
        return axios(authapi.HeaderPost('Summarys','updateSummary',JSON.stringify(data)))
        .then((response) => {
            //response = UtilService.decryptJson(response.data);
            response = UtilService.decodeJson(response);
            if (response.data.status === false) {
                returnObj = {
                    status: false,
                    data: 'Unsafe data.'
                };
                return returnObj; 

            }else {
                returnObj = {
                    'status' : true
                    ,'data'  : response.data.result
                };
                return returnObj;
            }
        })
        .catch((error) => {
            returnObj = {
                'status' : false
                ,'data'  : error
            };
            return returnObj;
        })
    }
    
    delSummary(data) {
        let returnObj = {
            'status' : false
            ,'data'  : ''
        };
        return axios(authapi.HeaderPost('Summarys','delSummary',JSON.stringify(data)))
        .then((response) => {
            //response = UtilService.decryptJson(response.data);
            response = UtilService.decodeJson(response);
            if (response.data.status === false) {
                returnObj = {
                    status: false,
                    data: 'Unsafe data.'
                };
                return returnObj; 

            }else {
                returnObj = {
                    'status' : true
                    ,'data'  : response.data.result
                };
                return returnObj;
            }
        }).catch((error) => {
            returnObj = {
                'status' : false
                ,'data'  : error
            };
            return returnObj;
        })
    }

    
    delSummary_Err_Wa(data) {
        return axios(authapi.HeaderPost('Summarys','delSummary_Err_Wa',JSON.stringify(data)))
        .then((response) => {
            //response = UtilService.decryptJson(response.data);
            response = UtilService.decodeJson(response);
            if (response.data.status === false) {
                returnObj = {
                    status: false,
                    data: 'Unsafe data.'
                };
                return returnObj; 

            }else {
                returnObj = {
                    'status' : true
                    ,'data'  : response.data.result
                };
                return returnObj;
            }

        }).catch((error) => {
            returnObj = {
                'status' : false
                ,'data'  : error
            };
            return returnObj;
        })
    }


    updateSummaryErrWA(data) {
        return axios(authapi.HeaderPost('Summarys','updateSummaryErrWA',JSON.stringify(data)))
        .then((response) => {
           // response = UtilService.decryptJson(response.data);
            response = UtilService.decodeJson(response);
            if (response.data.status === false) {
                returnObj = {
                    status: false,
                    data: 'Unsafe data.'
                };
                return returnObj; 

            }else {
                returnObj = {
                    'status' : true
                    ,'data'  : response.data.result
                };
                return returnObj;
            }
        }).catch((error) => {
            returnObj = {
                'status' : false
                ,'data'  : error
            };
            return returnObj;
        })
    }


    upDTSummaryErrWA(data) {
        return axios(authapi.HeaderPost('Summarys','upDTSummaryErrWA',JSON.stringify(data)))
        .then((response) => {
            //response = UtilService.decryptJson(response.data);
            response = UtilService.decodeJson(response);
            if (response.data.status === false) {
                returnObj = {
                    status: false,
                    data: 'Unsafe data.'
                };
                return returnObj; 

            }else {
                returnObj = {
                    'status' : true
                    ,'data'  : response.data.result
                };
                return returnObj;
            }
        }).catch((error) => {
            returnObj = {
                'status' : false
                ,'data'  : error
            };
            return returnObj;
        })
    }


    CreateSummaryErrWA(data) {
        return axios(authapi.HeaderPost('Summarys','CreateSummaryErrWA',JSON.stringify(data)))
        .then((response) => {
           // response = UtilService.decryptJson(response.data);
            response = UtilService.decodeJson(response);
            if (response.data.status === false) {
                returnObj = {
                    status: false,
                    data: 'Unsafe data.'
                };
                return returnObj; 

            }else {
                returnObj = {
                    'status' : true
                    ,'data'  : response.data.result
                };
                return returnObj;
            }
        }).catch((error) => {
            returnObj = {
                'status' : false
                ,'data'  : error
            };
            return returnObj;
        })
    }
    

    upDTErrErrorDT(data) {
        return axios(authapi.HeaderPost('Summarys','upDTErrErrorDT',JSON.stringify(data)))
        .then((response) => {
           // response = UtilService.decryptJson(response.data);
            response = UtilService.decodeJson(response);
            if (response.data.status === false) {
                returnObj = {
                    status: false,
                    data: 'Unsafe data.'
                };
                return returnObj; 

            }else {
                returnObj = {
                    'status' : true
                    ,'data'  : response.data.result
                };
                return returnObj;
            }        
           
        }).catch((error) => {
            returnObj = {
                'status' : false
                ,'data'  : error
            };
            return returnObj;
        })
    }

    
    updateWA(data) {
        return axios(authapi.HeaderPost('Summarys','updateWA',JSON.stringify(data)))
        .then((response) => {
            //response = UtilService.decryptJson(response.data);
            response = UtilService.decodeJson(response);
            if (response.data.status === false) {
                returnObj = {
                    status: false,
                    data: 'Unsafe data.'
                };
                return returnObj; 

            }else {
                returnObj = {
                    'status' : true
                    ,'data'  : response.data.result
                };
                return returnObj;
            }
        }).catch((error) => {
            returnObj = {
                'status' : false
                ,'data'  : error
            };
            return returnObj;
        })
    }


    getSummaryByCTIname(data) {
        return axios(authapi.HeaderPost('Summarys','getSummaryByCTIname',JSON.stringify(data)))
            .then((response) => {
                //response = UtilService.decryptJson(response.data);
                response = UtilService.decodeJson(response);
                if (response.data.status === false) {
                    returnObj = {
                        status: false,
                        data: 'Unsafe data.'
                    };
                    return returnObj; 
                }else {
                    returnObj = {
                        'status' : true
                        ,'data'  : response.data.result
                    };
                    return returnObj;
                }  
            }).catch((error) => {
                returnObj = {
                    'status' : false
                    ,'data'  : error
                };
                return returnObj;
            })
    }

}


