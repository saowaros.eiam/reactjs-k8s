import axios from "axios";
import { authapi } from "./authapi.service";
import { Base64 } from "js-base64";
import { UtilService } from "./util.service";

export const userService = {
  Login,
  Logout,
  getAccount,
  updateAccount,
  Registershop,
  Registeroutsource,
  VerifyLogin,
  Forgotpwd,
  chkLoginname,
  newTicket,
  Tracking,
  updateRasValidation,
  getRasValidation
};

let returnObj = {
  status: false,
  data: ""
};



function Forgotpwd(user) {
  if (user.password) {
    user.password = UtilService.randomString(2) + Base64.encode(user.password) + UtilService.randomString(4);
  }
  return axios(authapi.HeaderPost("Account", "forgotpwd", JSON.stringify(user)))
    .then(function(response) {
      response = UtilService.decodeJson(response);
      if (response.data.status === false) {
        returnObj = {
          status: false,
          data: 'Unsafe data.'
        };
        return returnObj; 
      }else if (response.data.result) {
        returnObj = {
          status: true,
          data: response.data.result
        };
        return returnObj;
      }
    }).catch(function(error) {
      returnObj = {
        status: false,
        data: error.message
      };
      return returnObj;
    });
}

function VerifyLogin(user) {
  return axios(authapi.HeaderPost("Account", "verifylogin", JSON.stringify(user)))
    .then(function(response) {
      response = UtilService.decodeJson(response);
      if (response.data.status === false) {
        returnObj = {
          status: false,
          data: 'Unsafe data.'
        };
        return returnObj; 
      }else if (response.data.result) {
        returnObj = {
          status: true,
          data: response.data.result
        };
        return returnObj;
      }
    }).catch(function(error) {
      returnObj = {
        status: false,
        data: error.message
      };
      return returnObj;
    });
}

function Registeroutsource(user) {
  return axios(authapi.HeaderPost("Account","Registeroutsource",JSON.stringify(user)))
    .then(function(response) {
      
      response = UtilService.decodeJson(response);
      if (response.data.status === false) {
        returnObj = {
          status: false,
          data: 'Unsafe data.'
        };
        return returnObj; 
      }else if (response.data.result) {
        returnObj = {
          status: true,
          data: response.data.result
        };
        return returnObj;
      }
    }).catch(function(error) {
      returnObj = {
        status: false,
        data: error.message
      };
      return returnObj;
    });
}

function Registershop(user) {
  return axios(authapi.HeaderPost("Account", "registershop", JSON.stringify(user)))
    .then(function(response) {
      response = UtilService.decodeJson(response);
      if (response.data.status === false) {
        returnObj = {
          status: false,
          data: 'Unsafe data.'
        };
        return returnObj; 
      }else if (response.data.result) {
        returnObj = {
          status: true,
          data: response.data.result
        };
        return returnObj;
      }
    }).catch(function(error) {
      returnObj = {
        status: false,
        data: error.message
      };
      return returnObj;
    });
}

function Login(username, password) {
  return axios(authapi.HeaderLogin(username, password))
    .then(function(response) {
      response = UtilService.decodeJson(response);
      if (response.data.status === false) {
        returnObj = {
          status: false,
          data: 'Unsafe data.'
        };
        return returnObj; 
      }else if (response.data.results.code === 200) {
        const key = Base64.encode(response.data.results.data.token);
        localStorage.setItem("wah", JSON.stringify(key));
        returnObj = {
          status: true,
          data: response.data.results.data
        };
        return returnObj;
      }else if (response.data.results.code === 401) {
        returnObj = {
          status: true,
          data: response.data.results
        };
        return returnObj;
      } else {
        returnObj = {
          status: true,
          data: response.data.results.code
        };
        return returnObj;
      }
    }).catch(function(error) {
      returnObj = {
        status: false,
        data: error.message
      };
      return returnObj;
    });
}

function Logout() {
  // remove user from local storage to log user out
  localStorage.removeItem("wah");
  localStorage.removeItem("avt");

}

function getAccount(key) {
  return axios(authapi.HeaderPost("Account", "getbyToken", JSON.stringify(key)))
    .then(function(response) {
      response = UtilService.decodeJson(response);
      if(response.data.status === false) {
        returnObj = {
          status: false,
          data: 'Unsafe data.'
        };
        return returnObj; 
      }else if (response.data.result) {
        let result = {...response.data.result};
        if(result.action || result.action === 'Account'){
          localStorage.removeItem("wah");
        }
        returnObj = {
          status: true,
          data: response.data.result
        };
        return returnObj;
      }
    }).catch(function(error) {
      returnObj = {
        status: false,
        data: error.message
      };
      return returnObj;
    });
}

function updateAccount(data) {
  return axios(authapi.HeaderPost("Account", "updateAccount", JSON.stringify(data)))
    .then(function(response) {
      response = UtilService.decodeJson(response);
      if (response.data.status === false) {
        returnObj = {
          status: false,
          data: 'Unsafe data.'
        };
        return returnObj; 
      }else if (response.data.result) {
        // const key = Base64.encode(response.data.result.token);
        // localStorage.setItem("wah", JSON.stringify(key));
        return response.data.result;
      }
    }).catch(function(error) {
      returnObj = {
        status: false,
        data: error.message
      };
      return returnObj;
    });
}

function chkLoginname(name) {
  return axios(authapi.HeaderPost("Account", "chkLoginname", JSON.stringify(name)))
    .then(function(response) {
      response = UtilService.decodeJson(response);
      if (response.data.status === false) {
        returnObj = {
          status: false,
          data: 'Unsafe data.'
        };
        return returnObj; 
      }else if (response.data.result) {
        returnObj = {
          status: true,
          data: response.data.result
        };
        return returnObj;
      }
    }).catch(function(error) {
      returnObj = {
        status: false,
        data: error.message
      };
      return returnObj;
    });
}

function newTicket(data) {
  return axios(authapi.HeaderPost("TtOpenTicket", "", JSON.stringify(data)))
    .then(function(response) {
      //response = UtilService.decryptJson(res.data);
      response = UtilService.decodeJson(response);

      if (response.data.status === false) {
        returnObj = {
          status: false,
          data: 'Unsafe data.'
        };
        return returnObj; 
      }else if (response.data.result) {
        returnObj = {
          status: true,
          data: response.data.result
        };
        return returnObj;
      }
    }).catch(function(error) {
      returnObj = {
        status: false,
        data: error.message
      };
      return returnObj;
    });
}

function Tracking(key) {
  return axios(authapi.HeaderPost("Tracking", "", JSON.stringify(key)))
    .then(function(response) {
      response = UtilService.decodeJson(response);
      if(response.data.status === false) {
        returnObj = {
          status: false,
          data: 'Unsafe data.'
        };
        return returnObj; 
      }else if (response.data.result) {
        returnObj = {
          status: true,
          data: response.data.result
        };
        return returnObj;
      }
    }).catch(function(error) {
      returnObj = {
        status: false,
        data: error.message
      };
      return returnObj;
    });
}

function updateRasValidation(data) {
  return axios(authapi.HeaderPost("Rasvalidation", "update", JSON.stringify(data)))
    .then(function(response) {
      response = UtilService.decodeJson(response);
      if (response.data.status === false) {
        returnObj = {
          status: false,
          data: 'Unsafe data.'
        };
        return returnObj; 
      }else if (response.data.result) {
        returnObj = {
          status: true,
          data: response.data.result
        };
        return returnObj;
      }
    }).catch(function(error) {
      returnObj = {
        status: false,
        data: error.message
      };
      return returnObj;
    });
}


function getRasValidation(data) {
  return axios(authapi.HeaderPost("Rasvalidation", "get", JSON.stringify(data)))
    .then(function(response) {
      response = UtilService.decodeJson(response);
      if (response.data.status === false) {
        returnObj = {
          status: false,
          data: 'Unsafe data.'
        };
        return returnObj; 
      }else if (response.data.result) {
        returnObj = {
          status: true,
          data: response.data.result
        };
        return returnObj;
      }
    }).catch(function(error) {
      returnObj = {
        status: false,
        data: error.message
      };
      return returnObj;
    });
}