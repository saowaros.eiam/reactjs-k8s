import { environment } from '../_environments/environment';
import { UtilService } from './util.service';
import { Base64 } from 'js-base64';
import { store } from '../_helpers';

 export const authapi = {
    HeaderLogin
   ,HeaderPost
};

let Hr_token = '';
function mapState(state){
    const  { authentication } = state;
    const  user               = authentication.user;
    return {
        user
    };
};

let d_user = mapState(store.getState());
let data = {...d_user};
    Hr_token = data.token ? data.token : d_user.user;

function HeaderLogin(username,password){
    password  = password.trim();
    let _url  = environment.backendUrlPrefix + environment.hrLoginUrl;
    let _pwd  = UtilService.randomString(2) + Base64.encode(password) + UtilService.randomString(4);
    let _data = Base64.encode(environment.hrAppKey + '|' + Base64.encode(UtilService.removeAllWhitespace(username)) + '|' + _pwd);

    const bodyFormData = new FormData();
        bodyFormData.append('t', UtilService.getToken());
        bodyFormData.append('d', _data);  
       
    const Header = {
        method: 'post',
        url: _url,
        data: bodyFormData,
        config: { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
        }
    }
    return Header;
}

function HeaderPost( action ,method= '',data=''  ){   
    let _data;
    if(action && data){
        _data =  Base64.encode( Base64.encode(action) + '|' + Base64.encode(method) + '|' + Base64.encode(data) );
    }else{
        _data =  Base64.encode( Base64.encode(action) + '|' + Base64.encode(method) + '|');
    }

    let _url  = environment.backendUrlPrefix + environment.backendQuery;

    
    const bodyFormData = new FormData();
        bodyFormData.append('t', UtilService.getToken());
        bodyFormData.append('h', Base64.encode(Hr_token));
        bodyFormData.append('d', _data);
    
    const Header = {
        method: 'post',
        url: _url,
        data: bodyFormData, 
        config: { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
        }
    }
    return Header;
}




